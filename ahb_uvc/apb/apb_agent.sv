class apb_agent extends uvm_agent;
	`uvm_component_utils(apb_agent)
	apb_sequencer p_seq;		//p denotes apb
	apb_driver p_drv;		

	function new(string name="", uvm_component parent = null);
		super.new(name, parent);
	endfunction

	function void build_phase(uvm_phase phase);
		`uvm_info("apb_agent", "build_phase", UVM_LOW)
		p_seq = apb_sequencer::type_id::create("p_seq", this);
		p_drv = apb_driver::type_id::create("p_drv", this);	
	endfunction

	function void connect_phase(uvm_phase phase);
		`uvm_info("apb_agent", "connect_phase", UVM_LOW)	
		p_drv.seq_item_port.connect(p_seq.seq_item_export);
	endfunction
endclass
