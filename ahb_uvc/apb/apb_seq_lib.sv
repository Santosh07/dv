class apb_base_sequence extends uvm_sequence#(apb_tx);
	`uvm_object_utils(apb_base_sequence)

	function new(string name="");
		super.new(name);
	endfunction

	task pre_body();
		if (starting_phase!=null) begin
			starting_phase.raise_objection(this);
		end
	endtask

	task post_body();
		if (starting_phase!=null) begin
			starting_phase.drop_objection(this);
		end
	endtask

endclass

class apb_config_reg_seq extends apb_base_sequence;
	`uvm_object_utils(apb_config_reg_seq)
	
	function new(string name="");
		super.new(name);
	endfunction

	task body();
		`uvm_do_with(req, {req.paddr==`S0_START_REG_ADDR;
				   req.pdata==`S0_START_ADDR;
				   req.wr_rd==1;
				  });
		`uvm_do_with(req, {req.paddr==`S0_END_REG_ADDR;
				   req.wr_rd==1;
				   req.pdata==`S0_END_ADDR;});
		`uvm_do_with(req, {req.paddr==`S1_START_REG_ADDR;
				   req.wr_rd==1;
				   req.pdata==`S1_START_ADDR;});
		`uvm_do_with(req, {req.paddr==`S1_END_REG_ADDR;
				   req.wr_rd==1;
				   req.pdata==`S1_END_ADDR;});
		`uvm_do_with(req, {req.paddr==`S2_START_REG_ADDR;
				   req.wr_rd==1;
				   req.pdata==`S2_START_ADDR;});
		`uvm_do_with(req, {req.paddr==`S2_END_REG_ADDR;
				   req.wr_rd==1;
				   req.pdata==`S2_END_ADDR;});
	endtask
endclass
