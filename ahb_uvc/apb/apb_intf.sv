interface apb_intf (input logic pclk, input logic prst);
	logic [31:0] paddr;
	logic psel;		//not sure
	logic penable;
	logic pwrite;
	logic [31:0] pwdata;
	logic pready;
	logic [31:0] prdata;
endinterface
