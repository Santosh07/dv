class apb_tx extends uvm_sequence_item;
	rand bit [31:0] paddr;
	rand bit [31:0] pdata;
	rand bit wr_rd;

	`uvm_object_utils_begin(apb_tx)
		`uvm_field_int(paddr, UVM_ALL_ON)
		`uvm_field_int(pdata, UVM_ALL_ON)
		`uvm_field_int(wr_rd, UVM_ALL_ON)
	`uvm_object_utils_end
endclass
