class apb_driver extends uvm_driver#(apb_tx);
	`uvm_component_utils(apb_driver)
	virtual apb_intf vif;		
	apb_tx atx;
	bit[31:0] rdataQ[$];
	bit flag;

	function new(string name="", uvm_component parent);
		super.new(name, parent);
	endfunction

	function void build_phase(uvm_phase phase);
		
		if (!uvm_config_db#(virtual apb_intf)::get(this, "", "pvif", vif)) begin
			`uvm_error("apb_driver", "interface nor registered to factory")
		end
//		atx = apb_tx::type_id::create("atx");
	endfunction

	task run_phase(uvm_phase phase);
		forever begin				//This has to be in forever loop. Otherwise it will run only once. 
			seq_item_port.get_next_item(req);				//get_next_item is blocking. If there are no more items in the sequence, it will be blocked
			drive_item(req);
			seq_item_port.item_done();
		end	
	endtask

	task drive_item(apb_tx atx);
		wait (vif.prst);		//Active low reset
		vif.paddr = atx.paddr;
		if (atx.wr_rd == 1) begin
			vif.pwdata = atx.pdata;
			vif.pwrite = atx.wr_rd;
			vif.penable = 1;
			flag = 0;
			while (flag==0) begin
				@(posedge vif.pclk) begin
					if (vif.pready==1) begin
						flag =1;
					end
				end
			end
			@(negedge vif.pclk);
			vif.penable=0;
		end
		else begin
			vif.pwrite=atx.wr_rd;
			vif.penable = 1;
			while (flag==0) begin
				@(posedge vif.pclk) begin
					if (vif.pready==1) begin
						rdataQ.push_back(vif.prdata);
						flag =1;
					end
				end
			end
			vif.penable=0;
			@(negedge vif.pclk);
		end
	endtask
endclass
