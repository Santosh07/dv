class apb_mon extends uvm_monitor;
	`uvm_component_utils(apb_mon)
	uvm_analysis_port#(apb_tx) ap_port;
	virtual apb_intf vif;
	apb_tx atx;
	
	function new(string name="", uvm_component parent);
		super.new(name, parent);
	endfunction

	function void build_phase(uvm_phase phase);
		uvm_config_db#(virtual apb_intf)::get(this, "", "pvif", vif);
	endfunction

	task run_phase(uvm_phase phase);
		forever begin
			@(posedge vif.pclk);
			if (vif.penable && vif.pready) begin
				atx = apb_tx::type_id::create("atx");
				atx.paddr=vif.paddr;
				atx.wr_rd=vif.wr_rd;
				if (vif.wr_rd) begin
					atx.pdata=vif.pwdata;
				end
				else begin
					atx.pdata=vif.prdata;
				end
			end	
			ap_port.write(atx);
		end
	endtask
endclass
