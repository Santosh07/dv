class top_sqr extends uvm_sequencer;			//This is a virtual sequencer. It is not parameterized, on the other hand, a normal sequencer is
	int num_masters;
	master_ahb_seq h_seq[];
	apb_sequencer p_seq;		

	`uvm_component_utils_begin(top_sqr)			//This is becaues virtual sequencer does not drive any item (so it does not have any item of its own). It only controls
		`uvm_field_int(num_masters, UVM_ALL_ON)
	`uvm_component_utils_end

	function new(string name="", uvm_component parent=null);
		super.new(name, parent);
	endfunction

	function void build_phase(uvm_phase phase);
		h_seq = new[num_masters];
	endfunction	
endclass
