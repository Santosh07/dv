module top;
	


	reg hclk, hrst;
	reg pclk, prst;
	
	ahb_intf mvif0(hclk, hrst);			//How to get varying number of master and slave interface
	ahb_intf mvif1(hclk, hrst);
	ahb_intf mvif2(hclk, hrst);
	ahb_intf svif0(hclk, hrst);
	ahb_intf svif1(hclk, hrst);
	ahb_intf svif2(hclk, hrst);
	apb_intf pvif(pclk, prst);

	
	//slave instantiation
/*	ahb_slave s0(			//The reason this is commented is because we will be coding the slave as a model and not as a module(we will be changing it from module to model)
		.clk(mvif0.hclk),	//Since, AHB is a model, it will be instantiated in the env and its run_phase will be called. I am not sure, this is what my understanding is? I am 
					//kind of guessing
		.rst(mvif1.hrst),
		.haddr(intf.haddr),
		.hbusreq(intf.hbusreq),
		.htrans(intf.htrans),
		.hwrite(intf.hwrite),
		.hsize(intf.size_t),
		.hburst(intf.hburst),
		.hprot(intf.hprot),
		.hwdata(intf.hwdata),
		.hrdata(intf.hrdata),
		.hready(intf.hready),
		.hresp(intf.hresp),
		.hlock(intf.hlock),
		.hgrant(intf.hgrant),
		.hmaster(intf.hmaster),
		.hmastlock(intf.hmastlock),
		.hsplit(intf.hsplit)
	);		*/

	
	initial begin
		uvm_config_db#(virtual ahb_intf)::set(uvm_root::get(), "*", "mvif0", mvif0);					//This is UVM style
		uvm_config_db#(virtual ahb_intf)::set(uvm_root::get(), "*", "mvif1", mvif1);					//This is UVM style
		uvm_config_db#(virtual ahb_intf)::set(uvm_root::get(), "*", "mvif2", mvif2);					//This is UVM style
		uvm_config_db#(virtual ahb_intf)::set(uvm_root::get(), "*", "svif0", svif0);					//This is UVM style
		uvm_config_db#(virtual ahb_intf)::set(uvm_root::get(), "*", "svif1", svif1);					//This is UVM style
		uvm_config_db#(virtual ahb_intf)::set(uvm_root::get(), "*", "svif2", svif2);					//This is UVM style
		uvm_config_db#(virtual apb_intf)::set(uvm_root::get(), "*", "pvif", pvif);					//This is UVM style
		//The interface is being registered in the config_db such that any one can access it (driver is gonne access it)
	end
//	initial begin
		//how to connect the sequencer with the driver
		//This has to be done in ahb_agent
//	end

	initial begin
		hclk = 0;
		forever begin
			#5 hclk = ~hclk;
		end
	end

	initial begin
		hrst = 1;
		repeat (2) @(posedge hclk); 
		hrst = 0; 
	end

	initial begin
		pclk=0;
		$display("generating pclk");
		forever begin
			#5 pclk = ~pclk;
		end
	end

	initial begin
		prst = 0;
		repeat(2) @(posedge pclk);
		prst = 1;
	end
	`include "test_lib.sv"			//Very important: this has to be at the end otherwise everything above will not happen
	
	initial begin
		run_test();			//this function is present in uvm_globals and so can be called from anywhere as it is global. This function in turn calls the
						//run_test of uvm_root. The purpose of calling this function is it calls all the phases one by one from all the component
	end
endmodule
