class ahb_test extends uvm_test;
	`uvm_component_utils(ahb_test)
//	ahb_env env;
	ahb_ic_env env;

	uvm_table_printer printer;


	function new(string name="", uvm_component parent);
		super.new(name, parent);
	endfunction

	function void build_phase(uvm_phase phase);
		printer = new();
		env = ahb_ic_env::type_id::create("env", this);//build env as it is one level below
		`uvm_info("ahb_test", "build_phase", UVM_LOW)
//		uvm_config_db#(uvm_object_wrapper)::set(this, "env.agent.sqr.run_phase", "default_sequence", ahb_complete_seq::get_type());	//in the run phase of sequencer,
																		//complete sequence that includes reset, config
																		//main sequence gets called
	endfunction

	function void end_of_elaboration_phase(uvm_phase phase);
		`uvm_info(get_type_name(), $psprintf("ahb_base_test hierarchy is %s", this.sprint(printer)), UVM_LOW)
	endfunction

	task run_phase(uvm_phase phase);			//if I run the child of this test, does both(parent & child) run_phase gets called or is it just child's run_phase gets called?
		`uvm_info("ahb_test", "run_phase", UVM_LOW)
//		uvm_config_db#(uvm_object_wrapper)::set(this, "env.agent.seq.run_phase", "default_sequence", ahb_complete_seq::get_type());		
	endtask
endclass

class mas_3_sla_3_R_test extends ahb_test;
	`uvm_component_utils(mas_3_sla_3_R_test)
	apb_config_reg_seq apb_config_seq;
	ahb_wr_rd_burst_seq0 ahb_wr_rd_seq0;	
	ahb_wr_rd_burst_seq1 ahb_wr_rd_seq1;	
	ahb_wr_rd_burst_seq2 ahb_wr_rd_seq2;	
	
	function new(string name="", uvm_component parent=null);
		super.new(name, parent);
	endfunction

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		ahb_cfg::num_masters = 1;	//These are done to set num_masters, num_slaves so that the tx can generate the correct address range. It is part of constraint
		ahb_cfg::num_slaves = 1;
		uvm_config_db#(int)::set(this, "*", "num_masters", 1);
		uvm_config_db#(int)::set(this, "*", "num_slaves", 1);
		`uvm_info("mas_3_sla_3_R_test", "build_phase", UVM_LOW)
	endfunction
	
	task run_phase(uvm_phase phase);
			apb_config_seq = apb_config_reg_seq::type_id::create("apb_config_seq");
			ahb_wr_rd_seq0 = ahb_wr_rd_burst_seq0::type_id::create("ahb_wr_rd_seq0");
//			ahb_wr_rd_seq1 = ahb_wr_rd_burst_seq1::type_id::create("ahb_wr_rd_seq1");
//			ahb_wr_rd_seq2 = ahb_wr_rd_burst_seq2::type_id::create("ahb_wr_rd_seq2");



		phase.raise_objection(this);
			`uvm_info("mas_3_sla_3_R_test", "run_phase", UVM_LOW)

			//Start the sequence by name_of_sequence.start(sequencer_on_which_sequence_should_run)
			apb_config_seq.start(env.pagent.p_seq);	//PV			
			ahb_wr_rd_seq0.start(env.magent[0].seq);		//how do i run it in all three sequencers
	//		ahb_wr_rd_seq.start(env.magent[1].sqr);		//how do i run it in all three sequencers
	//		ahb_wr_rd_seq.start(env.magent[2].sqr);		//how do i run it in all three sequencers			
	//		ahb_wr_rd_seq.start(env.magent*.seq);		//This one statement is equivalent to the above three statement. Sir is using this. This is giving error. Not sure why?
	/*		fork 
				ahb_wr_rd_seq0.start(env.magent[0].seq);		//When I had one common sequence for all. It did not work.
				ahb_wr_rd_seq1.start(env.magent[1].seq);		//Trying to create three diff. sequence which would basically be same(functionality), 
										//only name would be different. No even that did not work
				ahb_wr_rd_seq2.start(env.magent[2].seq);					
			join	*/
		phase.drop_objection(this);
	endtask
endclass

class mas_3_sla_3_b_test extends ahb_test;
	`uvm_component_utils(mas_3_sla_3_b_test)
//	apb_config_reg_seq apb_config_seq;
//	ahb_wr_rd_burst_seq ahb_wr_rd_seq;	
	function new(string name="", uvm_component parent=null);
		super.new(name, parent);
	endfunction

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		uvm_config_db#(int)::set(this, "*", "num_masters", 3);
		uvm_config_db#(int)::set(this, "*", "num_slaves", 3);
		ahb_cfg::num_masters = 3;
		ahb_cfg::num_slaves = 3;
		//apb_config_seq = apb_config_reg_seq::type_id::create("apb_config_seq");
		//ahb_wr_rd_seq = ahb_wr_rd_burst_seq::type_id::create("ahb_wr_rd_seq");
		uvm_config_db#(uvm_object_wrapper)::set(this, "env.pagent.p_seq.config_phase", "default_sequence", apb_config_reg_seq::get_type());//NOTE: In the configure phase of the test,
													//run the apb_config_seq on Pagent sequencer
		uvm_config_db#(uvm_object_wrapper)::set(this, "env.magent*.seq.main_phase", "default_sequence", ahb_wr_rd_burst_seq::get_type());//NOTE: In the main phase of the test,
													//run the ahb_wr_rd_seq on Magent sequencer
	endfunction
endclass

/*
class ahb_wr_rd_burst extends ahb_test;
	`uvm_component_utils(ahb_wr_rd_burst)
	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		//We can set the verbosity of a particular component directly from the testbench
		//env.agent.drv.set_report_verbosity_level(UVM_MEDIUM)	//This will set the verbosity of all in driver as UVM_MEDIUM
		//env.agent.drv.set_report_id_verbosity("ahb_driver", UVM_LOW)		//This will set the verbosity of that particular ID as UVM_LOW
		uvm_config_db#(int)::set(this, "env.agent.drv", "master_no", 0);		//setting master_no to 0 which is in the driver class
	endfunction

	task run_phase(uvm_phase phase);
		ahb_wr_rd_burst_seq wr_rd_burst_seq1;//instantiate the sequence
		wr_rd_burst_seq1 = ahb_wr_rd_burst_seq::type_id::create("wr_rd_burst_seq1");
		phase.phase_done.set_drain_time(this, 100);
		phase.raise_objection(this);
		wr_rd_burst_seq1.start(env.agent.seq);
		phase.drop_objection(this);
	endtask
endclass
*/

class ahb_ic_vtest extends ahb_test;
	`uvm_component_utils(ahb_ic_vtest)
	function new(string name="", uvm_component parent=null);
		super.new(name, parent);
	endfunction
	ahb_ic_seq_lib main_seq;

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		ahb_cfg::num_masters = 1;	//These are done to set num_masters, num_slaves so that the tx can generate the correct address range. It is part of constraint
		ahb_cfg::num_slaves = 1;
		uvm_config_db#(int)::set(this, "*", "num_masters", 1);
		uvm_config_db#(int)::set(this, "*", "num_slaves", 1);
		`uvm_info("mas_3_sla_3_R_test", "build_phase", UVM_LOW)
	endfunction

	task run_phase(uvm_phase phase);
		main_seq = ahb_ic_seq_lib::type_id::create("main_seq");
		phase.raise_objection(this);
			main_seq.start(null); 	
		phase.drop_objection(this);
	endtask
endclass
