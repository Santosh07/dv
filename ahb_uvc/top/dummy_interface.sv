interface ahb_intf(input logic clk, input logic rst);
	logic [7:0] haddr;
	logic [7:0] data;
	logic hwrite;
	logic [15:0] hbusreq; 
endinterface

module ic(input clk, 
	input rst,
	input [7:0] haddr_i,
	input [7:0] data_i,
	input hwrite,
	input [15:0] hbusreq,
	output [15:0] hgrant,
	output [7:0] haddr_i,
	output [7:0] data_i,
	output hwrite,
	output [15:0] ss);
endmodule
