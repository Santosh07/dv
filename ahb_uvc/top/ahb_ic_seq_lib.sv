class ahb_ic_seq_base_lib extends uvm_sequence;
	`uvm_object_utils(ahb_ic_seq_base_lib)
	function new(string name="");
		super.new(name);
	endfunction

	task pre_body();
		if (starting_phase != null) begin
			starting_phase.raise_objection(this);
		end
	endtask

	task post_body();
		if (starting_phase != null) begin
			starting_phase.drop_objection(this);
		end
	endtask
endclass

class ahb_ic_seq_lib extends ahb_ic_seq_base_lib;
	`uvm_object_utils(ahb_ic_seq_lib)	
	function new(string name="");
		super.new(name);
	endfunction	
	
	apb_config_reg_seq apb_config_seq;
	ahb_wr_rd_burst_seq ahb_wr_rd_seq0;
//	ahb_wr_rd_burst_seq ahb_wr_rd_seq1;
//	ahb_wr_rd_burst_seq ahb_wr_rd_seq2;
	top_sqr t_sqr;	
	
	`uvm_declare_p_sequencer(top_sqr)

	task body();
		`uvm_do_on(apb_config_seq, p_sequencer.p_seq)
		fork 
			`uvm_do_on(ahb_wr_rd_seq0, p_sequencer.h_seq[0])
//			`uvm_do_on(ahb_wr_rd_seq1, p_sequencer.h_seq[1])
//			`uvm_do_on(ahb_wr_rd_seq2, p_sequencer.h_seq[2])
		join
	endtask
endclass
