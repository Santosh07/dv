onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /top/pvif/pclk
add wave -noupdate /top/pvif/prst
add wave -noupdate -radix hexadecimal /top/pvif/paddr
add wave -noupdate /top/pvif/psel
add wave -noupdate /top/pvif/penable
add wave -noupdate /top/pvif/pwrite
add wave -noupdate -radix hexadecimal /top/pvif/pwdata
add wave -noupdate /top/pvif/pready
add wave -noupdate /top/pvif/prdata
add wave -noupdate /top/mvif0/hclk
add wave -noupdate /top/mvif0/hrst
add wave -noupdate -radix hexadecimal /top/mvif0/haddr
add wave -noupdate /top/mvif0/htrans
add wave -noupdate /top/mvif0/hwrite
add wave -noupdate /top/mvif0/size_t
add wave -noupdate /top/mvif0/hburst
add wave -noupdate /top/mvif0/hprot
add wave -noupdate -radix hexadecimal /top/mvif0/hwdata
add wave -noupdate /top/mvif0/hsel
add wave -noupdate -radix hexadecimal /top/mvif0/hrdata
add wave -noupdate /top/mvif0/hready
add wave -noupdate /top/mvif0/hresp
add wave -noupdate -radix hexadecimal /top/mvif0/hbusreq
add wave -noupdate -radix hexadecimal /top/mvif0/hlock
add wave -noupdate -radix hexadecimal /top/mvif0/hgrant
add wave -noupdate /top/mvif0/hmaster
add wave -noupdate /top/mvif0/hmastlock
add wave -noupdate /top/mvif0/hsplit
add wave -noupdate /top/svif0/hclk
add wave -noupdate /top/svif0/hrst
add wave -noupdate -radix hexadecimal /top/svif0/haddr
add wave -noupdate /top/svif0/htrans
add wave -noupdate /top/svif0/hwrite
add wave -noupdate /top/svif0/size_t
add wave -noupdate /top/svif0/hburst
add wave -noupdate /top/svif0/hprot
add wave -noupdate -radix hexadecimal /top/svif0/hwdata
add wave -noupdate /top/svif0/hsel
add wave -noupdate -radix hexadecimal /top/svif0/hrdata
add wave -noupdate /top/svif0/hready
add wave -noupdate /top/svif0/hresp
add wave -noupdate -radix hexadecimal /top/svif0/hbusreq
add wave -noupdate /top/svif0/hlock
add wave -noupdate -radix hexadecimal /top/svif0/hgrant
add wave -noupdate /top/svif0/hmaster
add wave -noupdate /top/svif0/hmastlock
add wave -noupdate /top/svif0/hsplit
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ns} 0}
configure wave -namecolwidth 176
configure wave -valuecolwidth 40
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {350 ns} {455 ns}
