library verilog;
use verilog.vl_types.all;
entity ahb_intf is
    port(
        hclk            : in     vl_logic;
        hrst            : in     vl_logic
    );
end ahb_intf;
