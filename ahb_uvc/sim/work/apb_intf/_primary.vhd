library verilog;
use verilog.vl_types.all;
entity apb_intf is
    port(
        pclk            : in     vl_logic;
        prst            : in     vl_logic
    );
end apb_intf;
