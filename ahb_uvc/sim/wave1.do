onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /top/mvif0/hclk
add wave -noupdate /top/mvif0/hrst
add wave -noupdate -radix hexadecimal /top/mvif0/haddr
add wave -noupdate /top/mvif0/htrans
add wave -noupdate /top/mvif0/hwrite
add wave -noupdate /top/mvif0/size_t
add wave -noupdate /top/mvif0/hburst
add wave -noupdate /top/mvif0/hprot
add wave -noupdate -radix hexadecimal /top/mvif0/hwdata
add wave -noupdate /top/mvif0/hsel
add wave -noupdate -radix hexadecimal /top/mvif0/hrdata
add wave -noupdate /top/mvif0/hready
add wave -noupdate /top/mvif0/hresp
add wave -noupdate /top/mvif0/hbusreq
add wave -noupdate /top/mvif0/hlock
add wave -noupdate /top/mvif0/hgrant
add wave -noupdate /top/mvif0/hmaster
add wave -noupdate /top/mvif0/hmastlock
add wave -noupdate /top/mvif0/hsplit
add wave -noupdate /top/mvif0/cb/hclk
add wave -noupdate /top/mvif0/cb/hsplit
add wave -noupdate /top/mvif0/cb/hmastlock
add wave -noupdate /top/mvif0/cb/hmaster
add wave -noupdate /top/mvif0/cb/hgrant
add wave -noupdate /top/mvif0/cb/hlock
add wave -noupdate /top/mvif0/cb/hbusreq
add wave -noupdate /top/mvif0/cb/hresp
add wave -noupdate /top/mvif0/cb/hready
add wave -noupdate /top/mvif0/cb/hrdata
add wave -noupdate /top/mvif0/cb/hsel
add wave -noupdate /top/mvif0/cb/hwdata
add wave -noupdate /top/mvif0/cb/hprot
add wave -noupdate /top/mvif0/cb/hburst
add wave -noupdate /top/mvif0/cb/size_t
add wave -noupdate /top/mvif0/cb/hwrite
add wave -noupdate /top/mvif0/cb/htrans
add wave -noupdate /top/mvif0/cb/haddr
add wave -noupdate /top/mvif0/cb/cb_event
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {95 ns} 0}
configure wave -namecolwidth 150
configure wave -valuecolwidth 40
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {38 ns} {146 ns}
