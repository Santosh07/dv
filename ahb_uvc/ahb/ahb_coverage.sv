class ahb_coverage extends uvm_subscriber#(ahb_tx);
	`uvm_component_utils(ahb_coverage)
//	uvm_analysis_imp#(ahb_tx, ahb_coverage) ap_imp;	
	
	ahb_tx t;

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
//		ap_imp = new("ap_imp", this);
	endfunction

	function void write(ahb_tx t);
		$display("-------------------------------COVERAGE-------------------------------");
		t.print();	
		$display("-------------------------------COVERAGE-------------------------------");
//		ahb_cg.sample();
	endfunction

	covergroup ahb_cg;
		HADDR: coverpoint t.haddr{
			bins S0_SA_corner = {`S0_START_ADDR};
			bins S0_EA_corner = {`S0_END_ADDR};
			bins S1_SA_corner = {`S1_START_ADDR};
			bins S1_EA_corner = {`S1_END_ADDR};
			bins S2_SA_corner = {`S2_START_ADDR};
			bins S2_EA_corner = {`S2_END_ADDR};
			bins S0 = {[`S0_START_ADDR:`S0_END_ADDR]}; 
			bins S1 = {[`S1_START_ADDR:`S1_END_ADDR]};
			bins S2 = {[`S2_START_ADDR:`S2_END_ADDR]};
		}

		HWRITE: coverpoint t.wr_rd{
			bins READ = {1'b0};
			bins WRITE = {1'b1};
		}

		HBURST: coverpoint t.burst_typ{
			bins S = {SINGLE};
			bins I = {INCR};
			bins I4 = {INCR4};
			bins W4 = {WRAP4};
			bins I8 = {INCR8};
			bins W8 = {WRAP8};
			bins I16 = {INCR16};
			bins W16 = {WRAP16};
		}

		HSIZE_1: coverpoint t.size{
			bins B1 = {BYTE_1};
			bins B2 = {BYTE_2};
			bins B4 = {BYTE_4};
			bins B8 = {BYTE_8};
			bins B16 = {BYTE_16};
			bins B32 = {BYTE_32};
			bins B64 = {BYTE_64};
			bins B128 = {BYTE_128};	
		}	
	endgroup
	
	function new(string name="", uvm_component parent=null);
		super.new(name, parent);
		ahb_cg = new();
	endfunction
endclass
