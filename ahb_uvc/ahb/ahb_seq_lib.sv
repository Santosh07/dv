class ahb_base_seq extends uvm_sequence#(ahb_tx);		//in ahb_base_seq we will have the code that will be common to all the sequemces like raising, dropping objections
	`uvm_object_utils(ahb_base_seq)

	function new(string name="");
		super.new(name);
	endfunction
	
	task pre_body();
		`uvm_info("ahb_base_seq", "pre_body", UVM_LOW)
		if (starting_phase != null) starting_phase.raise_objection(this);
	endtask

	task post_body();
		`uvm_info("ahb_base_seq", "post_body", UVM_LOW)
		if (starting_phase != null) starting_phase.drop_objection(this);
	endtask
endclass

class ahb_reset_seq extends ahb_base_seq;
	`uvm_object_utils(ahb_reset_seq)

	function new(string name="");
		super.new(name);
	endfunction

	task body();
		`uvm_info("ahb_reset_seq", "Applying reset", UVM_LOW)
	endtask
endclass

class ahb_configure_seq extends ahb_base_seq;
	`uvm_object_utils(ahb_configure_seq)

	function new(string name="");
		super.new(name);
	endfunction

	task body();
		`uvm_info("ahb_seq", "config_phase", UVM_LOW)
	endtask
endclass

class ahb_main_seq extends ahb_base_seq;
	`uvm_object_utils(ahb_main_seq)
	
	function new(string name="");
		super.new(name);
	endfunction

	task body();
		`uvm_info("ahb_seq", "main_phase", UVM_LOW)
		repeat(5) begin
			`uvm_do(req)			//how does it come to know that req is the type of ahb_tx. Because we are parameterizing it to ahb_tx. REQ req so REQ is now
							//replaced with ahb_tx. So req becomes a type of ahb_tx
		end
	endtask
endclass

class ahb_complete_seq extends ahb_base_seq;
	`uvm_object_utils(ahb_complete_seq)
	ahb_reset_seq ars;
	ahb_configure_seq acs;
	ahb_main_seq ams;	

	function new(string name="");
		super.new(name);
	endfunction

	task body();
		ars = ahb_reset_seq::type_id::create("ars");
		acs = ahb_configure_seq::type_id::create("acs");
		ams = ahb_main_seq::type_id::create("ams");
		
		`uvm_do(ars)
		`uvm_do(acs)
		`uvm_do(ams)
	endtask

endclass

class ahb_wr_rd_burst_seq0 extends ahb_base_seq;
	ahb_tx req_t;
	`uvm_object_utils(ahb_wr_rd_burst_seq0)
	function new(string name = "");
		super.new(name);
	endfunction

	task body();
		repeat (1) begin
			`uvm_do_with(req, {req.burst_typ != SINGLE; 
					   req.wr_rd==1'b1;})
			//`uvm_do(req)
		end
		$cast(req_t, req);		//Note: a new req gets created when we do 
		`uvm_info("ahb_seq_lib", $psprintf("haddr=%h", req_t.haddr), UVM_LOW)
		repeat(1) begin
			`uvm_do_with(req, {req.burst_typ == req_t.burst_typ;		//`uvm_do: does all of these
											//- It creates the transactions
											//- waits for the grants from the sequencer. The sequencer gives 																//  grant to the sequence once it receives a request from the driver
											//  for the next item. The driver does this using get_next_item()																//- Once it gets the grant from the sequencer, it randomizes the tx																//- Then it sends the tx to the Sequencer  
																					
					   req.haddr == req_t.haddr;
					   req.size == req_t.size;
					   req.wr_rd == 1'b0;})
		end	
	endtask
endclass

class ahb_wr_rd_burst_seq1 extends ahb_base_seq;
	ahb_tx req_t;
	`uvm_object_utils(ahb_wr_rd_burst_seq1)
	function new(string name = "");
		super.new(name);
	endfunction

	task body();
		repeat (1) begin
			`uvm_do_with(req, {req.burst_typ != SINGLE; 
					   req.wr_rd==1'b1;})
			//`uvm_do(req)
		end
		$cast(req_t, req);		//Note: a new req gets created when we do 
		`uvm_info("ahb_seq_lib", $psprintf("haddr=%h", req_t.haddr), UVM_LOW)
		repeat(1) begin
			`uvm_do_with(req, {req.burst_typ == req_t.burst_typ;		//`uvm_do: does all of these
											//- It creates the transactions
											//- waits for the grants from the sequencer. The sequencer gives 																//  grant to the sequence once it receives a request from the driver
											//  for the next item. The driver does this using get_next_item()																//- Once it gets the grant from the sequencer, it randomizes the tx																//- Then it sends the tx to the Sequencer  
																					
					   req.haddr == req_t.haddr;
					   req.size == req_t.size;
					   req.wr_rd == 1'b0;})
		end	
	endtask
endclass

class ahb_wr_rd_burst_seq2 extends ahb_base_seq;
	ahb_tx req_t;
	`uvm_object_utils(ahb_wr_rd_burst_seq2)
	function new(string name = "");
		super.new(name);
	endfunction

	task body();
		repeat (1) begin
			`uvm_do_with(req, {req.burst_typ != SINGLE; 
					   req.wr_rd==1'b1;})
			//`uvm_do(req)
		end
		$cast(req_t, req);		//Note: a new req gets created when we do 
		`uvm_info("ahb_seq_lib", $psprintf("haddr=%h", req_t.haddr), UVM_LOW)
		repeat(1) begin
			`uvm_do_with(req, {req.burst_typ == req_t.burst_typ;		//`uvm_do: does all of these
											//- It creates the transactions
											//- waits for the grants from the sequencer. The sequencer gives 																//  grant to the sequence once it receives a request from the driver
											//  for the next item. The driver does this using get_next_item()																//- Once it gets the grant from the sequencer, it randomizes the tx																//- Then it sends the tx to the Sequencer  
																					
					   req.haddr == req_t.haddr;
					   req.size == req_t.size;
					   req.wr_rd == 1'b0;})
		end	
	endtask
endclass

class ahb_wr_rd_burst_seq extends ahb_base_seq;
	ahb_tx req_t;
	`uvm_object_utils(ahb_wr_rd_burst_seq)
	function new(string name = "");
		super.new(name);
	endfunction

	task body();
		repeat (1) begin
			`uvm_do_with(req, {req.burst_typ != SINGLE; 
					   req.wr_rd==1'b1;})
			//`uvm_do(req)
		end
		$cast(req_t, req);		//Note: a new req gets created when we do 
		`uvm_info("ahb_seq_lib", $psprintf("haddr=%h", req_t.haddr), UVM_LOW)
		repeat(1) begin
			`uvm_do_with(req, {req.burst_typ == req_t.burst_typ;		//`uvm_do: does all of these
											//- It creates the transactions
											//- waits for the grants from the sequencer. The sequencer gives 																//  grant to the sequence once it receives a request from the driver
											//  for the next item. The driver does this using get_next_item()																//- Once it gets the grant from the sequencer, it randomizes the tx																//- Then it sends the tx to the Sequencer  
																					
					   req.haddr == req_t.haddr;
					   req.size == req_t.size;
					   req.wr_rd == 1'b0;})
		end	
	endtask
endclass
///Virtual Sequencer is like a controller that controls when should which sequencer run their sequences
//Its like a top-level manager that controls things
//It does not have any sequence of its own. It only does the managing part as to running different sequences at the appropriate time

/*
class ahb_top_wr_rd_seq extends uvm_sequence;			//Virtual Sequence-> will not be paramterized. Thats how a normal sequence(parameterized) is distinguished 
								//from a virtual sequence
	`uvm_declare_p_sequencer(ahb_top_sqr)			//ahb_top_sqr is the type of the parent virtual sequence. An object called p_sequencer is also created over here internally
								//i.e. ahb_top_sqr p_sequencer
								//     p_sequencer=ahb_top_sqr::type_id::create("p_sequencer", this);
	task body();
		fork 
			`uvm_do_on(apb_config_seq, p_sequencer.apb_sqr);	//Here p_sequencer is the object of ahb_top_sqr
			`uvm_do_on(apb_config1_seq, p_sequencer.apb_sqr);
		join
			`uvm_do_on(ahb_wr_rd_seq, p_sequencer.ahb_sqr);
	endtask
endclass
*/


