class slave_ahb_agent extends uvm_agent;
	`uvm_component_utils_begin(slave_ahb_agent)
	`uvm_component_utils_end

	slave_ahb_driver s_drv;
	ahb_monitor mon;

	function new(string name="", uvm_component parent=null);
		super.new(name, parent);
	endfunction

	function void build_phase(uvm_phase phase);
		s_drv = slave_ahb_driver::type_id::create("s_drv", this);
		mon = ahb_monitor::type_id::create("mon", this);
	endfunction
endclass
