class ahb_monitor extends uvm_monitor;
	int master_no;
	int slave_no;				//please verify(This is me trying out. For all that I am trying out. I am having PV(please verify next to the code))
	bit mas_slv_f;
	ahb_tx tx=new();;
	bit grant_f;
	htrans_t prev_trans;	
	string inst_name;
	uvm_analysis_port#(ahb_tx) ap_port;

	virtual ahb_intf vif;
	string inst_name_m;	//PV
	string inst_name_s;	//PV

	`uvm_component_utils_begin(ahb_monitor)
		`uvm_field_int(master_no, UVM_ALL_ON)		//now we need not retrieve master_no using uvm_config_db#(int)::get(this, "env.agent.drv", "master_no", master_no)
		`uvm_field_int(slave_no, UVM_ALL_ON)		//PV
		`uvm_field_int(mas_slv_f, UVM_ALL_ON)
	`uvm_component_utils_end

	function new(string name="", uvm_component parent=null);
		super.new(name, parent);
	endfunction
	
	function void build_phase(uvm_phase phase);
		`uvm_info("ahb_monitor", "build_phase", UVM_LOW)
		super.build_phase(phase);
		ap_port = new("ap_port", this);
		if (mas_slv_f) begin
			$sformat(inst_name, "mvif%0d", master_no);				//PV
			uvm_config_db#(virtual ahb_intf)::get(this, "", inst_name, vif);
		end
		else begin
			$sformat(inst_name, "svif%0d", slave_no);				//PV
			uvm_config_db#(virtual ahb_intf)::get(this, "", inst_name, vif);
		end
	endfunction

	function void connect_phase(uvm_phase phase);
		`uvm_info("ahb_monitor", "connect_phase", UVM_LOW)
	endfunction

	task run_phase(uvm_phase phase);
		//monitor various phases
		//know which master is currently in control
		//monitor address phase
		//monitor data phase
		//collect all the phase info in ahb_tx object

		
		grant_f = 0;
		prev_trans = IDLE;
		forever begin
//			@(posedge vif.hclk) begin
		//========Moniotr arbitrtation phase=================
			@(posedge vif.hclk) begin			//PV
				if (vif.hbusreq[master_no] && vif.hgrant[master_no]) begin					//We need to monitor the signals only after the grant is given
																//which makes sense
					`uvm_info("ahb_monitor", $psprintf("Master_no %d got the grant", master_no), UVM_LOW)
					grant_f = 1;
				end
		//===arbitration phase monitoring ends==================

		//====Monitoring address and data phase (need to be monitored together coz of pipelining)==================
				if (grant_f == 1) begin
					case(vif.htrans) 
						IDLE: begin
							//no address phase
							if (vif.hwrite == 1'b1) begin
								if (prev_trans inside {SEQ, NONSEQ}) begin
									tx.dataQ.push_back(vif.hwdata);
								end
							end
							else begin
								if (prev_trans inside {SEQ, NONSEQ}) begin
									tx.dataQ.push_back(vif.hrdata);
								end
							end
							prev_trans = IDLE;
						end

						BUSY: begin
							//no address phase
							if (vif.hwrite == 1'b1) begin
								if (prev_trans inside {SEQ, NONSEQ}) begin
									tx.dataQ.push_back(vif.hwdata);
								end
							end
							else begin
								if (prev_trans inside {SEQ, NONSEQ}) begin
									tx.dataQ.push_back(vif.hrdata);
								end
							end
							prev_trans = BUSY;
						end

						NONSEQ: begin
							prev_trans = NONSEQ;
							tx.haddr = vif.haddr;
							tx.hprot = vif.hprot;
							tx.wr_rd= vif.hwrite;	
							//tx.size = vif.hsize_t;
							tx.burst_typ = vif.hburst;
							if (vif.hwrite == 1'b1) begin			//write phase
								if (prev_trans inside {SEQ, NONSEQ}) begin
									tx.dataQ.push_back(vif.hwdata);
								end	
							end
							else begin					//read phase
								if (prev_trans inside {SEQ, NONSEQ}) begin
									tx.dataQ.push_back(vif.hrdata);
								end
							end
							//write it to the analysis port to be sent to the coverage and the scoreboard
							$display("---------------------------MONITOR---------------------------");
							tx.print();
							$display("MONITOR=%p", tx);
							$display("---------------------------MONITOR---------------------------");	
							ap_port.write(tx);
							tx = new();
							prev_trans = NONSEQ;
							tx.haddr = vif.haddr;
							tx.hprot = vif.hprot;
							//tx.size = vif.hsize_t;
							tx.burst_typ = vif.hburst;
							tx.wr_rd= vif.hwrite;
						end

						SEQ: begin
							if (vif.hwrite == 1'b1) begin			//write phase
								if (prev_trans inside {SEQ, NONSEQ}) begin
									tx.dataQ.push_back(vif.hwdata);
								end	
							end
							else begin					//read phase
								if (prev_trans inside {SEQ, NONSEQ}) begin
									tx.dataQ.push_back(vif.hrdata);
								end
							end
							//write it to the analysis port to be sent to the coverage and the scoreboard
							//prev_trans = SEQ;
							//tx.haddr = vif.haddr;
							//tx.hprot = vif.hprot;
							//tx.wr_rd = vif.hwrite;
						end
					endcase
				end
	
			end
		end
	endtask
endclass



//Review on how to connect monitor and the coverage model
/*
mon.ap_port.connect(cov.ap_imp);

uvm_analysis_port#(ahb_tx) ap_port;
uvm_analysis_imp#(ahb_tx, ahb_ref_model) ap_imp;

mon.ap_port.connect(cov.ap_imp);

ap_port.write(tx);

function void write(ahb_tx tx);
endfunction
*/
