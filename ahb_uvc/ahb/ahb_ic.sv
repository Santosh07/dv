class ahb_ic extends uvm_component;
	virtual apb_intf pvif;
	virtual ahb_intf mvif[];
	virtual ahb_intf svif[];
	int num_masters, num_slaves;		//Number of masters, slaves in the environment. Will be set from the build_phase of test_lib

	int master_active, slave_active;

	string inst_name;
	
	logic [31:0] s0_start;
	logic [31:0] s0_end;
	logic [31:0] s1_start;
	logic [31:0] s1_end;
	logic [31:0] s2_start;
	logic [31:0] s2_end;

	`uvm_component_utils_begin(ahb_ic)
		`uvm_field_int(num_masters, UVM_ALL_ON)
		`uvm_field_int(num_slaves, UVM_ALL_ON)
	`uvm_component_utils_end	

	function new(string name="", uvm_component parent=null);
		super.new(name, parent);
	endfunction

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
	
		mvif = new[num_masters];
		svif = new[num_slaves];	

		for (int i =0; i<num_masters; i++) begin
			$sformat(inst_name, "mvif%0d", i);
			uvm_config_db#(virtual ahb_intf)::get(this,"" , inst_name, mvif[i]);
		end

		for (int i =0; i<num_slaves; i++) begin
			$sformat(inst_name, "svif%0d", i);	//it assigns inst_name=mvif0 or mvif1 based on which iteration we are
			uvm_config_db#(virtual ahb_intf)::get(this,"" , inst_name, svif[i]);
		end

		if (!uvm_config_db#(virtual apb_intf)::get(this,"" , "pvif", pvif)) begin
			`uvm_error("ahb_ic", "apb_interface not registered")
		end
	endfunction

	task run_phase(uvm_phase phase);
		fork
		begin
			while(1) begin
				if (num_masters==1) begin
					@(posedge mvif[0].hclk);
					if (mvif[0].hbusreq) begin
						mvif[0].hgrant = 1;
					end
					else begin
						mvif[0].hgrant = 1;		//Default Master						
					end
				end
				else if (num_masters==2) begin
					@(posedge mvif[0].hclk);
					if (mvif[0].hbusreq) begin
						mvif[0].hgrant = 1;
					end
					else if (mvif[1].hbusreq) begin
						mvif[1].hgrant = 1;
					end
					else begin
						mvif[0].hgrant = 1;		//Default Master						
					end
	
				end
				else if (num_masters==3) begin
					@(posedge mvif[0].hclk);
					if (mvif[0].hbusreq) begin
						mvif[0].hgrant = 1;
					end
					else if (mvif[1].hbusreq) begin
						mvif[1].hgrant = 1;
					end
					else if (mvif[2].hbusreq) begin
						mvif[2].hgrant = 1;
					end
					else begin
						mvif[0].hgrant = 1;		//Default Master						
					end
				end
			end
		end
		
		begin
			while (1) begin					//Configuring the slave start and end address 
			@(posedge pvif.pclk);
			if (pvif.penable) begin
				pvif.pready = 1;
				if (pvif.pwrite) begin
				case(pvif.paddr)
					`S0_START_REG_ADDR: begin
						s0_start = pvif.pwdata;		//please define s0_start as logic. Had it been verilog, we would have declared it as reg
					end		
					`S0_END_REG_ADDR: begin
						s0_end = pvif.pwdata;
					end
					`S1_START_REG_ADDR: begin
						s1_start = pvif.pwdata;		//please define s0_start as logic. Had it been verilog, we would have declared it as reg
					end			
					`S1_END_REG_ADDR: begin
						s1_end = pvif.pwdata;
					end
					`S2_START_REG_ADDR: begin
						s2_start = pvif.pwdata;
					end
					`S2_END_REG_ADDR: begin
						s2_end = pvif.pwdata;
					end
				endcase	
				end
			end
			end
		end
			
		begin
			while (1) begin
				if (num_masters==1) begin
				@(posedge mvif[0].hclk);
				if (mvif[0].hgrant[0]) begin
					master_active = 0;
					if (mvif[0].haddr>=s0_start && mvif[0].haddr<=s0_end) begin
						slave_active = 0; end
					else if (mvif[0].haddr>=s1_start && mvif[0].haddr<=s1_end) begin
						slave_active = 1; end
					else if (mvif[0].haddr>=s2_start && mvif[0].haddr<=s2_end) begin
						slave_active = 2; end
				end
				end
	
				else if (num_masters==2) begin
				@(posedge mvif[0].hclk);
				if (mvif[0].hgrant[0]) begin
					master_active = 0;
					if (mvif[0].haddr>=s0_start && mvif[0].haddr<=s0_end) begin
						slave_active = 0; end
					else if (mvif[0].haddr>=s1_start && mvif[0].haddr<=s1_end) begin
						slave_active = 1; end
					else if (mvif[0].haddr>=s2_start && mvif[0].haddr<=s2_end) begin
						slave_active = 2; end
				end
	
				else if (mvif[1].hgrant[1]) begin
					master_active = 1;
					if (mvif[1].haddr>=s0_start && mvif[0].haddr<=s0_end) begin
						slave_active = 0;  end
					else if (mvif[1].haddr>=s1_start && mvif[0].haddr<=s1_end) begin
						slave_active = 1;  end
					else if (mvif[2].haddr>=s2_start && mvif[0].haddr<=s2_end) begin
						slave_active = 2;  end
				end
				end

				else if (num_masters==3) begin
				@(posedge mvif[0].hclk);
				if (mvif[0].hgrant[0]) begin
					master_active = 0;
					if (mvif[0].haddr>=s0_start && mvif[0].haddr<=s0_end) begin
						slave_active = 0; end
					else if (mvif[0].haddr>=s1_start && mvif[0].haddr<=s1_end) begin
						slave_active = 1; end
					else if (mvif[0].haddr>=s2_start && mvif[0].haddr<=s2_end) begin
						slave_active = 2; end
				end
	
				else if (mvif[1].hgrant[1]) begin
					master_active = 1;
					if (mvif[1].haddr>=s0_start && mvif[0].haddr<=s0_end) begin
						slave_active = 0;  end
					else if (mvif[1].haddr>=s1_start && mvif[0].haddr<=s1_end) begin
						slave_active = 1;  end
					else if (mvif[2].haddr>=s2_start && mvif[0].haddr<=s2_end) begin
						slave_active = 2;  end
				end
				else if (mvif[2].hgrant[2]) begin
					master_active = 2;
					if (mvif[2].haddr>=s0_start && mvif[2].haddr<=s0_end) begin
						slave_active = 0;  end
					else if (mvif[2].haddr>=s1_start && mvif[2].haddr<=s1_end) begin
						slave_active = 1;  end
					else if (mvif[2].haddr>=s2_start && mvif[2].haddr<=s2_end) begin
						slave_active = 2;  end
				end
				end
				svif[slave_active].htrans = mvif[master_active].htrans;
				svif[slave_active].size_t = mvif[master_active].size_t;
				svif[slave_active].haddr = mvif[master_active].haddr;
				svif[slave_active].hprot = mvif[master_active].hprot;
				//svif[slave_active].hsize = mvif[master_active].hsize;
				svif[slave_active].hburst = mvif[master_active].hburst;
				svif[slave_active].hsel = mvif[master_active].hsel;
				svif[slave_active].hwrite=mvif[master_active].hwrite;
				svif[slave_active].hwdata=mvif[master_active].hwdata;
				mvif[master_active].hrdata = svif[slave_active].hrdata;
				mvif[master_active].hready = svif[slave_active].hready;						
			end
			end
		join	
	endtask
endclass
