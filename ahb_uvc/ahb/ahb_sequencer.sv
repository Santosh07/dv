class ahb_sequencer extends uvm_sequencer#(ahb_tx);
	`uvm_component_utils(ahb_sequencer)		//this is all that is required to be done if it is a pull model
	function new(string name="", uvm_component parent);
		super.new(name, parent);
	endfunction
	function void build_phase(uvm_phase phase);
		`uvm_info("ahb_sequencer", "build_phase", UVM_LOW)
	endfunction
endclass
