class master_ahb_agent extends uvm_agent;
	master_ahb_seq seq;
	master_ahb_driver drv;
	ahb_monitor mon;
	ahb_coverage cov;
	
	`uvm_component_utils(master_ahb_agent)

	function new(string name="", uvm_component parent);
		super.new(name, parent);
	endfunction

	function void build_phase(uvm_phase phase);
		`uvm_info("master_ahb_agent", "build_phase", UVM_LOW)
		seq = master_ahb_seq::type_id::create("seq", this);
		drv = master_ahb_driver::type_id::create("drv", this);
		mon = ahb_monitor::type_id::create("mon", this);
		cov = ahb_coverage::type_id::create("cov", this);
	endfunction

	function void connect_phase(uvm_phase phase);
		drv.seq_item_port.connect(seq.seq_item_export);		//since sequencer is exporting, it has an export port	
		mon.ap_port.connect(cov.analysis_export);
	endfunction
endclass

