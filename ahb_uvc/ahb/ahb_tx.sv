class ahb_tx extends uvm_sequence_item;
	rand bit[31:0] haddr;
	rand bit [31:0] dataQ[$:15];
	     bit [31:0] dataQ_r[$:15];
	rand burst_type burst_typ;
	rand hsize_t size;
	rand bit wr_rd;
	rand bit [1:0] hprot;
	     resp_t resp;
	     //Local variables
	rand bit[4:0] burst_length;
	     int tx_size;
	     int tfr_size;
	     int addr_offset;
	     bit[31:0] wrap_lower_addr;
	     bit[31:0] wrap_upper_addr;

	function new(string name="");
		super.new(name);
	endfunction

	`uvm_object_utils_begin(ahb_tx)
		`uvm_field_int(haddr, UVM_ALL_ON|UVM_NOPACK)
		`uvm_field_int(wrap_lower_addr, UVM_ALL_ON|UVM_NOPACK)
		`uvm_field_int(wrap_upper_addr, UVM_ALL_ON|UVM_NOPACK)
		`uvm_field_int(wr_rd, UVM_ALL_ON|UVM_NOPACK)
		`uvm_field_queue_int(dataQ, UVM_ALL_ON|UVM_NOPACK)
		`uvm_field_enum(burst_type, burst_typ, UVM_ALL_ON|UVM_NOPACK)
		`uvm_field_enum(hsize_t, size, UVM_ALL_ON|UVM_NOPACK)
		`uvm_field_enum(resp_t, resp, UVM_ALL_ON|UVM_NOPACK)
		`uvm_field_int(wr_rd, UVM_ALL_ON)
	`uvm_object_utils_end

	function void post_randomize();
		tx_size = (burst_length) * (2**size);
		tfr_size = 2**size;
		addr_offset = haddr%tx_size;
		wrap_lower_addr = haddr-addr_offset;
		wrap_upper_addr = wrap_lower_addr + tx_size - 1;
	endfunction

//NOTE: Whatever is part of constraint has to be a random variable otherwise constraint will fail 

	constraint aligned_c{					//For Aligned, this condition needs to be met: HADDR%num_of_bytes/per_trans(2**hsize) == 0
		(size==BYTE_2)->(haddr[0]==1'b0);	
		(size==BYTE_4)->(haddr[1:0] == 2'b0);
		(size==BYTE_8)->(haddr[2:0] == 3'b0);
		(size==BYTE_16)->(haddr[3:0] == 4'b0);
		(size==BYTE_32)->(haddr[4:0] == 5'b0);
		(size==BYTE_64)->(haddr[5:0] == 6'b0);
		(size==BYTE_128)->(haddr[6:0] == 7'b0);
	};

	constraint burst_length_c{
		(burst_typ == SINGLE) -> (burst_length == 1);
		(burst_typ == INCR) -> (burst_length == 16);
		(burst_typ inside {INCR4, WRAP4}) -> (burst_length == 4);
		(burst_typ inside {INCR8, WRAP8}) -> (burst_length == 8);
		(burst_typ inside {INCR16, WRAP16}) -> (burst_length == 16);
	};

	constraint dataQ_c{							//not exactly sure about this
		(burst_typ==SINGLE) -> (dataQ.size() == 1);
		(burst_typ inside {INCR4, WRAP4}) -> (dataQ.size() == 4);
		(burst_typ inside {INCR8, WRAP8}) -> (dataQ.size() == 8);
		(burst_typ inside {INCR, INCR16, WRAP16}) -> (dataQ.size() == 16);
	};

	constraint data_width_c{
		size inside {BYTE_1, BYTE_2, BYTE_4};		//Please test it with this
//		size == BYTE_4;
	};

	constraint haddr_c{
		(ahb_cfg::num_slaves == 3) -> (haddr inside {[`S0_START_ADDR:`S0_END_ADDR], 
						    [`S1_START_ADDR:`S1_END_ADDR],
						    [`S2_START_ADDR:`S2_END_ADDR]});
		(ahb_cfg::num_slaves == 2) -> (haddr inside {[`S0_START_ADDR:`S0_END_ADDR],
						    [`S1_START_ADDR:`S1_END_ADDR]});
		(ahb_cfg::num_slaves == 1) -> (haddr inside {[`S0_START_ADDR:`S0_END_ADDR]});
	};

/*	constraint order1_c{
//		solve burst_typ before size;
		solve size before burst_typ;
		
	};	*/

endclass
