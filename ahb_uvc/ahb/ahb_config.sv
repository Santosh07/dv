typedef enum bit [1:0] {
	IDLE,
	BUSY,
	NONSEQ,
	SEQ 
} htrans_t;

typedef enum bit [2:0]{
	SINGLE,
	INCR,
	INCR4,
	WRAP4,
	INCR8,
	WRAP8,
	INCR16,
	WRAP16
} burst_type;

typedef enum bit[2:0]{
	BYTE_1,
	BYTE_2,
	BYTE_4,
	BYTE_8,
	BYTE_16,
	BYTE_32,
	BYTE_64,
	BYTE_128
} hsize_t;

typedef enum bit [1:0]{
	ERROR,
	OKAY,
	SPLIT,
	RETRY
} resp_t; 

`define S0_START_REG_ADDR 32'h1000
`define S0_END_REG_ADDR 32'h2000
`define S1_START_REG_ADDR 32'h3000
`define S1_END_REG_ADDR 32'h4000
`define S2_START_REG_ADDR 32'h5000
`define S2_END_REG_ADDR 32'h6000

`define S0_START_ADDR 32'h1000
`define S0_END_ADDR 32'h2000
`define S1_START_ADDR 32'h3000
`define S1_END_ADDR 32'h4000
`define S2_START_ADDR 32'h5000
`define S2_END_ADDR 32'h6000

class ahb_cfg;
	static int num_masters;
	static int num_slaves;
endclass
