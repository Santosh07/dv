/*define IDLE 2'b00
define BUSY 2'b01
define NONSEQ 2'b10
define SEQ 2'b11

define BYTE_1 2'b00
define BYTE_2 2'b01
define BYTE_4 2'b10	*/

//module ahb_slave(		//no more a module, changed to model

class slave_ahb_driver extends uvm_component;
	int slave_no;
	virtual ahb_intf vif;
	`uvm_component_utils_begin(slave_ahb_driver)
		`uvm_field_int(slave_no, UVM_ALL_ON)
	`uvm_component_utils_end

	function new(string name="", uvm_component parent=null);
		super.new(name, parent);
	endfunction
	
	string inst_name;
	byte mem[*]; 			//associative array
	//bit [7:0] mem[*];		//alternative way
	logic [1:0] htrans_t;
	logic [31:0] haddr_t;
	logic [2:0] hburst_t;
	logic [3:0] hprot_t;
	logic [2:0] hsize_t; 



	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		$sformat(inst_name, "svif%0d", slave_no);
		if (!uvm_config_db#(virtual ahb_intf)::get(this, "", inst_name, vif)) begin
			`uvm_error("slave_ahb_driver", "interface not registered")
		end
	endfunction

/*
- Arbitration is taken care by the interconnect. So we don't need this anymore

always @(posedge clk or posedge rst) begin
	if (svif0.hrst) begin
		svif0.hgrant <= 0;
		svif1.hgrant <= 0;
		svif2.hgrant <= 0;
	end
	else begin
		if (svif0.hbusreq[0] == 1) begin
			svif0.hgrant[0] = 1;
		end
		else if (svif1.hbusreq[1] == 1) begin
			svif1.hgrant[1] = 1;
		end
		else if (svif2.hbusreq[2] == 1) begin
			svif2.hgrant[2] = 1;
		end
	
		else if (hbusreq[3] == 1) begin
			hgrant[3] = 1;
		end
		else if (hbusreq[4] == 1) begin
			hgrant[4] = 1;
		end
		else if (hbusreq[5] == 1) begin
			hgrant[5] = 1;
		end
		else if (hbusreq[6] == 1) begin
			hgrant[6] = 1;
		end
		else if (hbusreq[7] == 1) begin
			hgrant[7] = 1;
		end
		else if (hbusreq[8] == 1) begin
			hgrant[8] = 1;
		end
		else if (hbusreq[9] == 1) begin
			hgrant[9] = 1;
		end
		else if (hbusreq[10] == 1) begin
			hgrant[10] = 1;
		end
		else if (hbusreq[11] == 1) begin
			hgrant[11] = 1;
		end
		else if (hbusreq[12] == 1) begin
			hgrant[12] = 1;
		end
		else if (hbusreq[13] == 1) begin
			hgrant[13] = 1;
		end
		else if (hbusreq[14] == 1) begin
			hgrant[14] = 1;
		end
		else if (hbusreq[15] == 1) begin
			hgrant[15] = 1;
		end
	end	
end
*/

//	always @(posedge clk or posedge rst) begin


	task run_phase(uvm_phase phase);
	while (1) begin
	@(posedge vif.hclk); 
		if (vif.hrst) begin
			vif.hrdata <= 32'h0;
			vif.hresp <= 0;
			vif.hready <= 0;
			vif.hgrant <= 0;
		end
		else begin
			case(vif.htrans) 
				IDLE: begin
					if (htrans_t inside {NONSEQ, SEQ}) begin
						if (vif.hwrite == 1) begin
							case (vif.size_t)
								BYTE_1: begin
									mem[haddr_t] = vif.hwdata[7:0];
								end
								BYTE_2: begin
									mem[haddr_t] = vif.hwdata[7:0];
									mem[haddr_t+1] = vif.hwdata[15:8];
								end
								BYTE_4: begin
									mem[haddr_t] = vif.hwdata[7:0];
									mem[haddr_t+1] = vif.hwdata[15:8];
									mem[haddr_t+2] = vif.hwdata[23:16];
									mem[haddr_t+3] = vif.hwdata[31:24];
								end
							endcase
						end
				/*		else begin
							case (vif.size_t)
								BYTE_1: begin
									vif.hrdata = mem[haddr_t];
								end
								BYTE_2: begin
									vif.hrdata = {mem[haddr_t+1], mem[haddr_t]};
								end
								BYTE_4: begin
									vif.hrdata = {mem[haddr_t+3], mem[haddr_t+2], mem[haddr_t+1], mem[haddr_t]};
								end
							endcase
						end	*/
					/*	else begin
							$display("I came in IDLE");
							hrdata[31:0] = {mem[haddr_t+3], mem[haddr_t+2], mem[haddr_t+1], mem[haddr_t]};
							$display("IDLE:HADDR=%h, HRDATA=%h", haddr, hrdata);
						end	*/
					end
					haddr_t = vif.haddr;
					htrans_t = vif.htrans;
					vif.hready = 1;	
				end
				
				BUSY: begin
					if (htrans_t inside {NONSEQ, SEQ}) begin
						if (vif.hwrite == 1) begin
							case (vif.size) 
							BYTE_1: begin
								mem[haddr_t] = vif.hwdata[7:0];
							end
							BYTE_2: begin
								mem[haddr_t] = vif.hwdata[7:0];
								mem[haddr_t+1] = vif.hwdata[15:8];							
							end
							BYTE_4: begin
							//	$display("NONSEQ: HADDR=%h, HWDATA=%h", haddr_t, vif.hwdata);
								mem[haddr_t] = vif.hwdata[7:0];
								mem[haddr_t+1] = vif.hwdata[15:8];
								mem[haddr_t+2] = vif.hwdata[23:16];
								mem[haddr_t+3] = vif.hwdata[31:24];
								$display("mem[%h]=%h", haddr_t, mem[haddr_t]);
								$display("mem[%h]=%h", haddr_t+1, mem[haddr_t+1]);
								$display("mem[%h]=%h", haddr_t+2, mem[haddr_t+2]);
								$display("mem[%h]=%h", haddr_t+3, mem[haddr_t+3]);
							end
							endcase
						end
					/*	else begin
							case (vif.size_t)
								BYTE_1: begin
									vif.hrdata = mem[haddr_t];
								end
								BYTE_2: begin
									vif.hrdata = {mem[haddr_t+1], mem[haddr_t]};
								end
								BYTE_4: begin
									vif.hrdata = {mem[haddr_t+3], mem[haddr_t+2], mem[haddr_t+1], mem[haddr_t]};
								end
							endcase
	
						end		*/
					end
					haddr_t = vif.haddr;
					htrans_t = vif.htrans;
					vif.hready = 1;	
				end

				SEQ: begin
					if (htrans_t inside {NONSEQ, SEQ}) begin
						if (vif.hwrite == 1) begin
							case (vif.size_t) 
							BYTE_1: begin
								mem[haddr_t] = vif.hwdata[7:0];
								$display("SQ:Insider slave driver: mem[%h]=%h", haddr_t, mem[haddr_t]);
							end
							BYTE_2: begin
								$display("SEQ: HADDR=%h, HWDATA=%h", haddr_t, vif.hwdata);
								mem[haddr_t] = vif.hwdata[7:0];
								mem[haddr_t+1] = vif.hwdata[15:8];							
								$display("SQ:Insider slave driver: mem[%h]=%h", haddr_t, mem[haddr_t]);
								$display("SQ:Insider slave driver: mem[%h]=%h", haddr_t+1, mem[haddr_t+1]);
							end
							BYTE_4: begin
								$display("SEQ: HADDR=%h, HWDATA=%h", haddr_t, vif.hwdata);
								mem[haddr_t] = vif.hwdata[7:0];
								mem[haddr_t+1] = vif.hwdata[15:8];
								mem[haddr_t+2] = vif.hwdata[23:16];
								mem[haddr_t+3] = vif.hwdata[31:24];
								$display("SQ:Insider slave driver: mem[%h]=%h", haddr_t, mem[haddr_t]);
								$display("SQ:Insider slave driver: mem[%h]=%h", haddr_t+1, mem[haddr_t+1]);
								$display("SQ:Insider slave driver: mem[%h]=%h", haddr_t+2, mem[haddr_t+2]);
								$display("SQ:Insider slave driver: mem[%h]=%h", haddr_t+3, mem[haddr_t+3]);
							end
							endcase
						end
					/*	else begin
							$display("slave_number=%d", slave_no);
							case (vif.size_t)
								BYTE_1: begin
									vif.hrdata = mem[haddr_t];
								end
								BYTE_2: begin
									vif.hrdata = {mem[haddr_t+1], mem[haddr_t]};
								end
								BYTE_4: begin
									vif.hrdata = {mem[haddr_t+3], mem[haddr_t+2], mem[haddr_t+1], mem[haddr_t]};
								end
							endcase
						end	*/
					end
					
					if (vif.hwrite == 0) begin
						$display("-----------------------HAHAHAHAHA_SEQ---------------------");
						case (vif.size_t)
							BYTE_1: begin
								vif.hrdata[31:0] = {mem[vif.haddr]};
								$display("BUSY:HADDR=%h, HRDATA=%h", vif.haddr, vif.hrdata);
							end
							BYTE_2: begin
								vif.hrdata[31:0] = {mem[vif.haddr+1], mem[vif.haddr]};
								$display("BUSY:HADDR=%h, HRDATA=%h", vif.haddr, vif.hrdata);	
							end
							BYTE_4: begin
								vif.hrdata[31:0] = {mem[vif.haddr+3], mem[vif.haddr+2], mem[vif.haddr+1], mem[vif.haddr]};
								$display("BUSY:HADDR=%h, HRDATA=%h", vif.haddr, vif.hrdata);	
							end
						endcase
					end	
					haddr_t = vif.haddr;
					htrans_t = vif.htrans;
					vif.hready = 1;
				end

				NONSEQ: begin

					if (htrans_t inside {NONSEQ, SEQ}) begin		
						if (vif.hwrite == 1) begin
							case (vif.size_t) 
							BYTE_1: begin
								mem[haddr_t] = vif.hwdata[7:0];
								$display("NS:Inside slave driver, mem[%h]=%h", haddr_t, mem[haddr_t]);
							end
							BYTE_2: begin
								$display("NS:Byte2:hwdata[7:0]=%h", vif.hwdata[7:0]);
								$display("NS:Byte2:hwdata[15:8]=%h", vif.hwdata[15:8]);
								mem[haddr_t] = vif.hwdata[7:0];
								mem[haddr_t+1] = vif.hwdata[15:8];							
								$display("NS:Insider slave driver: mem[%h]=%h", haddr_t, mem[haddr_t]);
								$display("NS:Insider slave driver: mem[%h]=%h", haddr_t+1, mem[haddr_t+1]);
							end
							BYTE_4: begin
								$display("NONSEQ: HADDR=%h, HWDATA=%h", vif.haddr_t, vif.hwdata);
								mem[haddr_t] = vif.hwdata[7:0];
								mem[haddr_t+1] = vif.hwdata[15:8];
								mem[haddr_t+2] = vif.hwdata[23:16];
								mem[haddr_t+3] = vif.hwdata[31:24];
								$display("NS:Insider slave driver: mem[%h]=%h", haddr_t, mem[haddr_t]);
								$display("NS:Insider slave driver: mem[%h]=%h", haddr_t+1, mem[haddr_t+1]);
								$display("NS:Insider slave driver: mem[%h]=%h", haddr_t+2, mem[haddr_t+2]);
								$display("NS:Insider slave driver: mem[%h]=%h", haddr_t+3, mem[haddr_t+3]);
							end
							endcase
						end
						/*else begin
							case (vif.size_t)
								BYTE_1: begin
									vif.hrdata = mem[haddr_t];
								end
								BYTE_2: begin
									vif.hrdata = {mem[haddr_t+1], mem[haddr_t]};
								end
								BYTE_4: begin
									vif.hrdata = {mem[haddr_t+3], mem[haddr_t+2], mem[haddr_t+1], mem[haddr_t]};
								end
							endcase
	
						end		*/
					end
					if (vif.hwrite == 0) begin
						$display("-----------------------HAHAHAHAHASEQ---------------------");
						case (vif.size_t)
							BYTE_1: begin
								vif.hrdata[31:0] = {mem[vif.haddr]};
								$display("BUSY:HADDR=%h, HRDATA=%h", vif.haddr, vif.hrdata);
							end
							BYTE_2: begin
								vif.hrdata[31:0] = {mem[vif.haddr+1], mem[vif.haddr]};
								$display("BUSY:HADDR=%h, HRDATA=%h", vif.haddr, vif.hrdata);	
							end
							BYTE_4: begin
								vif.hrdata[31:0] = {mem[vif.haddr+3], mem[vif.haddr+2], mem[vif.haddr+1], mem[vif.haddr]};
								$display("BUSY:HADDR=%h, HRDATA=%h", vif.haddr, vif.hrdata);	
							end
						endcase
					end	
					haddr_t = vif.haddr;
					htrans_t = vif.htrans;
					vif.hready = 1;
				end
			endcase
		end
	end	
	endtask
endclass


