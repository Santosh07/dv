interface ahb_intf(input logic hclk, input logic hrst);
	bit [31:0] haddr;
	htrans_t htrans;
	bit hwrite;
	hsize_t size_t;
	burst_type hburst;
	bit [3:0] hprot;
	bit [31:0] hwdata;
	bit [3:0] hsel;
	bit [31:0] hrdata;
	bit hready;
	bit [1:0] hresp;
	bit [15:0] hbusreq;
	bit [15:0] hlock;
	bit [15:0] hgrant;
	bit [3:0] hmaster;
	bit hmastlock;
	bit hsplit;

	clocking cb @(posedge hclk);
		default input #1step;
		default output #2ns;
		output haddr;
		output htrans;
		output hwrite;
		output size_t;
		output hburst;
		output hprot;
		output hwdata;
		output hsel;
		input hrdata;
		input hready;
		input hresp;
		output hbusreq;
		output hlock;
		input hgrant;
		input hmaster;
		input hmastlock;
		input hsplit;
		input hclk;
	endclocking

	modport drv (clocking cb, input hrst);
endinterface
