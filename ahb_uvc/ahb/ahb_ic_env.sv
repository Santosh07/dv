class ahb_ic_env extends uvm_env;
	master_ahb_agent magent[];
	slave_ahb_agent sagent[];
	apb_agent pagent;
	ahb_ic	dut_ic;
	string inst_name;
	ahb_ic_sbd ic_sbd;
	int num_masters, num_slaves;
	bit mas_slv_f;
	top_sqr t_sqr;


	`uvm_component_utils_begin(ahb_ic_env)
		`uvm_field_int(num_masters, UVM_ALL_ON)
		`uvm_field_int(num_slaves, UVM_ALL_ON)
	`uvm_component_utils_end
	
	function new(string name="", uvm_component parent=null);
		super.new(name, parent);
	endfunction
	
	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		
		magent = new[num_masters];
		sagent = new[num_slaves];	

		`uvm_info("ahb_ic_env", $psprintf("num_masters=%0d", num_masters), UVM_LOW)			
		for (int i=0; i<num_masters; i++) begin
		//	magent = new[num_masters];
			$sformat(inst_name, "magent[%0d]", i);
			magent[i] = master_ahb_agent::type_id::create(inst_name, this);
			uvm_config_db#(int)::set(this, {inst_name, ".*"}, "master_no", i);
			uvm_config_db#(int)::set(this, {inst_name, ".*"}, "mas_slv_f", 1);
		end
		for (int i=0; i<num_slaves; i++) begin	
		//	sagent = new[num_slaves];
			$sformat(inst_name, "sagent[%0d]", i);
			sagent[i] = slave_ahb_agent::type_id::create(inst_name, this);
			uvm_config_db#(int)::set(this, {inst_name, ".*"}, "slave_no", i);
			uvm_config_db#(int)::set(this, {inst_name, ".*"}, "mas_slv_f", 0);
		end
	
		pagent = apb_agent::type_id::create("pagent", this);

		dut_ic = ahb_ic::type_id::create("dut_ic", this);
		
		ic_sbd = ahb_ic_sbd::type_id::create("ic_sbd", this);
		
		t_sqr = top_sqr::type_id::create("t_sqr", this);
	endfunction	

	function void connect_phase(uvm_phase phase);
		for (int i=0; i<num_masters; i++) begin
			t_sqr.h_seq[i] = magent[i].seq;
		end
			t_sqr.p_seq = pagent.p_seq;
	endfunction
endclass
