class master_ahb_driver extends uvm_driver#(ahb_tx);
	bit [3:0] master_no;
	bit [31:0] haddr_t;
	string inst_name;

	`uvm_component_utils_begin(master_ahb_driver)
		`uvm_field_int(master_no, UVM_ALL_ON)	//if you register this to the factory then you need not do uvm_config_db#(int)::get(this, "", "master_no", master_no)
							//two things we need to do to retrieve a non-object field automatically:
							//-register it to the factory
							//-do super.build_phase(phase)
	`uvm_component_utils_end

	virtual ahb_intf vif;
	int flag1;
	uvm_table_printer printer;

	function new(string name="", uvm_component parent);
		super.new(name, parent);
	endfunction

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);	//if you register this to the factory then you need not do uvm_config_db#(int)::get(this, "", "master_no", master_no)
		printer = new();
		//getting the interface handle from the config_db to drive the signals to the DUT
		`uvm_info("master_ahb_driver", "build_phase", UVM_LOW)
		$sformat(inst_name, "mvif%0d", master_no);
		if (!uvm_config_db#(virtual ahb_intf)::get(this, "", inst_name, vif)) begin
			`uvm_error("master_ahb_driver", "interface not registered to factory")
		end
	endfunction

	task run_phase(uvm_phase phase);
		forever begin
			seq_item_port.get_next_item(req);		//get_next_item is a task that is present in the uvm_sequencer which is being called here. This task returns the 
									// req. This req is received here and printed.
			req.print(printer);
			drive_item(req);
			seq_item_port.item_done();
		end
	endtask

	task drive_item(ahb_tx req);
		//$cast(rsp, req);			//not sure about this
		int trans_count = 0;
		haddr_t = req.haddr;
		arbitration_phase(req);	
		address_phase(req, trans_count);
		for (int i=0; i<req.burst_length-1; i++) begin
			fork
				address_phase(req);
				data_phase(req);
			join
		end
		data_phase(req);
	endtask		

	task arbitration_phase(ahb_tx req);
		bit flag1;
		`uvm_info("master_ahb_driver", "abritration_phase", UVM_LOW)
		vif.hbusreq[master_no] = 1;				//did not quite understand the significance of this
		vif.hlock[master_no] = 1;
		flag1=0;
		`uvm_info("master_ahb_driver", $psprintf("Master number %d requesting for the bus", master_no), UVM_NONE)
		while (flag1 == 0) begin
			@(negedge vif.hclk) begin
				if (vif.hgrant[master_no] == 1) begin
					flag1=1;
					`uvm_info("master_ahb_driver", "got the grant", UVM_LOW)
				end
			end
		end
		`uvm_info("master_ahb_driver", $psprintf("Master no. %d got the bus grant", master_no), UVM_NONE)
	endtask

	task address_phase(ahb_tx req, int trans_count=1);
		bit ready_f;
		`uvm_info("master_ahb_driver", "address_phase", UVM_LOW)
//		haddr_t = req.haddr;
		//In this phase, we need to drive address and control signals. So, haddr, hprot, hburst, hsize
		//Also , if it is the first transfer in the transaction, then htrans will be NONSEQ and for the first transfer we need not wait for the hready signal from the slave.
		//The slave has to accept it.
		if (trans_count == 0) begin				//that means it is the first transfer of the transaction
		//	if (req.wr_rd == 1'b1) begin		//write cycle. We need not check whether it is read or write during address phase. Needs to be checked only in the data phase
				vif.haddr = haddr_t;
				vif.htrans = NONSEQ;			//indicating to the slave that it is the first transfer of the tx
				vif.hburst = req.burst_typ;
				vif.hprot = req.hprot;
				vif.size_t = req.size;	
				vif.hwrite = req.wr_rd;
				@(negedge vif.hclk);			//Wait till the negedge of the interface clock before changing the signals to new values
				trans_count = trans_count+1;
				vif.htrans = IDLE;			//Driving it to IDLE for a short period of time just to tell the slave that the data 
				haddr_t = haddr_t+req.tfr_size;	//So that it has the next address the next time
		end
		else begin
				vif.haddr = haddr_t;		//not sure
				vif.htrans = SEQ;
				vif.hburst = req.burst_typ;
				vif.hprot = req.hprot;
				vif.size_t = req.size;
				ready_f=0;
				while (ready_f == 0) begin		//wait for hready from the slave known as handshaking
					@(negedge vif.hclk) begin
						if (vif.hready==1) begin
							ready_f = 1;
						end
					end
				end
			vif.htrans = IDLE;
			haddr_t = haddr_t+req.tfr_size;
			end
	//		vif.htrans = IDLE;
	//		req.haddr = req.haddr+req.tfr_size;
	endtask

	task data_phase(ahb_tx req);
		bit ready_f;
		int k;
		`uvm_info("master_ahb_driver", $psprintf("data_phase:req.wr_rd=%d", req.wr_rd), UVM_LOW)
		if (req.wr_rd ==  1) begin			//Write cycle
			`uvm_info("master_ahb_driver_write", "I am here", UVM_LOW)
			vif.hwdata = req.dataQ.pop_front();			//Keep taking the first data from the DataQ and drive it to hwdata
			ready_f = 0;
			while (ready_f==0) begin
				@(negedge vif.hclk);
				if (vif.hready==1) begin			//Keep the current data on hwdata till the slave responses with the hready signal
					ready_f=1;
					if (req.wr_rd == 0) begin		//This means it is a read cycle
						req.dataQ_r.push_back(vif.hrdata);	//Storing the data in the dataQ
						`uvm_info("master_ahb_driver_write", $psprintf("haddr=%h, hrdata=%h", vif.haddr, vif.hrdata), UVM_LOW)
					//	$display("vif.hrdata=%h", vif.hrdata);
					end
				end
			end		
		end
		else begin					//if it is a ready cycle
			`uvm_info("master_ahb_driver_read", "I am here", UVM_LOW)
			@(negedge vif.hclk);	
			`uvm_info("ahb_driver_read", "clk edge became positive", UVM_LOW)	
			if (vif.hready==1) begin		//if hready is 1 that means valid data is present on hrdata
				`uvm_info("master_ahb_driver_read", "hready became positive", UVM_LOW)
				req.dataQ_r.push_back(vif.hrdata);
//				$display("vif.hrdata=%h", vif.hrdata);
				`uvm_info("master_ahb_driver_read", $psprintf("haddr=%h, hrdata=%h", vif.haddr, vif.hrdata), UVM_LOW)
			end
		end
//		$display("DATAQ_R=%p", req.dataQ_r);
		foreach(req.dataQ_r[k]) begin
			$display("DataQ_r[%d]=%h", k, req.dataQ_r[k]);
		end
	endtask
endclass

