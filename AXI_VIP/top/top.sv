module top;
reg aclk, arstn;
//instantiate interface
axi_intf pif(aclk, arstn);
//instantiate tb
axi_slave dut(.aclk(pif.aclk),
		.arstn(pif.arstn),
		.awaddr(pif.awaddr),
		.awsize(pif.awsize),
		.awlen(pif.awlen),
		.awprot(pif.awprot),
		.awcache(pif.awcache),
		.awlock(pif.awlock),
		.awburst(pif.awburst),
		.awvalid(pif.awvalid),
		.awready(pif.awready),
		.awid(pif.awid),
		//data phase
		.wdata(pif.wdata),
		.wstrb(pif.wstrb),
		.wlast(pif.wlast),
		.wvalid(pif.wvalid),
		.wready(pif.wready),
		.wid(pif.wid),
		//response phase
		.bid(pif.bid),
		.bvalid(pif.bvalid),
		.bresp(pif.bresp),
		.bready(pif.bready),
		//read address phase
		.araddr(pif.araddr),
		.arsize(pif.arsize),
		.arlen(pif.arlen),
		.arprot(pif.arprot),
		.arcache(pif.arcache),
		.arlock(pif.arlock),
		.arburst(pif.arburst),
		.arvalid(pif.arvalid),
		.arready(pif.arready),
		.arid(pif.arid),
		//read data phase+resp phase
		.rdata(pif.rdata),
		.rlast(pif.rlast),
		.rid(pif.rid),
		.rvalid(pif.rvalid),
		.rready(pif.rready),
		.rresp(pif.rresp)
	);
//instantiate dut
axi_tb tb();

//logic to generate clk

initial begin
	axi_cfg::vif = pif;
end

initial begin
	aclk = 0;
	forever #5 aclk = ~aclk;
end
//logic to end simulation	
initial begin
	#5000;
	$finish;
end
//logic to generate rst
initial begin
	arstn = 0;
	repeat(2) @(posedge aclk);
	arstn = 1;
end

endmodule
