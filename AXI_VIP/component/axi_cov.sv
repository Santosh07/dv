class axi_cov;
	axi_tx tx;
	covergroup axi_cg;
		coverpoint tx.wlen {
					bins AWLEN_SMALL = {[2:0]};
					bins AWLEN_MID1 = {[5:3]};
					bins AWLEN_MID2 = {[8:6]};
					bins AWLEN_MID3 = {[12:7]};
					bins AWLEN_LARGE = {[15:13]};
				}
	endgroup

	task run();
		$display("COV::I am in the COV class");
		forever begin
			axi_cfg::mon2cov_mbx.get(tx);
			axi_cg.sample();
		end
	endtask
		
	function new();
		axi_cg = new();
	endfunction
endclass
