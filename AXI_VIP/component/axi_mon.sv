class axi_mon;
	virtual axi_intf mon_vif; 
	axi_tx tx_addr_arr[16];	
//We will be collecting only the valid transactions and passing them to coverage, scoreboard
	task run();
		mon_vif = axi_cfg::vif;
		$display("MON::I am in the MON class");
		$display($time, "mon_vif.awvalid=%d, mon_vif.awready=%d", mon_vif.awvalid, mon_vif.awready);
		forever begin
			@(posedge mon_vif.aclk);
				if (mon_vif.awvalid && mon_vif.awready) begin
					$display("MON::AWVALID and AWREADY became 1");
					tx_addr_arr[mon_vif.awid] = new();
					tx_addr_arr[mon_vif.awid].wid = mon_vif.awid;
					tx_addr_arr[mon_vif.awid].waddr = mon_vif.awaddr;
					tx_addr_arr[mon_vif.awid].wlen = mon_vif.awlen;
					tx_addr_arr[mon_vif.awid].wsize = mon_vif.awsize;
//					tx_addr_arr[mon_vif.awid].wprot = mon_vif.awprot;
//					tx_addr_arr[mon_vif.awid].wcache = mon_vif.awcache;
				end
			if (mon_vif.wvalid && mon_vif.wready) begin
				tx_addr_arr[mon_vif.wid].wdataQ.push_back(mon_vif.wdata);
				tx_addr_arr[mon_vif.wid].wstrbQ.push_back(mon_vif.wstrb);
			end
			if (mon_vif.bvalid && mon_vif.bready) begin
				$display("MON::BVALID and BREADY became 1");
				tx_addr_arr[mon_vif.bid].bresp = mon_vif.bresp;
				axi_cfg::mon2cov_mbx.put(tx_addr_arr[mon_vif.bid]);
				$display("##############Printing from the Monitor######################");
				$display("Here it is %d", tx_addr_arr[mon_vif.bid].tx_typ);
				tx_addr_arr[mon_vif.bid].print();
				$display("##########End of Printing from Monitor#########################");
			end
		end
	endtask
endclass
