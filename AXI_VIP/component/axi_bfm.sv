class axi_bfm;
	bit flag1;
	bit flag2;
	bit flag3;
	bit read_data_f;
	bit read_address_f;
	axi_tx tx;
	virtual axi_intf vif_bfm;
	
	task run();
		vif_bfm = axi_cfg::vif;
		forever begin
			axi_tx tx = new();
			axi_cfg::gen2bfm_mbx.get(tx);
			//tx.print();
			drive_data(tx);
		end
	endtask
	
	task drive_data(axi_tx tx);
		//for write phase, it should
		//-first, drive the address which is one cycle
		//-second, multiple data phase
		//-third, one response phase
		if (tx.tx_typ==WRITE) begin
			write_address_phase(tx);
			write_data_phase(tx);
			write_response_phase(tx);		
		end
	
		if (tx.tx_typ==READ) begin
			read_address_phase(tx);
			read_data_resp_phase(tx);
		end
	
		if (tx.tx_typ==READ_WRITE) begin
			fork
				begin
					read_address_phase(tx);
					read_data_resp_phase(tx);
				end
				begin
					write_address_phase(tx);
					write_data_phase(tx);
					write_response_phase(tx);
				end
			join
		end

	endtask
	
		task read_address_phase(axi_tx tx);
			$display("BFM::READ_ADDRESS_PHASE");
			@(posedge vif_bfm.aclk);
				vif_bfm.arid = tx.rid;
				vif_bfm.araddr = tx.raddr;
				vif_bfm.arlen = tx.rlen;
				vif_bfm.arvalid = 1'b1;
				read_address_f=0;
				while (read_address_f!=1) begin
					@(posedge vif_bfm.aclk);
					if (vif_bfm.arready==1) begin
						read_address_f = 1;
					end
				end
				@(negedge vif_bfm.aclk);
				vif_bfm.arvalid = 1'b0;
		endtask

		task read_data_resp_phase(axi_tx tx);
			$display("BFM::read_data_PHASE");
			read_data_f = 0;
			while (read_data_f!=1) begin
				@(posedge vif_bfm.aclk);
					if (vif_bfm.rvalid ) begin
						$display("Read_Data=%p", vif_bfm.rdata);
						vif_bfm.rready = 1'b1;
					end
			end
			vif_bfm.rready=1'b0;				
		endtask

		task write_address_phase(axi_tx tx);
			$display("BFM::write_ADDRESS_PHASE");
			flag1 = 1'b0;
			vif_bfm.awaddr = tx.waddr;
			vif_bfm.awlen = tx.wlen;
			vif_bfm.awsize = tx.wsize;
			vif_bfm.awprot = tx.wprot;
			vif_bfm.awcache = tx.wcache;
			vif_bfm.awlock = tx.wlock;
			vif_bfm.awburst = tx.wburst;
			vif_bfm.awid = tx.wid;
			vif_bfm.awvalid = 1'b1;
			while (flag1!=1) begin
				@(posedge vif_bfm.aclk);
					if (vif_bfm.awready==1) begin
						flag1 = 1;
						$display("AWREADY became 1");
					end
				end
				@(negedge vif_bfm.aclk);
					vif_bfm.awvalid = 1'b0;
		endtask

		task write_data_phase(axi_tx tx);
			$display("BFM::write_data_PHASE");
			//vif_bfm.wlast = 1'b0; 
			for (int i=0; i<=vif_bfm.awlen; i++) begin
				$display("Sending the %d data", i);
				if (i==vif_bfm.awlen) begin
					vif_bfm.wlast = 1'b1;
					$display("WLAST has become 1");
				end
				vif_bfm.wdata = tx.wdataQ[i];
				vif_bfm.wstrb = tx.wstrbQ[i];
				vif_bfm.wid = tx.wid;
				vif_bfm.wvalid = 1'b1;
				flag2=0;
				while (flag2!=1) begin
					@(posedge vif_bfm.aclk);
						if (vif_bfm.wready==1) begin
							flag2=1;
						end
				end
				@(negedge vif_bfm.aclk);
					vif_bfm.wvalid = 1'b0;
					vif_bfm.wlast = 1'b0;
			end
		endtask

		task write_response_phase(axi_tx tx);
			$display("BFM::write_resp_PHASE");
			vif_bfm.bready = 1'b0;
			flag3 = 0;
			while (flag3 != 1) begin
				@(posedge vif_bfm.aclk);
				if (vif_bfm.bvalid == 1) begin
					$display("BVALID became 1");
					flag3 = 1;
					vif_bfm.bready = 1;
				end
			end
		endtask
endclass
