program axi_tb;
	axi_env env;
	
	initial begin
		wait(axi_cfg::e.triggered());
		env = new();
		env.run();
	end
	
	initial begin
		$value$plusargs("testname=%s", axi_cfg::testname);
		-> axi_cfg::e;
	end
endprogram
