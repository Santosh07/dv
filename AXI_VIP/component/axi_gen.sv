class axi_gen;
	axi_tx wrtQ[$];
	task run();
//-----------------------------------test 1-----------------------------------------
//---------------------------test to write data----------------------
		case (axi_cfg::testname) 
		"Two_Transfers_Len_1": begin
			repeat(2) begin
				axi_tx tx = new();
				assert(tx.randomize() with {tx.wlen==0;
							   tx.tx_typ==WRITE;});
				axi_cfg::gen2bfm_mbx.put(tx);
				tx.print();
			end	
		end
				
//----------------------------10 write---------------------------
		"WRITE_10_tx": begin
			repeat(10) begin
				axi_tx tx = new();
				assert(tx.randomize() with {tx.tx_typ==WRITE;});	//in-line constraint
				axi_cfg::gen2bfm_mbx.put(tx);
			end
		end
//----------------------------------------------------------------------------------

//---------------------------Single_Read_Write--------------------------------------
		"Single_Write_Read": begin
				axi_tx tx = new();
				axi_tx tx_t;
				axi_tx tx_r = new();
				assert(tx.randomize() with {tx.wlen == 0;
							    tx.tx_typ == WRITE;});
				$cast(tx_t, tx);
				tx_t.print();
				axi_cfg::gen2bfm_mbx.put(tx);

				assert(tx_r.randomize() with {tx_r.rlen == 0;
							   tx_r.rid == tx_t.wid;
							   tx_r.raddr == tx_t.waddr;
							   tx_r.rsize == tx_t.wsize;
							   tx_r.rburst == tx_t.wburst;
							   tx_r.tx_typ == READ;});
				axi_cfg::gen2bfm_mbx.put(tx_r);
		end

//----------------------------------------------------------------------------------
//---------------------------------------
		"Two_Read_Write": begin
			axi_tx tx = new();
			axi_tx tx_tt;
			axi_tx tx_rx=new();
			assert(tx.randomize() with {tx.wlen==1;
						    tx.tx_typ == WRITE;});
			tx.print();
			axi_cfg::gen2bfm_mbx.put(tx);
			$cast(tx_tt, tx); 
			assert(tx_rx.randomize() with {tx_rx.rid==tx_tt.wid;
							tx_rx.raddr==tx_tt.waddr;
							tx_rx.rlen==1;
							tx_rx.tx_typ == READ;});
			axi_cfg::gen2bfm_mbx.put(tx_rx);
		end
//-----------------------------------------
//------------------------------------------
		"Fifteen_WRITE_READ": begin
			axi_tx tx = new();
			axi_tx tx_rx = new();
			axi_tx tx_t;
			assert(tx.randomize() with {tx.wlen==15;
						   tx.tx_typ == WRITE;});
			axi_cfg::gen2bfm_mbx.put(tx);
			tx.print();
			$cast(tx_t, tx);
			assert(tx_rx.randomize() with {tx_rx.raddr == tx_t.waddr;
							tx_rx.tx_typ == READ;
							tx_rx.rlen == 15;
							tx_rx.rid == tx_t.wid;});
			axi_cfg::gen2bfm_mbx.put(tx_rx);
		end
//------------------------------------------
//----------------------------10 read-----------------------------
		"READ_10_tx": begin
			repeat(10) begin
				axi_tx tx = new();
				assert(tx.randomize() with {tx.tx_typ==READ;});
				axi_cfg::gen2bfm_mbx.put(tx);
			end
		end
//--------------------------------------------

//---------------------------10 write-read and compare-----------------------
		"WRITE_READ_COMPARE": begin
				for (int i=0; i<10; i++) begin
					axi_tx tx = new();
					assert(tx.randomize() with {tx.tx_typ==WRITE;});	//in-line constraint
					axi_cfg::gen2bfm_mbx.put(tx);
					wrtQ.push_back(tx);
				end
		
				for (int i=0; i<10; i++) begin
					axi_tx tx = new();
					assert(tx.randomize() with 
								{tx.tx_typ==READ;
								tx.raddr==wrtQ[i].waddr;
								tx.rlen==wrtQ[i].wlen;
								tx.rsize==wrtQ[i].wsize;
								tx.rburst==wrtQ[i].wburst;
								tx.rprot==wrtQ[i].wprot;
								tx.rcache==wrtQ[i].wcache;});
					axi_cfg::gen2bfm_mbx.put(tx);
				end
				//Logic for compare to be written
				for (int i=0; i<10; i++) begin
					axi_tx rx = new();
					axi_cfg::bfm2gen_mbx.get(rx);
					if (rx.raddr != wrtQ[0].waddr) begin
						$display("Addr does not match");
						return;
					end
					if (rx.rdataQ.size()!=wrtQ[i].wdataQ.size()) begin
						$display("Data size does not match, RDATASIZE=%h, WDATASIZE=%h", rx.rdataQ.size(), wrtQ[i].wdataQ.size());
						return;
					end
					foreach(rx.rdataQ[j]) begin
						if (rx.rdataQ[j] != wrtQ[i].wdataQ[j]) begin
							$display("Data does not match. RDATA=%h, WDATA=%h", rx.rdataQ[j], wrtQ[i].wdataQ[j]);
							return;
						end
					end
					if (rx.rprot != wrtQ[i].wprot) begin
						$display("RPROT=%h, WPROT=%h", rx.rprot, wrtQ[i].wprot);
						return;
					end
				end
		end
//-------------------------------------------
//-----------------------------------test 2----------------------------------------
	"Write_Read_Compare_1000": begin
//-----------------------Write Part------------------------------------------------
		axi_tx tx = new();
		assert(tx.randomize() with {tx.waddr==32'd1000;
					tx.tx_typ == WRITE;});
		wrtQ.push_back(tx);
		axi_cfg::gen2bfm_mbx.put(tx);
		//wrtQ.push_back(tx);
//-----------------------Read Part--------------------------------------------------
		repeat(1) begin
			axi_tx tx_rd = new();
			assert(tx_rd.randomize() with {tx_rd.tx_typ == READ;
						tx_rd.raddr==32'd1000;
						tx_rd.rlen==wrtQ[0].wlen;
						tx_rd.rsize==wrtQ[0].wsize;
						tx_rd.rburst==wrtQ[0].wburst;});
			axi_cfg::gen2bfm_mbx.put(tx_rd);
		end
//----------------------Compare Part-------------------------------------------------
		repeat(1) begin
			axi_tx rx=new();
			axi_cfg::bfm2gen_mbx.get(rx);
			if (rx.raddr != wrtQ[0].waddr) begin
				$display("Addr does not match");
				return;
			end
			if (rx.rlen != wrtQ[0].wlen) begin
				$display("Len does not match");
				return;
			end
		//rx.compare(wrtQ);		//not right coz the compare function in axi_tx accepts only axi_tx objects as arguments whereas over here we are trying to pass a queue.
			foreach(rx.rdataQ[i]) begin
				if (rx.rdataQ[i] != wrtQ[0].wdataQ[i]) begin
					$display("FAILED: Data does not match");
					return;
				end
			end
		end
	end
//---------------------------------------------------------------------------------
	endcase
	endtask
endclass
