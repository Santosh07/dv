library verilog;
use verilog.vl_types.all;
entity axi_intf is
    port(
        aclk            : in     vl_logic;
        arstn           : in     vl_logic
    );
end axi_intf;
