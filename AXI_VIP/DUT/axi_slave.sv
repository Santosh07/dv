module axi_slave(aclk,
		arstn,
		//write address phase
		awaddr,
		awsize,
		awlen,
		awprot,
		awcache,
		awlock,
		awburst,
		awvalid,
		awready,
		awid,
		//data phase
		wdata,
		wstrb,
		wlast,
		wvalid,
		wready,
		wid,
		//response phase
		bid,
		bvalid,
		bresp,
		bready,
		//read address phase
		araddr,
		arsize,
		arlen,
		arprot,
		arcache,
		arlock,
		arburst,
		arvalid,
		arready,
		arid,
		//read data phase+resp phase
		rdata,
		rlast,
		rid,
		rvalid,
		rready,
		rresp
		);

	bit flag1;

	input aclk;
	input arstn;
		//write address phase
	input [31:0] awaddr;
	input [2:0] awsize;
	input [3:0] awlen;
	input [2:0] awprot;
	input [3:0] awcache;
	input [1:0] awlock;
	input [1:0] awburst;
	input reg awvalid;
	output reg awready;
	input [3:0] awid;
		//data phase
	input [31:0] wdata;
	input [3:0] wstrb;
	input wlast;
	input wvalid;
	output reg wready;
	input [3:0] wid;
		//response phase
	output reg [3:0] bid;
	output reg bvalid;
	output reg [1:0] bresp;
	input bready;
		//read address phase
	input [31:0] araddr;
	input [2:0] arsize;
	input [3:0] arlen;
	input [2:0] arprot;
	input [3:0] arcache;
	input [1:0] arlock;
	input [1:0] arburst;
	input arvalid;
	output reg arready;
	input [3:0] arid;
		//read data phase+resp phase
	output reg [31:0] rdata;
	output reg rlast;
	output reg rid;
	output reg rvalid;
	input rready;
	output reg [1:0] rresp;
	byte mem [*];		//this declaration has to be in the beginning not inside posedge of clk
	reg [31:0] awaddr_t_arr[16];		//coz a transaction supports a max. of 16 transfers
	reg [3:0] awlen_t_arr[16];
	reg [2:0] awsize_t_arr[16];
	reg [3:0] awprot_t_arr[16];
	reg [3:0] awcache_t_arr[16];
	reg [31:0] araddr_t_arr[16];
	reg [3:0] arlen_t_arr[16];
	reg [2:0] arsize_t_arr[16];
	reg [2:0] arprot_t_arr[16];
	reg [2:0] arcache_t_arr[16];
	
	bit flag2;
	reg wvalid;	

	initial begin
		$display("I am in the slave");
	end

	always @(posedge aclk or negedge arstn) begin
		if (!arstn) begin
			$display("I am in the reset phase");
		end
		if (awvalid == 1'b0) begin
			awready = 1'b0;
		end
		if (wvalid == 1'b1) begin
			wready = 1'b1;
			mem[awaddr_t_arr[wid]]	= wdata[7:0];
			mem[awaddr_t_arr[wid]+1] = wdata[15:8];
			mem[awaddr_t_arr[wid]+2] = wdata[23:16];
			mem[awaddr_t_arr[wid]+3] = wdata[31:24];
			awaddr_t_arr[wid] = awaddr_t_arr[wid]+4;
		end
		if (wvalid == 1'b0) begin
			wready = 1'b0;
		end
		if (wlast == 1'b1) begin
			resp_phase(wid);	
		end	

		if (awvalid == 1'b1) begin
			awaddr_t_arr[awid] = awaddr;
			awlen_t_arr[awid] = awlen;
			awsize_t_arr[awid] = awsize;
			awprot_t_arr[awid] = awprot;
			awcache_t_arr[awid] = awcache;
			awready = 1'b1;
			//do_write_phase(awid);
		end 	

		if (arvalid == 1'b1) begin
			araddr_t_arr[arid] = araddr;
			arlen_t_arr[arid] = arlen;
			arsize_t_arr[arid] = arsize;
			arprot_t_arr[arid] = arprot;
			arcache_t_arr[arid] = arcache;
			arready = 1'b1;
			do_read_phase(arid);
		end	

		if (arvalid == 1'b0) begin
			arready = 1'b0;
		end
	end
		task resp_phase(bit [3:0] bid_t);
			flag1 = 0;
			bresp = 2'b00;
			bid = bid_t;
			bvalid = 1'b1;
			while (flag1 != 1'b1) begin
				@(posedge aclk);
				if (bready == 1'b1) begin
					flag1=1;
				end
			end
			@(negedge aclk);	
				bvalid = 0;
				//flag1 = 0;
		endtask

//----------------------------------------
//This is the read data phase of program

	task do_read_phase(bit [3:0] arid_t);
		flag1=0;
		rid = arid_t;
		for (int i=0; i<=arlen; i++) begin
			flag1 = 0;
			$display("ARLEN=%d", arlen);
			rdata[7:0] = mem[araddr_t_arr[arid_t]];
			rdata[15:8] = mem[araddr_t_arr[arid_t]+1];
			rdata[23:16] = mem[araddr_t_arr[arid_t]+2];
			rdata[31:24] = mem[araddr_t_arr[arid_t]+3];
			araddr_t_arr[arid]=araddr_t_arr[arid_t]+4;
			rvalid = 1'b1;
			bresp = 2'b00;
			while (flag1!=1) begin
				@(posedge aclk);
					if (rready==1'b1) begin
						flag1=1;
					end
			end
			rvalid = 1'b0;
		end
	endtask
//--------------------------------------------------------------
endmodule
