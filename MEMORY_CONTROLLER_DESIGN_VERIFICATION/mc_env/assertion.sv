module assertion(input clk,
		input rst,
		input cyc,
		input stb,
		input ack,
		input [31:0] data_i,
		input [31:0] data_o,
		input wr_rd
);

sequence cyc_stb;
	cyc && stb;
endsequence

sequence ack_c;
//	##1 (ack);
	(ack);
endsequence

sequence reset_c;
	rst;
endsequence

sequence data_i_c;
	data_i == 32'h0; 
endsequence

sequence data_o_c;
	data_o == 32'h0; 
endsequence

sequence wr_rd_tt;
	wr_rd==0;
endsequence

sequence data_o_r;
	##1 (data_o != 32'h0);
endsequence

/*sequence read_hand;
	(wr_rd_tt && cyc_stb);
endsequence*/

property handshake;
//	@(posedge clk) cyc_stb |-> ack_c;		//This is using overlapping Implication Operator
	@(posedge clk) cyc_stb |=> ack_c;		//This is using non-overlapping implication Operator
endproperty

property res_data_i;
	@(posedge clk) reset_c |-> (data_i_c);
endproperty

property res_data_o;
	@(posedge clk) reset_c |-> (data_o_c);
endproperty

property read_data;
//	@(posedge clk) (cyc_stb && wr_rd_tt) |-> data_o_r;		//We cannot AND a sequence
	@(posedge clk) (cyc && stb && !wr_rd) |-> data_o_r;		//This is how I solved the problem
endproperty

/*assert property (handshake) $display("Pass", $time);
				else $display("Error", $time);	*/

/*assert property (res_data_i) $display("Pass I", $time);
				else $display("Error", $time);

assert property (res_data_o) $display("Pass O", $time);
				else $display("Error", $time);*/
assert property (read_data) $display("Pass", $time);
				else $display("Fail", $time);
	
endmodule	
	
