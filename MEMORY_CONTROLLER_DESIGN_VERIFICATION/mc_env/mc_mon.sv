// TO collect the memory side data we use the mc_mon. we are collecting the data from the mem_intf so we instantiate the mem_intf here. Not sure about which tx class is to be used wb or mc???
//We will get the actual output from the mc_controller i.e. our DUT by collecting the signals from the mem_intf and then pass this on to the checker to compare the actual data with the golden
//data

//So the question is when should we collect the data? 
//- When the data is valid thats when we collect the data i.e. in case of SRAM(for write i.e. when the mc is writing) when adsc, we is low and doe is high, we will capture the addr, data_o
// and wr_rd 


class mc_mon;
	mc_reg reg_h;
	bit [2:0] cs;
	virtual mem_intf mc_mon_vif;
	bit [2:0] mem_typ_t;
	mem_tx memtx=new();
	
	task run();
		mc_mon_vif = mem_cfg::mvif;
		$display("MC_MON::RUN");
		process1();
	endtask

	task process1();
		int i;
		i = 0;
		mem_cfg::mc_reftomc_ckr_mc_reg.get(reg_h);
		forever begin
//			$display("=======HAHAHAHAH========");
//			mem_cfg::mc_reftomc_ckr_mc_reg.get(reg_h);		//this mailbox is of blockig type hence the code wasnt moving further as the mailbox was blocking it. reg_h
//			$display("================HAHAHAH1================");	//is only sent when the tx is of write type. Hence it wasnt doing the read part as it was getting blocked  
			@(negedge mc_mon_vif.mem_clk);				//since there was nothing in the mailbox to get.
			if (mc_mon_vif.mc_cs_o!='hff) begin
				cs = find_cs();
				mem_typ_t = reg_h.csc_h[cs].mem_type;
				$display("MEM_TYPE from the mc_mon=%b", mem_typ_t);
				case (mem_typ_t)
				3'b001: begin
				//SSRAM
					if (!mc_mon_vif.mc_adsc_o && !mc_mon_vif.mc_we_o && mc_mon_vif.mc_doe_doe_o) begin	//checking for the valid data. MC generates valid WRITE data 
																//on this condition
						$display("I came to the write part of monitor");
						memtx.mem_addr = mc_mon_vif.mc_addr_o;
						memtx.mem_data = mc_mon_vif.mc_data_o;
						memtx.wr_rd = 1'b1;
						mem_cfg::mc_montockr.put(memtx);			//This is the actual data thats being passed to the checker for comparison
						repeat(2) begin						//NOTE: This thing does not work: repeat(2); 
							$display("I am waiting here = %d", i);					//	  @(posedge mc_mon_vif.mem_clk);
							i += 1;						//	It has to have begin and end like I changed it to
							@(posedge mc_mon_vif.mem_clk);
						end
					end
					if (!mc_mon_vif.mc_adsc_o && !mc_mon_vif.mc_oe_o && !mc_mon_vif.mc_doe_doe_o) begin
						$display("I came to the read part of monitor");
						memtx.mem_addr = mc_mon_vif.mc_addr_o;			//After the address arrives on the interface, the read data comes on the data_i bus
													//after how many cycles is what we need to determine. Then wait for that many cycles
													//before capturing the data from the data_i bus. It comes after 2 cycles.
						repeat(2) begin
							@(posedge mc_mon_vif.mem_clk);
						end
						memtx.mem_data = mc_mon_vif.mc_data_i;
						memtx.wr_rd = 1'b0;
						mem_cfg::mc_montockr.put(memtx); 
					end		
	/*		3'h000: begin
			//SDRAM
			end		*/
			end
			endcase
			end
			end
		endtask

	function bit[2:0] find_cs();
		case(mc_mon_vif.mc_cs_o)
			8'b0000_0001: begin
				return 0;
			end
			8'b0000_0010: begin
				return 1;
			end
			8'b0000_0100: begin
				return 2;
			end
			8'b0000_1000: begin
				return 3;
			end
			8'b0001_0000: begin
				return 4;
			end
			8'b0010_0000: begin
				return 5;
			end
			8'b0100_0000: begin
				return 6;
			end
			8'b1000_0000: begin
				return 7;
			end
		endcase
	endfunction
endclass
