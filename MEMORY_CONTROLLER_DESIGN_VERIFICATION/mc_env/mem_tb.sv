program mem_tb();
	mem_env env;

	initial begin
		$display("MEM_TB::RUN");
		env = new();
		env.run();
	end
endprogram

