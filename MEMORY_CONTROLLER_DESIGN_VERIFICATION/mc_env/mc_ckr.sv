//COllect the tx from both the refernece model and mem_mon side and do comparison
//The read part is not generating the right data. Need to fix that?????
class mc_ckr;
	mem_tx memtx_gd1, memtx_at1;
	mem_tx memtx_golden, memtx_actual;	
	mem_tx golden[$], actual[$];
	bit answer;
	
	task run();
	fork 
			forever begin
				mem_cfg::reftockr.get(memtx_golden);
				golden.push_back(memtx_golden);
			end

			forever begin
				mem_cfg::mc_montockr.get(memtx_actual);
				actual.push_back(memtx_actual);
			end

			forever begin
				wait ((golden.size() > 0) && (actual.size() > 0)) begin 
					memtx_gd1 = golden.pop_front();
					memtx_at1 = actual.pop_front();
					answer = memtx_gd1.compare(memtx_at1);
					if (answer == 1'b1) begin
						$display("====================Tx Match=====================");
						$display("=====================Expected this=========================");
						memtx_gd1.print();
						$display("=====================Got this==============================");
						memtx_at1.print();
					end	
					else begin
						$display("==========================Tx MisMatch=======================");
						$display("==========================Expected this=====================");
						memtx_gd1.print();
						$display("==========================Got this=========================");
						memtx_at1.print();
					end
				end
			end	
		join
	endtask
endclass
