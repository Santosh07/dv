class csr;
	bit [7:0] refresh_prescaler;
	bit [2:0] ref_int;
	bit fs;
	bit rw;
	bit fvpen;
	bit frdy;

	function void write(bit [31:0] data);
		refresh_prescaler = data[31:24];
		ref_int = data[10:8];
		fs = data[2];
		fvpen = data[1];
		frdy = data[0];
	endfunction

	function bit [31:0] read();
		return {refresh_prescaler, 13'h0, ref_int, 5'h0, fs, fvpen, frdy};
	endfunction
endclass


class ba_mask;
	bit [7:0] mask;
	
	function void write(bit [31:0] data);
		mask = data[7:0]; 
	endfunction

	function bit[31:0] read();
		return {24'h0, mask};
	endfunction
endclass

class csc;
	bit [7:0] sel;
	bit pen;
	bit kro;
	bit bas;
	bit wp;
	bit [1:0] ms;
	bit [1:0] bw;
	bit [2:0] mem_type;
	bit en;

	function void write(bit [31:0] data);
		sel = data[23:16];
		pen = data[11];
		kro = data[10];
		bas = data[9];
		wp = data[8];
		ms = data[7:6];
		bw = data[5:4];
		mem_type = data[3:1];
		en = data[0];
	endfunction

	function bit [31:0] read();
		return {8'h0, sel, 4'h0, pen, kro, bas, wp, ms, bw, mem_type, en};
	endfunction
endclass

class tms_sdram;
	bit [3:0] trfc;
	bit [3:0] trp;
	bit [2:0] trcd;
	bit [1:0] twr;
	bit rw;
	bit wbl;
	bit [1:0] om;
	bit [2:0] cl;
	bit bt;
	bit [2:0] bl;

	function void write(bit [31:0] data);
		trfc = data[27:24];
		trp = data[23:20];
		trcd = data[19:17];
		twr = data[16:15];
		rw = data[9];
		om = data[8:7];
		cl = data[6:4];
		bt = data[3];
		bl = data[2:0];
	endfunction

	function bit [31:0] read();
		return {4'h0, trfc, trp, trcd, twr, rw, om, cl, bt, bl};
	endfunction
endclass

class tms_async;
	bit [5:0] twwd;
	bit [3:0] twd;
	bit [3:0] twpw;
	bit [3:0] trdz;
	bit [7:0] trdv;

	function void write(bit [31:0] data);
		twwd = data[25:20];
		twd = data[19:16];
		twpw = data[15:12];
		trdz = data[11:8];
		trdv = data[7:0];
	endfunction

	function bit [31:0] read();
		return {6'h0, twwd, twd, twpw, trdz, trdv};
	endfunction
endclass

class tms_sync;
	bit [8:0] tto;
	bit [3:0] twr;
	bit [3:0] trdz;
	bit [7:0] trdv;

	function void write(bit [31:0] data);
		tto = data[24:16];
		twr = data[15:12];
		trdz = data[11:8];
		trdv = data[7:0];
	endfunction

	function bit[31:0] read();
		return {7'h0, tto, twr, trdz, trdv};
	endfunction
endclass

class mc_reg;
	csr csr_h;
	ba_mask ba_mask_h;
	csc csc_h[8];
	tms_sdram tms_sdram_h[8];
	tms_sync tms_sync_h[8];
	tms_async tms_async_h[8];
	wb_tx wbtx;
	event e;

//We will do the coverage for each of the chip select so csc_h[0], csc_h[1], csc_h[2] ....
	covergroup mem_cg @ (e);
		CSC_SEL: coverpoint csc_h[0].sel{
			bins sel_cs_1 = {1};
			ignore_bins sel_cs_2 = {2};	
			ignore_bins sel_cs_3 = {4};
			ignore_bins sel_cs_4 = {8};
			ignore_bins sel_cs_5 = {16};
			ignore_bins sel_cs_6 = {32};
			ignore_bins sel_cs_7 = {64};
			ignore_bins sel_cs_8 = {128};
		} 
		CSC_PEN: coverpoint csc_h[0].pen{
			bins pen_1 = {1};
			bins pen_0_disabled = {0};
		} 
		CSC_KRO: coverpoint csc_h[0].kro{
			bins kro_1 = {1};
		}
		CSC_BAS: coverpoint csc_h[0].bas{
			bins bas_1 = {1};
		}
		CSC_WP: coverpoint csc_h[0].wp{
			bins wp_0_enabled = {0};
		}
		CSC_MS: coverpoint csc_h[0].ms{
			bins ms_64 = {0};
			bins ms_128 = {1};
			bins ms_256 = {2};
		}
		CSC_BW: coverpoint csc_h[0].bw{
			bins bw_8_bit_bus = {0};
			bins bw_16_bit_bus = {1};
			bins bw_32_bit_bus = {2};
		}
		CSC_MEM_TYPE: coverpoint csc_h[0].mem_type{
			bins mem_type_SDRAM_0 = {0};
			bins mem_type_SSRAM_1 = {1};
			bins mem_type_ASYNC_2 = {2};
			bins mem_type_SYNC_3 = {3};
		}
		CSC_EN: coverpoint csc_h[0].en{
			bins en_enabled_1 = {1};
			bins en_disabled_0 = {0};
		}
	endgroup

	function new();
		mem_cg = new();
		csr_h = new();
		ba_mask_h = new();
		foreach(csc_h[i]) begin
			csc_h[i] = new();
		end
		foreach(tms_sdram_h[i]) begin
			tms_sdram_h[i] = new();
		end
		foreach(tms_sync_h[i]) begin
			tms_sync_h[i] = new();
		end
		foreach(tms_async_h[i]) begin
			tms_async_h[i] = new();
		end
	endfunction
	
	function void write(bit [31:0] wb_addr_i, bit [31:0] wb_data_i);
		case (wb_addr_i[28:0])
			29'h0: begin
				csr_h.write(wb_data_i);
			end
			29'h8: begin
				ba_mask_h.write(wb_data_i);
			end
			29'h10: begin
				csc_h[0].write(wb_data_i);				
			//	-> e;
			end
			29'h18: begin
				csc_h[1].write(wb_data_i);				
				-> e;
			end
			29'h20: begin
				csc_h[2].write(wb_data_i);				
			end
			29'h28: begin
				csc_h[3].write(wb_data_i);				
			end
			29'h30: begin
				csc_h[4].write(wb_data_i);				
			end
			29'h38: begin
				csc_h[5].write(wb_data_i);				
			end
			29'h40: begin
				csc_h[6].write(wb_data_i);				
			end
			29'h48: begin
				csc_h[7].write(wb_data_i);				
			end
		//Need to know which memory would it be for TMS	
			29'h14: begin
				if (csc_h[0].mem_type == 3'h0) begin
					tms_sdram_h[0].write(wb_data_i);
				end
				else if (csc_h[0].mem_type == 3'h2) begin
					tms_async_h[0].write(wb_data_i);
				end
				else if (csc_h[0].mem_type == 3'h3) begin
					tms_sync_h[0].write(wb_data_i);
				end
			end				
			29'h1c: begin
				if (csc_h[1].mem_type == 3'h0) begin
					tms_sdram_h[1].write(wb_data_i);
				end
				else if (csc_h[1].mem_type == 3'h2) begin
						tms_async_h[1].write(wb_data_i);
					end
				
				else if (csc_h[1].mem_type == 3'h3) begin
						tms_sync_h[1].write(wb_data_i);
					end
			end
			29'h24: begin
				if (csc_h[2].mem_type == 3'h0) begin
					tms_sdram_h[2].write(wb_data_i);
				end
				else if (csc_h[2].mem_type == 3'h2) begin
						tms_async_h[2].write(wb_data_i);
					end
				else if (csc_h[2].mem_type == 3'h3) begin
						tms_sync_h[2].write(wb_data_i);
					end
			end
			29'h2c: begin
				if (csc_h[3].mem_type == 3'h0) begin
					tms_sdram_h[3].write(wb_data_i);
				end
				else if (csc_h[3].mem_type == 3'h2) begin
						tms_async_h[3].write(wb_data_i);
					end
				else if (csc_h[3].mem_type == 3'h3) begin
						tms_sync_h[3].write(wb_data_i);
					end
			end
			29'h34: begin
				if (csc_h[4].mem_type == 3'h0) begin
					tms_sdram_h[4].write(wb_data_i);
				end
				else if (csc_h[4].mem_type == 3'h2) begin
						tms_async_h[4].write(wb_data_i);
					end
				else if (csc_h[4].mem_type == 3'h3) begin
						tms_sync_h[4].write(wb_data_i);
					end
			end
			29'h3c: begin
				if (csc_h[5].mem_type == 3'h0) begin
					tms_sdram_h[5].write(wb_data_i);
				end
				else if (csc_h[5].mem_type == 3'h2) begin
						tms_async_h[5].write(wb_data_i);
					end
				else if (csc_h[5].mem_type == 3'h3) begin
						tms_sync_h[5].write(wb_data_i);
					end
			end
			29'h44: begin
				if (csc_h[6].mem_type == 3'h0) begin
					tms_sdram_h[6].write(wb_data_i);
				end
				else if (csc_h[6].mem_type == 3'h2) begin
						tms_async_h[6].write(wb_data_i);
					end
				else if (csc_h[6].mem_type == 3'h3) begin
						tms_sync_h[6].write(wb_data_i);
					end
			end
			29'h4c: begin
				if (csc_h[7].mem_type == 3'h0) begin
					tms_sdram_h[7].write(wb_data_i);
				end
				else if (csc_h[7].mem_type == 3'h2) begin
						tms_async_h[7].write(wb_data_i);
					end
				else if (csc_h[7].mem_type == 3'h3) begin
						tms_sync_h[7].write(wb_data_i);
					end
			end
		endcase
	endfunction

	function bit[31:0] read(bit [31:0] wb_addr_i);
		case (wb_addr_i[28:0])
			29'h0: begin
				return csr_h.read();
			end
			29'h8: begin
				return ba_mask_h.read();
			end
			29'h10: begin
				return csc_h[0].read();				
			end
			29'h18: begin
				return csc_h[1].read();				
			end
			29'h20: begin
				return csc_h[2].read();				
			end
			29'h28: begin
				return csc_h[3].read();				
			end
			29'h30: begin
				return csc_h[4].read();				
			end
			29'h38: begin
				return csc_h[5].read();				
			end
			29'h40: begin
				return csc_h[6].read();				
			end
			29'h48: begin
				return csc_h[7].read();				
			end
			//Need to know which memory would it be for TMS	
			29'h14: begin
				if (csc_h[0].mem_type == 3'h0) begin
					return tms_sdram_h[0].read();
				end
				else if (csc_h[0].mem_type == 3'h2) begin
						return tms_sync_h[0].read();
					end
				else if (csc_h[0].mem_type == 3'h3) begin
						return tms_async_h[0].read();
					end
			end
			29'h1c: begin
				if (csc_h[1].mem_type == 3'h0) begin
					return tms_sdram_h[1].read();
				end
				else if (csc_h[1].mem_type == 3'h2) begin
						return tms_async_h[1].read();
					end
				else if (csc_h[1].mem_type == 3'h3) begin
						return tms_sync_h[1].read();
					end
			end
			29'h24: begin
				if (csc_h[2].mem_type == 3'h0) begin
					return tms_sdram_h[2].read();
				end
				else if (csc_h[2].mem_type == 3'h2) begin
						return tms_async_h[2].read();
					end
				else if (csc_h[2].mem_type == 3'h3) begin
						return tms_sync_h[2].read();
					end
			end
			29'h2c: begin
				if (csc_h[3].mem_type == 3'h0) begin
					return tms_sdram_h[3].read();
				end
				else if (csc_h[3].mem_type == 3'h2) begin
						return tms_async_h[3].read();
					end
				else if (csc_h[3].mem_type == 3'h3) begin
						return tms_sync_h[3].read();
					end
			end
			29'h34: begin
				if (csc_h[4].mem_type == 3'h0) begin
					return tms_sdram_h[4].read();
				end
				else if (csc_h[4].mem_type == 3'h2) begin
						return tms_async_h[4].read();
					end
				else if (csc_h[4].mem_type == 3'h3) begin
						return tms_sync_h[4].read();
					end
			end
			29'h3c: begin
				if (csc_h[5].mem_type == 3'h0) begin
					return tms_sdram_h[5].read();
				end
				else if (csc_h[5].mem_type == 3'h2) begin
						return tms_async_h[5].read();
					end
				else if (csc_h[5].mem_type == 3'h3) begin
						return tms_sync_h[5].read();
					end
			end
			29'h44: begin
				if (csc_h[6].mem_type == 3'h0) begin
					return tms_sdram_h[6].read();
				end
				else if (csc_h[6].mem_type == 3'h2) begin
						return tms_async_h[6].read();
					end
				else if (csc_h[6].mem_type == 3'h3) begin
						return tms_sync_h[6].read();
				end
			end
			29'h4c: begin
				if (csc_h[7].mem_type == 3'h0) begin
					return tms_sdram_h[7].read();
				end
				else if (csc_h[7].mem_type == 3'h2) begin
						return tms_async_h[7].read();
					end
				else if (csc_h[7].mem_type == 3'h3) begin
						return tms_sync_h[7].read();
					end
			end			
		endcase
	endfunction
endclass

/*This code will go into the mc_monitor
if (mvif.adsc==0 && mvif.we == 0) begin	//mc is writing into the memory
	mem_addr_i = mvif.addr_i;
	mem_data_i = mvif.data_o;
end
else if (mvif.adv && mvif.oe == 0) begin  //mc is reading the data from the memory
	mem_addr_i = mvif.addr_i;
	mem_data_i = mvif.data_i;
end*/
