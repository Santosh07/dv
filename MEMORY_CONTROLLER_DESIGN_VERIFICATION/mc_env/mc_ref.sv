class mc_ref;
	wb_tx wbtx;
	mc_reg reg_h;	
	mem_tx memtx;
	bit cs;	
	bit [2:0] mem_type_t;

	function new();
		reg_h = new();
		memtx = new();
	endfunction

	task run();
		$display("WB_REF::Inside Reference Model");
		forever begin
			mem_cfg::montoref.get(wbtx);		//This is coming from the wb interface, we are intercepting the actual signal that is generated from the wb side to generate the
								//the actual data
/*			$display("Printing from inside the ref model");
			wbtx.print();
			$display("End printing from inside the ref model");		*/
//			process1(wbtx);
			process1();		//passing of handle is not required 
		end
	endtask
	
//	task process1(wb_tx wbtx);
	task process1();
		cs = find_cs(wbtx.wb_addr_i);		//calling the find function to find the cs that is active. Taking it out as it is common for both read and write
//		$display("Blah new");
//		$display("ADDDDRRRRR=%h", wbtx.wb_addr_i);	
//		$display("WB_WE_I=%s", wbtx.wb_we_i);
		if (wbtx.wb_addr_i[31:29] == 3'b011) begin		//Checking to see if the address is targeted to register.
			case (wbtx.wb_we_i)
				WRITE: begin				//if its write, writing it to the appropriate register by passing the address, data. we are doing register modelling
					reg_h.write(wbtx.wb_addr_i, wbtx.wb_data_i);	
		                end
				READ: begin
					wbtx.wb_data_i = reg_h.read(wbtx.wb_addr_i);		//not sure whether we need this, if its read, maybe because we need to check whether the
												//register modelling is working fine. So we will first write into it and then read back just to
												//check
				end
			endcase
		end
		if (wbtx.wb_addr_i[31:29] == 3'b000) begin			//if the address is targeted to memory (i.e. if [31:29] is 000) then we need to first get which chip select
										//is selected. Once we get that, we will find the memory type that is connected to that chip select and based on
										//the memory type that is selected, we will generate the address (coz address changes coz of memory mapping
										//e.g. for SSRAM the address gets sliced to [25:2] of wishbone address). This will then be passed to the checker
										//which will act as golden output and will be compared with the actual output.

			mem_cfg::mc_reftomc_ckr_mc_reg.put(reg_h);		//This is going to mc_mon and not mc_ckr to find the mem_typ after finding the cs. Not sure why????
										//Coz according to me, even this needs to be checked meaning if the tb configures mem_cntl for tx with certain
										//type of memory, if the mem_cntl is working fine, it should generate the appropriate tx. But by sending this
										//throught the mb to ckr, we are not doing that. we are directly getting what the tb had configured and
										//not checking to see whether the mem_cntl is generating the appropriate signals
										//HERE's THE ANSWER I HAVE FIGURED OUT:
										// - If the mc_cntrl is working fine, its internal register configuration will match with that we have 
										//	modelled as golden register modelling. e.g. in the mc_mon by analyzing the cs, we determine cs0 has been selected.
										//  Then by using this information and reading the csc0 register of our golden reference model, we determine that SRAM has been connected to
										// to cs0. Then we start collecting the signals using SRAM signals (i.e adsc, we, oe, doe). If these signals do not toggle, that means there is
										// something wrong with the mc_cntrl configuration. If it toggles, that means the mc_cntrl is working fine.
			case (wbtx.wb_we_i)

				WRITE: begin
//					$display("BLAHH11111");
//					$display("CS = %d", cs);
					if (cs == 0) begin
						mem_type_t = reg_h.csc_h[0].mem_type;
						case (mem_type_t)
							3'b001: begin				//SSRAM
								$display("I came to write reference model");
								memtx.mem_addr = wbtx.wb_addr_i[25:2];
								memtx.mem_data = wbtx.wb_data_i;
								memtx.wr_rd = 1'b1;
								mem_cfg::reftockr.put(memtx);		//So this is supposed to be actual/golden data and is being passed to the checker 
													//for comparison 
							end
							3'b000: begin
								//Similarly do for SDRAM
							/*	memtx.mem_addr = wbtx.wb_addr_i[];	//Need to check in the SDRAM design which part of the address is getting sent
								memtx.mem_data = wbtx.wb_data_i;
								memtx.wr_rd = 1'b1;
								mem_cfg::reftockr.put(memtx);		*/
							end
							3'b010: begin
								memtx.mem_addr = wbtx.wb_addr_i[24:1];	
								memtx.mem_data = wbtx.wb_data_i;
								memtx.wr_rd = 1'b1;
								mem_cfg::reftockr.put(memtx);
								//SImilarly for ASYNC
								$display("Printing from Reference");
								memtx.print();
								$display("End of printing from the reference");
							end
							3'b011: begin
								//SYNC
							end
						endcase
					end 
				end
				READ: begin
					$display("I came inside read reference model");
					if (cs == 0) begin
						mem_type_t = reg_h.csc_h[0].mem_type;
						case (mem_type_t)
							3'b001: begin				//SSRAM
								$display("I came to read reference model`");
								memtx.mem_addr = wbtx.wb_addr_i[25:2];
								memtx.mem_data = wbtx.wb_data_i;
								memtx.wr_rd = 1'b0;
								mem_cfg::reftockr.put(memtx);		//So this is supposed to be actual/golden data and is being passed to the checker 
													//for comparison 
								$display("Printing from inside the read referece model::Actual read data");
								memtx.print();
								$display("End of printing the read reference model::Actual read data");		
							end
							3'b000: begin
								//Similarly do for SDRAM
							end
							3'b010: begin
								//SImilarly for ASYNC
								memtx.mem_addr = wbtx.wb_addr_i[24:1];
								memtx.mem_data = wbtx.wb_data_i;
								memtx.wr_rd = 1'b0;
								mem_cfg::reftockr.put(memtx);
								$display("Printing the read part from inside the reference");
								memtx.print();
								$display("End of printing the read part from inside the reference");
							end
							3'b011: begin
								//SYNC
							end	
						endcase
					end
				end
			endcase
		end
	endtask

	function find_cs(bit [31:0] addr);
		if (addr[28:21] == (reg_h.ba_mask_h.mask[7:0] & reg_h.csc_h[0].sel)) begin
			return 0;
		end

		if (addr[28:21] == (reg_h.ba_mask_h.mask[7:0] & reg_h.csc_h[1].sel)) begin
			return 1;
		end
		
		if (addr[28:21] == (reg_h.ba_mask_h.mask[7:0] & reg_h.csc_h[2].sel)) begin
			return 2;
		end
		
		if (addr[28:21] == (reg_h.ba_mask_h.mask[7:0] & reg_h.csc_h[3].sel)) begin
			return 3;
		end
		
		if (addr[28:21] == (reg_h.ba_mask_h.mask[7:0] & reg_h.csc_h[4].sel)) begin
			return 4;
		end
		
		if (addr[28:21] == (reg_h.ba_mask_h.mask[7:0] & reg_h.csc_h[5].sel)) begin
			return 5;
		end
		
		if (addr[28:21] == (reg_h.ba_mask_h.mask[7:0] & reg_h.csc_h[6].sel)) begin
			return 6;
		end
		
		if (addr[28:21] == (reg_h.ba_mask_h.mask[7:0] & reg_h.csc_h[7].sel)) begin
			return 7;
		end

	endfunction
endclass

