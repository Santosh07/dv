class mem_tx;
	bit [23:0] mem_addr;
	bit [31:0] mem_data;
	bit wr_rd;

	function void print();
		$display("Mem_Addr = %h", mem_addr);
		$display("Mem_Data = %h", mem_data);
		$display("Wr_Rd = %b", wr_rd);
	endfunction

	function bit compare(mem_tx tx);
			if ((mem_addr!=tx.mem_addr) || (mem_data!=tx.mem_data) || (wr_rd!=tx.wr_rd)) begin
				return 0;
			end
			else begin
				return 1;
			end
	endfunction
endclass
