//This is the main memory environment 
class mem_env;
	wb_env wbenv_h = new();
	mc_ref mc_ref_h = new();
	mc_mon mc_mon_h = new();
	mc_ckr mc_ckr_h = new();	

	task run();
		$display("MEM_ENV::RUN");
		fork
			wbenv_h.run();
			mc_ref_h.run();
			mc_mon_h.run();
			mc_ckr_h.run();
		join
	endtask
endclass
