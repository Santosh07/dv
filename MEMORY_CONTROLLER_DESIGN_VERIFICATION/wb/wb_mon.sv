class wb_mon;
	virtual wb_intf wb_mon_vif; 
	wb_tx wbtx;
	//Collect the valid control and data into the transaction and send it wherever required like cov, reference model
	task run();
		$display("WB_MON::RUN");
		wb_mon_vif = mem_cfg::wvif;
		forever begin				//if we do not do forever loop, it will do it just once when the monitor gets called so it will never get all three one
			@(negedge wb_mon_vif.wb_clk);	//we are sampling at the negedge of clk to get the most stable data
			if (wb_mon_vif.wb_cyc_i && wb_mon_vif.wb_stb_i && wb_mon_vif.wb_ack_o) begin		//Collecting the signals only when they are valid
				if (wb_mon_vif.wb_we_i == 1) begin
	//				$display("***********************All three became 1********************************");
					wbtx = new();					//got to create a memory
					wbtx.wb_addr_i = wb_mon_vif.wb_addr_i;		
					wbtx.wb_data_i = wb_mon_vif.wb_data_i;
					wbtx.wb_we_i = WRITE;
					
					mem_cfg::montocov.put(wbtx);	
					mem_cfg::montoref.put(wbtx);
/*					$display("Printing from write monitor");
					wbtx.print();
					$display("Ending printing from the write monitor");	*/
				end
				if (wb_mon_vif.wb_we_i == 0) begin
					wbtx.wb_addr_i = wb_mon_vif.wb_addr_i;
					wbtx.wb_data_i = wb_mon_vif.wb_data_o;
					wbtx.wb_we_i = READ;
					mem_cfg::montocov.put(wbtx);
					mem_cfg::montoref.put(wbtx);	
/*					$display("Printing from read monitor");
					wbtx.print();
					$display("Ending printing from the read monitor");		*/
				end
			end
		end
	endtask
endclass
