//How will you improve the coverage?
// - By running more number of times
// - By changing the seed

//What do you mean by directed testcase?
// - If even after running more number of times, some features does not get covered or hit, we write a new testcase specifically targetting that feature, this is known as directed testcase
/*
class name_of_coverage_class;
	tx_handle;

	covergroup name_of_the_covergroup;
		name_of_coverpoint: coverpoint instance_handle.tx_needed_to_be_sampled{
					bins name_of_the_bin = {[range]};
					}
	endgroup

	function new();
		covergroup_handle = new()		//creating memory for the covergroup
	endfunction

	task run();
		forever begin
			name_of_the_cfg_class::mbx(tx);		//getting tx from the mon
			covergroup_handle.sample()
		end
	endtask
// Sampling usimg .sample
class wb_cov;
	wb_tx wbtx;

	covergroup mem_cg;
		ADDR_I: coverpoint wbtx.wb_addr_i{
				bins SMALL1 = {[`CSC0_SA_ADDR:`CSC0_END_ADDR]};	
				bins SMALL2 = {[`CSC1_SA_ADDR:`CSC1_END_ADDR]};		
				bins SMALL3 = {[`CSC2_SA_ADDR:`CSC2_END_ADDR]};		
				bins SMALL4 = {[`CSC3_SA_ADDR:`CSC3_END_ADDR]};		
				bins SMALL5 = {[`CSC4_SA_ADDR:`CSC4_END_ADDR]};		
				bins MEDIUM = {[`CSC5_SA_ADDR:`CSC5_END_ADDR]};
				bins LARGE = {[`CSC6_SA_ADDR:`CSC6_END_ADDR]};
				bins SMALL7 = {[`CSC7_SA_ADDR:`CSC7_END_ADDR]};		
		}	

		DATA_I: coverpoint wbtx.wb_data_i{
				bins SMALL = {[0:255]};
				bins MEDIUM = {[256:400]};
				bins LARGE = {[401:600]};
		}
	endgroup

	function new();
		mem_cg = new();
	endfunction

	task run();
		forever begin
			mem_cfg::montocov.get(wbtx);
			mem_cg.sample();
		end		
	endtask
endclass
*/






//Sampling at event
class wb_cov;
	wb_tx wbtx;
	event e;
	
	covergroup mem_cg @ (e);
		WR_RD: coverpoint wbtx.wb_we_i{
						bins READ = {1'b0};
						bins WRITE = {1'b1};
						}
	
		ADDR_I: coverpoint wbtx.wb_addr_i{
				bins SMALL1 = {[`CSC0_SA_ADDR:`CSC0_END_ADDR]};	
				bins SMALL2 = {[`CSC1_SA_ADDR:`CSC1_END_ADDR]};		
				bins SMALL3 = {[`CSC2_SA_ADDR:`CSC2_END_ADDR]};		
				bins SMALL4 = {[`CSC3_SA_ADDR:`CSC3_END_ADDR]};		
				bins SMALL5 = {[`CSC4_SA_ADDR:`CSC4_END_ADDR]};		
				bins MEDIUM = {[`CSC5_SA_ADDR:`CSC5_END_ADDR]};
				bins LARGE = {[`CSC6_SA_ADDR:`CSC6_END_ADDR]};
				bins SMALL7 = {[`CSC7_SA_ADDR:`CSC7_END_ADDR]};		
		}	

		WR_RD_ADDR_CROSS: cross WR_RD, ADDR_I;
	endgroup

	function new();
		mem_cg = new();
	endfunction

	task run();
		forever begin
			mem_cfg::montocov.get(wbtx);
/*			$display("Printing from inside the coverage");
			wbtx.print();
			$display("End printing from inside the coverage");		*/
			-> e;
		end
	endtask
endclass
