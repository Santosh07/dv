class wb_gen;
	bit [31:0] wdata;
	wb_tx wbtx;
	wb_tx wdata_Q[$];
	wb_tx wdata_Q1[$];

	task run();
		case (mem_cfg::testname)
			"sanity_reset_reg_read": begin
				for (int i = 0; i<3; i++) begin
					wbtx = new();
					assert(wbtx.randomize() with {wbtx.wb_we_i==READ;		//to generate read tx/
							              wbtx.reg_mem_f == 1;	
								      wbtx.wb_addr_i[28:0] == i*4;});
					mem_cfg::wb_gentowb_bfm.put(wbtx);
				end
				
				for (int i = 4; i<=19; i++) begin
					wbtx = new();
					assert(wbtx.randomize() with {wbtx.wb_we_i==READ;		//to generate read tx/	
								      wbtx.reg_mem_f == 1;
								      wbtx.wb_addr_i[28:0] == i*4;});
					mem_cfg::wb_gentowb_bfm.put(wbtx);
				end
			end
// Whats the importance of using assert?
// - Assert lets us know whether the assertion passsed or failed. 
// When does the assertion fail?
// - When there are conflicting constraints. Also, with assert, debugging becomes easier as it prints out a message saying assertion error when the assertion fail.

			"test_reg_write_read": begin
				$display("I came inside 'test_reg_write_read'");
				for (int i = 0; i<3; i++) begin
	                 	 	wbtx = new();
					assert(wbtx.randomize() with {wbtx.wb_addr_i[28:0] == i*4;
								      wbtx.wb_we_i == WRITE;
								      wbtx.reg_mem_f == 1;});
					wbtx.wb_data_i = wbtx.wb_data_i & mem_cfg::reg_mask[i];
					mem_cfg::wb_gentowb_bfm.put(wbtx);
				end
				
				for (int i = 4; i<=19; i++) begin
					wbtx = new();
					assert(wbtx.randomize() with {wbtx.wb_addr_i[28:0] == i*4;
								      wbtx.wb_we_i == WRITE;
								      wbtx.reg_mem_f == 1;}); 
					wbtx.wb_data_i = wbtx.wb_data_i & mem_cfg::reg_mask[i];		//few of the bits in the registers are masked bits (i.e. read-only & cannot be written)
					mem_cfg::wb_gentowb_bfm.put(wbtx);				//These bits are given values of 0 in the reg_mask register and then anded to wb_data_i
				end									//so that these bits will end up being 0 when they are being written
			
				for (int i = 0; i<3; i++) begin
	                 	 	wbtx = new();
					assert(wbtx.randomize() with {wbtx.wb_addr_i[28:0] == i*4;
								      wbtx.wb_we_i == READ;
								      wbtx.reg_mem_f == 1;}); 
					wbtx.wb_data_i = wbtx.wb_data_i & mem_cfg::reg_mask[i];		//when I am reading from the registers why am I sending this data. This is actually
					mem_cfg::wb_gentowb_bfm.put(wbtx);				//not required since we are not using this in the BFM. Should be commented out. Does not matter as it is not being used.
				end
				
				for (int i = 4; i<=19; i++) begin
					wbtx = new();
					assert(wbtx.randomize() with {wbtx.wb_addr_i[28:0] == i*4;
								      wbtx.wb_we_i == READ;
								      wbtx.reg_mem_f == 1;}); 
					wbtx.wb_data_i = wbtx.wb_data_i & mem_cfg::reg_mask[i];
					mem_cfg::wb_gentowb_bfm.put(wbtx);
				end
			 end
//This testcase is to test the chip select that is connected to the memory models.
			"chip_select_0_SSRAM": begin
//Test case failing. Design is not writing the data in the first address. Its writing the second data in the first address.  
//So we will be targetting the BA_MASK register and CSCR[0].
//1. Writing to the BA_MASK register
				wbtx = new();
				wdata = 32'hff;
				assert (wbtx.randomize() with {wbtx.wb_addr_i == `BA_MASK;			//writing to the BA_MASK register
							       wbtx.wb_we_i == WRITE;		
							       wbtx.wb_data_i == wdata;});			//writing a value of 32'hff
	
				mem_cfg::wb_gentowb_bfm.put(wbtx);

//2.Writing to CSC[0] register
//Notes: WP is 1 in order to enable writing to SSRAM. If WP is 0, it will give error when we write data into SSRAM. (WP - Write Protected)
				wbtx = new();
				wdata = {8'h00, 8'h01, 4'b0000, 1'b0, 1'b0, 1'b0, 1'b0,2'b00, 2'b10 ,3'b001, 1'b1};				
				assert(wbtx.randomize() with {wbtx.wb_we_i==WRITE;
							      wbtx.wb_addr_i == `CSC0;
							      wbtx.wb_data_i == wdata;});
				mem_cfg::wb_gentowb_bfm.put(wbtx);
//Now we will perform 5 writes to SSRAM and then read it back just to see whether memory controller's chip select feature is working fine.
				for (int i = 0; i<3; i++) begin
					wbtx = new();
					assert(wbtx.randomize() with {wbtx.wb_addr_i inside {[`CSC0_SA_ADDR:`CSC0_END_ADDR]};
								      wbtx.wb_we_i == WRITE;});
					wdata_Q.push_back(wbtx);
					mem_cfg::wb_gentowb_bfm.put(wbtx);
				end
	
				wbtx = new();
				assert(wbtx.randomize() with {wbtx.wb_we_i == READ;
							      wbtx.wb_addr_i == `BA_MASK;
								});
				mem_cfg::wb_gentowb_bfm.put(wbtx);
		
				wbtx = new();
				assert(wbtx.randomize() with {wbtx.wb_we_i == READ;
							      wbtx.wb_addr_i == `CSC0;
								});
				mem_cfg::wb_gentowb_bfm.put(wbtx);
	
			
				for (int i = 0; i<3; i++) begin
					wbtx = new();
					assert (wbtx.randomize() with {wbtx.wb_addr_i == wdata_Q[i].wb_addr_i;
								       wbtx.wb_we_i == READ;});
					mem_cfg::wb_gentowb_bfm.put(wbtx);
				end				
			end
		
			"SSRAM_to_all_CS": begin
				wbtx = new();
				wdata = 32'hff;
				assert (wbtx.randomize() with {wbtx.wb_addr_i == `BA_MASK;
							       wbtx.wb_data_i == wdata;
								wbtx.wb_we_i == WRITE;});
				mem_cfg::wb_gentowb_bfm.put(wbtx);

				wbtx = new();
				wdata = {8'h00, 8'h01, 4'b0000, 1'b0, 1'b0, 1'b0, 1'b0,2'b00, 2'b10 ,3'b001, 1'b1};				
				assert (wbtx.randomize() with {wbtx.wb_addr_i == `CSC0;
							       wbtx.wb_data_i == wdata;
								wbtx.wb_we_i == WRITE;});
				mem_cfg::wb_gentowb_bfm.put(wbtx);
		
				for (int i = 0; i<2;i++) begin
					wbtx = new();
					assert(wbtx.randomize() with {wbtx.wb_addr_i inside {[`CSC0_SA_ADDR:`CSC0_END_ADDR]};
									wbtx.wb_we_i == WRITE;});
					wdata_Q.push_back(wbtx);
					mem_cfg::wb_gentowb_bfm.put(wbtx);
				end	

				for (int i = 0; i<2;i++) begin
					wbtx = new();
					assert(wbtx.randomize() with {wbtx.wb_addr_i == wdata_Q[i].wb_addr_i;
									wbtx.wb_we_i == READ;});
					mem_cfg::wb_gentowb_bfm.put(wbtx);
				end		
			
				wbtx = new();
				wdata = 32'hff;
				assert (wbtx.randomize() with {wbtx.wb_addr_i == `BA_MASK;
							       wbtx.wb_data_i == wdata;
								wbtx.wb_we_i == WRITE;});
				mem_cfg::wb_gentowb_bfm.put(wbtx);

				wbtx = new();
				wdata = {8'h00, 8'h02, 4'b0000, 1'b0, 1'b0, 1'b0, 1'b0,2'b00, 2'b10 ,3'b001, 1'b1};				
				assert (wbtx.randomize() with {wbtx.wb_addr_i == `CSC1;
							       wbtx.wb_data_i == wdata;
								wbtx.wb_we_i == WRITE;});
				mem_cfg::wb_gentowb_bfm.put(wbtx);
		
				for (int i = 0; i<2;i++) begin
					wbtx = new();
					assert(wbtx.randomize() with {wbtx.wb_addr_i inside {[`CSC1_SA_ADDR:`CSC1_END_ADDR]};
									wbtx.wb_we_i == WRITE;});
					wdata_Q1.push_back(wbtx);
					mem_cfg::wb_gentowb_bfm.put(wbtx);
				end	

				for (int i = 0; i<2;i++) begin
					wbtx = new();
					assert(wbtx.randomize() with {wbtx.wb_addr_i == wdata_Q1[i].wb_addr_i;
									wbtx.wb_we_i == READ;});
					mem_cfg::wb_gentowb_bfm.put(wbtx);
				end		
			end	

			"chip_select_0_SDRAM":begin
//Precharge: Once the row inside the row buffer is closed after performing either read or write operation, it is precharged before storing it back to its original position.
//Activate: The requested row is activated and copied to the row buffer of the corresponding bank.
//RD/WR: The row is activated and brought into the row buffer of the corresponding bank, column address is issued and the data is read/written to. After this, it is precharged and then 
//	 stored back to its original position inside the bank (also known as row close). 
//Refresh cycle: The memory tends to loose charge overtime as it is made up of capacitor. In order to retain the charge, it requires periodic refresh
//CL: Column to data delay
//RCD: Row to column delay
//Trfc: Auto-rferesh to activate command delay
//Tpc: Precharge to auto-refresh delay

				//TO test for the chip select, we only configure CSC, BA_MASK and TMS register.
				wbtx = new();
				wdata = 32'hff;
				assert(wbtx.randomize() with {wbtx.wb_addr_i == `BA_MASK;
								wbtx.wb_data_i == wdata;
								wbtx.wb_we_i == WRITE;});	
				mem_cfg::wb_gentowb_bfm.put(wbtx);

/*				wbtx = new();
				wdata = ;
				assert(wbtx.randomize() with {wbtx.wb_addr_i == `CSR;
								wbtx.wb_data_i == wdata;
								wbtx.wb_we_i == WRITE;});
				mem_cfg::wb_gentowb_bfm.put(wbtx);		*/
		
				wbtx = new();
				wdata = {8'h00, 8'h01, 4'b0000, 1'b0, 1'b1, 1'b1, 1'b0,2'b00, 2'b10 ,3'b000, 1'b1};				
				assert(wbtx.randomize() with {wbtx.wb_addr_i == `CSC0;
								wbtx.wb_data_i == wdata;
								wbtx.wb_we_i == WRITE;});	
				mem_cfg::wb_gentowb_bfm.put(wbtx);

				wbtx = new();
				wdata = {4'h0, 4'h7, 4'h2, 3'h2, 2'h2, 5'h0, 1'b1, 2'h0, 3'h3, 1'b0, 3'h0};
				assert(wbtx.randomize() with {wbtx.wb_addr_i == `TMS0;
								wbtx.wb_data_i == wdata;
								wbtx.wb_we_i == WRITE;});
				mem_cfg::wb_gentowb_bfm.put(wbtx);
	
			
				for (int i = 0; i<3;i++) begin
					wbtx = new();
					assert(wbtx.randomize() with {wbtx.wb_addr_i inside {[`CSC0_SA_ADDR:`CSC0_END_ADDR]};
									wbtx.wb_we_i == WRITE;});
					wdata_Q1.push_back(wbtx);
					mem_cfg::wb_gentowb_bfm.put(wbtx);
				end	

				for (int i = 0; i<3;i++) begin
					wbtx = new();
					assert(wbtx.randomize() with {wbtx.wb_addr_i == wdata_Q1[i].wb_addr_i;
									wbtx.wb_we_i == READ;});
					mem_cfg::wb_gentowb_bfm.put(wbtx);
				end		
			end	

			"chip_select_0_SYNCS": begin
				wbtx = new();
				wdata = 32'hff;
				assert(wbtx.randomize() with {wbtx.wb_addr_i == `BA_MASK;
								wbtx.wb_data_i == wdata;
								wbtx.wb_we_i == WRITE;});	
				mem_cfg::wb_gentowb_bfm.put(wbtx);

/*				wbtx = new();
				wdata = ;
				assert(wbtx.randomize() with {wbtx.wb_addr_i == `CSR;
								wbtx.wb_data_i == wdata;
								wbtx.wb_we_i == WRITE;});
				mem_cfg::wb_gentowb_bfm.put(wbtx);		*/
		
				wbtx = new();
				wdata = {8'h00, 8'h01, 4'b0000, 1'b0, 1'b1, 1'b1, 1'b0,2'b00, 2'b10 ,3'h3, 1'b1};				
				assert(wbtx.randomize() with {wbtx.wb_addr_i == `CSC0;
								wbtx.wb_data_i == wdata;
								wbtx.wb_we_i == WRITE;});	
				mem_cfg::wb_gentowb_bfm.put(wbtx);

				wbtx = new();
				wdata = {7'h0, 9'h1ff, 4'h4, 4'h5, 4'h5};
				assert(wbtx.randomize() with {wbtx.wb_addr_i == `TMS0;
								wbtx.wb_data_i == wdata;
								wbtx.wb_we_i == WRITE;});
				mem_cfg::wb_gentowb_bfm.put(wbtx);
	
			
				for (int i = 0; i<3;i++) begin
					wbtx = new();
					assert(wbtx.randomize() with {wbtx.wb_addr_i inside {[`CSC0_SA_ADDR:`CSC0_END_ADDR]};
									wbtx.wb_we_i == WRITE;});
					wdata_Q1.push_back(wbtx);
					mem_cfg::wb_gentowb_bfm.put(wbtx);
				end	

				for (int i = 0; i<3;i++) begin
					wbtx = new();
					assert(wbtx.randomize() with {wbtx.wb_addr_i == wdata_Q1[i].wb_addr_i;
									wbtx.wb_we_i == READ;});
					mem_cfg::wb_gentowb_bfm.put(wbtx);
				end		
			end
//Doubt: Data bus width for an Async(Flash) device is 16 bits whereas memory controller has a bus width of 32 bits. How does this work	
//	 It was the same in the case of SRAM where SRAM's data bus was also 16 bits wide so we connected two instances of SRAM in the top file to make it 32 bits and also configured the
//	 SRAM's memory bus width in the CSC register as 32. But in case of FLASH, the data bus width is same i.e. 16 bits and the configuration over here is 16 bits only not 32 bits in the 
//	 CSC register also just one instance of FLASH is connected in the top file
			"chip_select_0_FLASH_ASYNC": begin
				wbtx = new();
				wdata = {8'h00, 13'h0, 3'h0, 5'h0, 1'b0, 1'b1, 1'b0};		//Added by my just to check whether it works after this but it does not. SIr will let us know
				assert(wbtx.randomize() with {wb_addr_i == `CSR;
							      wb_data_i == wdata;
							      wb_we_i == WRITE;});
				mem_cfg::wb_gentowb_bfm.put(wbtx);

				wbtx = new();
				wdata = 32'hff;
				assert(wbtx.randomize() with {wb_addr_i == `BA_MASK;
							      wb_data_i == wdata;
							      wb_we_i == WRITE;});
				mem_cfg::wb_gentowb_bfm.put(wbtx);

				wbtx = new();
				wdata = {8'h00, 8'h01, 4'h0, 1'b0, 1'b0, 1'b0, 1'b0, 2'b00, 2'b01, 3'h2, 1'b1};
				assert(wbtx.randomize() with {wb_addr_i == `CSC0;
							      wb_data_i == wdata;
							      wb_we_i == WRITE;});
				mem_cfg::wb_gentowb_bfm.put(wbtx);

				wbtx = new();
				wdata = {8'h00, 8'h00, 4'h0, 1'b0, 1'b0, 1'b0,6'h4, 4'h2, 4'h2, 4'h2, 8'h03};
				assert(wbtx.randomize() with {wb_addr_i == `TMS0;
							      wb_data_i == wdata;
							      wb_we_i == WRITE;});
				mem_cfg::wb_gentowb_bfm.put(wbtx);

				for (int i=0; i<3; i++) begin
					wbtx = new();
					assert(wbtx.randomize() with {wb_addr_i inside {[`CSC0_SA_ADDR:`CSC0_END_ADDR]};
								    //  wb_data_i == wdata;
								      wb_we_i == WRITE;});
					wdata_Q1.push_back(wbtx);
					mem_cfg::wb_gentowb_bfm.put(wbtx);
				end

				for (int i=0; i<3; i++) begin
					wbtx = new();
					assert(wbtx.randomize() with {wb_addr_i == wdata_Q1[i].wb_addr_i;
								    //  wb_data_i == wdata;
								      wb_we_i == READ;});
					mem_cfg::wb_gentowb_bfm.put(wbtx);
				end
			end

			"SDRAM_all_cs": begin
					wbtx = new();
					wdata = 32'hff;
					assert(wbtx.randomize() with {wb_addr_i == `BA_MASK;
									wb_data_i == wdata;
									wb_we_i == WRITE;});
					mem_cfg::wb_gentowb_bfm.put(wbtx);
				
					wbtx = new();
					wdata = {8'h00, 8'h01, 4'b0000, 1'b0, 1'b1, 1'b1, 1'b0,2'b00, 2'b10 ,3'b000, 1'b1};				
					assert(wbtx.randomize() with {wb_addr_i == `CSC0;
									wb_data_i == wdata;
									wb_we_i == WRITE;});
					mem_cfg::wb_gentowb_bfm.put(wbtx);
				
					wbtx = new();
					wdata = {4'h0, 4'h7, 4'h7, 3'h4, 2'h3, 5'h0, 1'b1 , 2'h0, 3'h2, 1'b0, 3'h0};				
					assert(wbtx.randomize() with {wb_addr_i == `TMS0;
									wb_data_i == wdata;
									wb_we_i == WRITE;});
					mem_cfg::wb_gentowb_bfm.put(wbtx);
		
					wbtx = new();
					wdata = {8'h00, 8'h02, 4'b0000, 1'b0, 1'b1, 1'b1, 1'b0,2'b00, 2'b10 ,3'b000, 1'b1};				
					assert(wbtx.randomize() with {wb_addr_i == `CSC1;
									wb_data_i == wdata;
									wb_we_i == WRITE;});
					mem_cfg::wb_gentowb_bfm.put(wbtx);
				
					wbtx = new();
					wdata = {4'h0, 4'h7, 4'h7, 3'h4, 2'h3, 5'h0, 1'b1 , 2'h0, 3'h2, 1'b0, 3'h0};				
					assert(wbtx.randomize() with {wb_addr_i == `TMS1;
									wb_data_i == wdata;
									wb_we_i == WRITE;});
					mem_cfg::wb_gentowb_bfm.put(wbtx);
	
	
					wbtx = new();
					wdata = {8'h00, 8'h04, 4'b0000, 1'b0, 1'b1, 1'b1, 1'b0,2'b00, 2'b10 ,3'b000, 1'b1};				
					assert(wbtx.randomize() with {wb_addr_i == `CSC2;
									wb_data_i == wdata;
									wb_we_i == WRITE;});
					mem_cfg::wb_gentowb_bfm.put(wbtx);

					wbtx = new();
					wdata = {4'h0, 4'h7, 4'h7, 3'h4, 2'h3, 5'h0, 1'b1 , 2'h0, 3'h2, 1'b0, 3'h0};				
					assert(wbtx.randomize() with {wb_addr_i == `TMS2;
									wb_data_i == wdata;
									wb_we_i == WRITE;});
					mem_cfg::wb_gentowb_bfm.put(wbtx);
	
				
					wbtx = new();
					wdata = {8'h00, 8'h08, 4'b0000, 1'b0, 1'b1, 1'b1, 1'b0,2'b00, 2'b10 ,3'b000, 1'b1};				
					assert(wbtx.randomize() with {wb_addr_i == `CSC3;
									wb_data_i == wdata;
									wb_we_i == WRITE;});
					mem_cfg::wb_gentowb_bfm.put(wbtx);
	
					wbtx = new();
					wdata = {4'h0, 4'h7, 4'h7, 3'h4, 2'h3, 5'h0, 1'b1 , 2'h0, 3'h2, 1'b0, 3'h0};				
					assert(wbtx.randomize() with {wb_addr_i == `TMS3;
									wb_data_i == wdata;
									wb_we_i == WRITE;});
					mem_cfg::wb_gentowb_bfm.put(wbtx);
	

					wbtx = new();
					wdata = {8'h00, 8'h10, 4'b0000, 1'b0, 1'b1, 1'b1, 1'b0,2'b00, 2'b10 ,3'b000, 1'b1};				
					assert(wbtx.randomize() with {wb_addr_i == `CSC4;
									wb_data_i == wdata;
									wb_we_i == WRITE;});
					mem_cfg::wb_gentowb_bfm.put(wbtx);
	
					wbtx = new();
					wdata = {4'h0, 4'h7, 4'h7, 3'h4, 2'h3, 5'h0, 1'b1 , 2'h0, 3'h2, 1'b0, 3'h0};				
					assert(wbtx.randomize() with {wb_addr_i == `TMS4;
									wb_data_i == wdata;
									wb_we_i == WRITE;});
					mem_cfg::wb_gentowb_bfm.put(wbtx);
	

					wbtx = new();
					wdata = {8'h00, 8'h20, 4'b0000, 1'b0, 1'b1, 1'b1, 1'b0,2'b00, 2'b10 ,3'b000, 1'b1};				
					assert(wbtx.randomize() with {wb_addr_i == `CSC5;
									wb_data_i == wdata;
									wb_we_i == WRITE;});
					mem_cfg::wb_gentowb_bfm.put(wbtx);
	
					wbtx = new();
					wdata = {4'h0, 4'h7, 4'h7, 3'h4, 2'h3, 5'h0, 1'b1 , 2'h0, 3'h2, 1'b0, 3'h0};				
					assert(wbtx.randomize() with {wb_addr_i == `TMS5;
									wb_data_i == wdata;
									wb_we_i == WRITE;});
					mem_cfg::wb_gentowb_bfm.put(wbtx);
	

					wbtx = new();
					wdata = {8'h00, 8'h40, 4'b0000, 1'b0, 1'b1, 1'b1, 1'b0,2'b00, 2'b10 ,3'b000, 1'b1};				
					assert(wbtx.randomize() with {wb_addr_i == `CSC6;
									wb_data_i == wdata;
									wb_we_i == WRITE;});
					mem_cfg::wb_gentowb_bfm.put(wbtx);
	
					wbtx = new();
					wdata = {4'h0, 4'h7, 4'h7, 3'h4, 2'h3, 5'h0, 1'b1 , 2'h0, 3'h2, 1'b0, 3'h0};				
					assert(wbtx.randomize() with {wb_addr_i == `TMS6;
									wb_data_i == wdata;
									wb_we_i == WRITE;});
					mem_cfg::wb_gentowb_bfm.put(wbtx);
	

					wbtx = new();
					wdata = {8'h00, 8'h80, 4'b0000, 1'b0, 1'b1, 1'b1, 1'b0,2'b00, 2'b10 ,3'b000, 1'b1};				
					assert(wbtx.randomize() with {wb_addr_i == `CSC7;
									wb_data_i == wdata;
									wb_we_i == WRITE;});
					mem_cfg::wb_gentowb_bfm.put(wbtx);


					wbtx = new();
					wdata = {4'h0, 4'h7, 4'h7, 3'h4, 2'h3, 5'h0, 1'b1 , 2'h0, 3'h2, 1'b0, 3'h0};				
					assert(wbtx.randomize() with {wb_addr_i == `TMS7;
									wb_data_i == wdata;
									wb_we_i == WRITE;});
					mem_cfg::wb_gentowb_bfm.put(wbtx);
	
					for (int i=0; i<40; i++) begin
						wbtx = new();
						assert(wbtx.randomize() with {wb_addr_i inside {[`CSC0_SA_ADDR:`CSC0_END_ADDR],[`CSC1_SA_ADDR:`CSC1_END_ADDR], [`CSC2_SA_ADDR:`CSC2_END_ADDR],
												[`CSC3_SA_ADDR:`CSC3_END_ADDR], [`CSC4_SA_ADDR:`CSC4_END_ADDR], [`CSC5_SA_ADDR:`CSC5_END_ADDR],
												[`CSC6_SA_ADDR:`CSC6_END_ADDR], [`CSC7_SA_ADDR:`CSC7_END_ADDR]};
												wb_we_i == WRITE;});
						wdata_Q1.push_back(wbtx);
						mem_cfg::wb_gentowb_bfm.put(wbtx);						
					end
					
					for (int i=0; i<40; i++) begin
						wbtx = new();
						assert(wbtx.randomize() with {wb_addr_i == wdata_Q1[i].wb_addr_i;
										wb_we_i == READ;});
						mem_cfg::wb_gentowb_bfm.put(wbtx);						
					end
		
			end
			"write_data":begin
				wbtx = new();
				assert(wbtx.randomize() with {wbtx.wb_we_i==WRITE;		//to generate read tx/	
							     wbtx.wb_addr_i==32'h0000;});	//first three MSB bits are 000 if it is memory transfer
				mem_cfg::wb_gentowb_bfm.put(wbtx);
			end
		endcase		
	
	$display("WB_GEN::RUN");
	endtask	
endclass
