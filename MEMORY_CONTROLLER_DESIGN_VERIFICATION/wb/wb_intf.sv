interface wb_intf(input logic wb_clk, input logic wb_rst);
	bit [31:0] wb_data_i; 
	bit [31:0] wb_data_o; 
	bit [31:0] wb_addr_i; 	
	bit [3:0] wb_sel_i; 	
	bit wb_we_i; 	
	bit wb_cyc_i;
	bit wb_stb_i; 	
	bit wb_ack_o; 	
	bit wb_err_o; 
	bit susp_req_i; 	
	bit resume_req_i; 	
	bit suspended_o; 	
	bit [31:0] poc_o;
	bit mc_br_i; 
	bit mc_bg_o; 
	
endinterface
