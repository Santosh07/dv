class wb_env;
	wb_gen wbgen = new();
	wb_bfm wbbfm = new();
	wb_cov wbcov = new();
	wb_mon wbmon = new();
	//wb_ref wbref = new();

	task run();
		$display("WB_ENV::RUN");
		fork
			wbgen.run();
			wbbfm.run();
			wbmon.run();
			wbcov.run();
	//		wbref.run();
		join
	endtask
endclass
