//Problems I faced
//I was generating an address that did not exist inside the mc register. Fixed it by adding implication constraint to target the required address
//I got to know about the problem by looking at the waveform and the data I was printing
//Only the first address was getting generated but I was targeting all the registers. The reason being I had @(negedge rst), since (negedge rst) comes only once, it was generating the 
//address only once. Fixed it by changing negedge to wait(!rst). It worked.
//I trying to print the dataq but for some reason, it only seems to be printing the last value it added to the queue, working on it.

// How I solved the ack problem?
//	- By changing the interface signals from logic to bit as the default value of bit is 0. Initially it was logic whose default value is x.
//	- Since the ack of the design calculates the cyc, stb at the posedge of clk and since these values hold the value of x, ack gets x. so by changing logic to bit, it gets the default
//	  value of 0 thereby solving the problem.

class wb_bfm;
	bit [31:0] data_q[$];
	wb_tx wbtx;
	virtual wb_intf wb_bfm_vif;
	bit flag1, flag2;
	
	task run();
		wb_bfm_vif = mem_cfg::wvif;
		$display("WB_BFM::RUN");
	
		forever begin
			wbtx = new();
			mem_cfg::wb_gentowb_bfm.get(wbtx);
//			wbtx.print();
			case (wbtx.wb_we_i)
			READ: begin
				read_data(wbtx);
			      end
			
			WRITE: begin
//				$display("I came to BFM Write");
				write_data(wbtx);
			end
			endcase
		end
	endtask

	task read_data(wb_tx wbtx);
//		wb_bfm_vif.wb_cyc_i = 0;		//the other way to fix the problem is to make all the three signals as bit as the default value of bit is 0. 
							//Initially it was logic whose default value is x. Changing it to bit now.
//		wb_bfm_vif.wb_stb_i = 0;
//		wb_bfm_vif.wb_sel_i = 4'h0;
//		$display("I came inside read");
		flag2 = 0;
	//	@(negedge wb_bfm_vif.wb_rst);		if we put negedge, it will do it only once as negedge only comes at the beginning i.e. the very first time
		wait (!wb_bfm_vif.wb_rst);
		@(posedge wb_bfm_vif.wb_clk);
//		#2;
		wb_bfm_vif.wb_addr_i = wbtx.wb_addr_i;
		wb_bfm_vif.wb_we_i = wbtx.wb_we_i;
		wb_bfm_vif.wb_cyc_i = 1;
		wb_bfm_vif.wb_stb_i = 1;
		wb_bfm_vif.wb_sel_i = 4'hf;
		while (flag2 == 0) begin
			@(posedge wb_bfm_vif.wb_clk);
		//	#2;
			if (wb_bfm_vif.wb_ack_o == 1) begin
				flag2 = 1;
//				$display("I entered. Flag2 became 1");
			end
		end
		@(negedge wb_bfm_vif.wb_clk);
		wb_bfm_vif.wb_cyc_i = 0;
		wb_bfm_vif.wb_stb_i = 0;
		wb_bfm_vif.wb_sel_i = 4'h0;
		data_q.push_back(wb_bfm_vif.wb_data_o);
//		$display("data_q=%p", data_q);		
//		$display("data_q = %h", wb_bfm_vif.wb_data_o);
		foreach (data_q[k]) begin
//			$display("Data_Q[%0d]=%h", k, data_q[k]);
		end
//		$display("I am on the next line of Data_q");
	endtask

	task write_data(wb_tx wbtx);
		flag1 = 0;
//		$display("I came to task write_data inside the BFM");
		wait (!wb_bfm_vif.wb_rst);
	//	$display("Reset became 1");
		wb_bfm_vif.wb_addr_i = wbtx.wb_addr_i;
		wb_bfm_vif.wb_data_i = wbtx.wb_data_i;
		wb_bfm_vif.wb_we_i = wbtx.wb_we_i;
		@(posedge wb_bfm_vif.wb_clk);

		wb_bfm_vif.wb_cyc_i = 1;	//The tx is only said to be valid when both cyc_i and stb_i are valid
		wb_bfm_vif.wb_stb_i = 1;
		wb_bfm_vif.wb_sel_i = 4'hf;
		while (flag1 == 0) begin
			@(posedge wb_bfm_vif.wb_clk);
				if (wb_bfm_vif.wb_ack_o == 1) begin		//the tx is said to be complete when ack_o is asserted by the slave
					flag1 = 1;
				end
		end
		@(negedge wb_bfm_vif.wb_clk);
		wb_bfm_vif.wb_cyc_i = 0;			//once it is asserted, both the signals are made low
		wb_bfm_vif.wb_stb_i = 0;
		wb_bfm_vif.wb_sel_i = 4'h0;
	endtask
endclass
