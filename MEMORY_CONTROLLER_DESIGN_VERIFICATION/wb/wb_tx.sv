//WB_intf is half duplex so only one data bus whereas in case of AXI we have two seperate data bus for read, write since AXI is full duplex

typedef enum{READ, WRITE} wr_rd_t;

//Definig all the regsiter address values
`define CSR 32'h60000000
`define POC 32'h60000004
`define BA_MASK 32'h60000008
`define CSC0 32'h60000010
`define TMS0 32'h60000014
`define CSC1 32'h60000018
`define TMS1 32'h6000001C
`define CSC2 32'h60000020
`define TMS2 32'h60000024
`define CSC3 32'h60000028
`define TMS3 32'h6000002C
`define CSC4 32'h60000030
`define TMS4 32'h60000034
`define CSC5 32'h60000038
`define TMS5 32'h6000003C
`define CSC6 32'h60000040
`define TMS6 32'h60000044
`define CSC7 32'h60000048
`define TMS7 32'h6000004C
//There is a particular address range for each chip select.
//If you are writing to a memory connected to chip select 0, the address range is from 0020_0000 to 003f_ffff. Similarly for other chip selects.
//How did you calculate the start and end address for each chip select device?
//	-BA_MASK[7:0]
//	-ADDR[28:21]
//	-CSC register[23:16]
// So incase of CSC0 register:00000000[00000001]
//SA_ADDR_CSC0               :0000_0000_0010_0000_0000_0000_0000_0000
//END_ADDR_CSC0		     :0000_0000_0011_1111_1111_1111_1111_1111
//SA_ADDR_CSC1			:0000_0000_0100_0000_0000_0000_0000_0000
//END_ADDR_CSC1			:0000_0000_0101_1111_1111_1111_1111_1111
//SA_ADDR_CSC2			:0000_0000_1000_0000_0000_0000_0000_0000
//END_ADDR_CSC2			:0000_0000_1001_1111_1111_1111_1111_1111
//SA_ADDR_CSC3			:0000_0001_0000_0000_0000_0000_0000_0000
//END_ADDR_CSC3			:0000_0001_0001_1111_1111_1111_1111_1111	
//SA_ADDR_CSC4			:0000_0010_0000_0000_0000_0000_0000_0000
//END_ADDR_CSC4			:0000_0010_0001_1111_1111_1111_1111_1111
//SA_ADDR_CSC5			:0000_0100_0000_0000_0000_0000_0000_0000
//END_ADDR_CSC5			:0000_0100_0001_1111_1111_1111_1111_1111
//SA_ADDR_CSC6			:0000_1000_0000_0000_0000_0000_0000_0000
//END_ADDR_CSC6			:0000_1000_0001_1111_1111_1111_1111_1111
//SA_ADDR_CSC7			:0001_0000_0000_0000_0000_0000_0000_0000
//END_ADDR_CSC7			:0001_0000_0001_1111_1111_1111_1111_1111	
`define CSC0_SA_ADDR 32'h0020_0000
`define CSC0_END_ADDR 32'h003f_ffff
`define CSC1_SA_ADDR 32'h0040_0000
`define CSC1_END_ADDR 32'h005f_ffff
`define CSC2_SA_ADDR 32'h0080_0000
`define CSC2_END_ADDR 32'h009f_ffff
`define CSC3_SA_ADDR 32'h0100_0000
`define CSC3_END_ADDR 32'h011f_ffff
`define CSC4_SA_ADDR 32'h0200_0000
`define CSC4_END_ADDR 32'h021f_ffff
`define CSC5_SA_ADDR 32'h0400_0000
`define CSC5_END_ADDR 32'h041f_ffff
`define CSC6_SA_ADDR 32'h0800_0000
`define CSC6_END_ADDR 32'h081f_ffff
`define CSC7_SA_ADDR 32'h1000_0000
`define CSC7_END_ADDR 32'h101f_ffff

//Masking of bits: to make sure we dont write to Read Only bits.
class wb_tx;
	rand bit [31:0] wb_addr_i; 	
	rand bit [31:0] wb_data_i; 	//only one data line if wb_we_i = 1, it acts as write bus, if wb_we_i = 0, it acts as read bus
	rand wr_rd_t wb_we_i; 		//1-write, 0-read
	rand bit reg_mem_f; 

//	bit [31:0] wb_data_o; 		//this will be driven by the DUT and not by the TB so not declared as rand
//	rand bit [3:0] wb_sel_i; 	//this signal will be driven from the BFM directly as 4'hf i.e. all four bytes are valid
//	bit wb_cyc_i;			// all three are handshaking signals that will be driven from the BFM so not declared as rand
//	bit wb_stb_i; 	
//	bit wb_ack_o; 	
//	rand bit wb_err_o; 
//	rand bit susp_req_i; 	
//	rand bit resume_req_i; 	
//	rand bit suspended_o; 	
//	rand bit [31:0] poc_o;
//	rand bit mc_br_i, 
//	rand bit mc_bg_o,
//	bit [31:0] data_q[$];

	
	virtual function void print();
		$display("WB_addr_i = %h", wb_addr_i);
		$display("WB_Data_i = %h", wb_data_i);
		$display("WB_we_i = %s", wb_we_i);
	endfunction

	constraint reg_mem_c{
		(reg_mem_f == 1) -> (wb_addr_i[31:29] == 3);		//register address	//These are known as implication constraints. Denoted by ->
		(reg_mem_f == 0) -> (wb_addr_i[31:29] == 0);		//memory address
	} 
endclass
