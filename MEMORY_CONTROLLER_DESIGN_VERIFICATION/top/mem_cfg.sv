class mem_cfg;
	static virtual wb_intf wvif;
	static virtual mem_intf mvif;
	static mailbox wb_gentowb_bfm = new();
	static mailbox montocov = new();
	static mailbox montoref = new();
	static mailbox reftockr = new();
	static mailbox mc_montockr = new();
	static mailbox mc_reftomc_ckr_mc_reg = new(); 
	static string testname;
		
	static bit [31:0] reg_mask[19] = {{8'hff, 13'h0, 3'h7, 5'h0, 1'h1, 1'h1, 1'h0},
					  {32'h0},
					  {16'h0, 8'hff},
					  {8'h0, 8'hff, 4'h0, 12'hfff},
					  {32'hffffffff},
					  {8'h0, 8'hff, 4'h0, 12'hfff},
					  {32'hffffffff},
					  {8'h0, 8'hff, 4'h0, 12'hfff},
					  {32'hffffffff},
					  {8'h0, 8'hff, 4'h0, 12'hfff},
					  {32'hffffffff},
					  {8'h0, 8'hff, 4'h0, 12'hfff},
					  {32'hffffffff},
					  {8'h0, 8'hff, 4'h0, 12'hfff},
					  {32'hffffffff},
					  {8'h0, 8'hff, 4'h0, 12'hfff},
					  {32'hffffffff},
					  {8'h0, 8'hff, 4'h0, 12'hfff},
					  {32'hffffffff}};
endclass
