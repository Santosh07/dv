//Dq is the data pin from the memory. It has a common bus for both read and write.
//Whereas Memory Controller has two seperate buses for read and write.
//So in order to tackle this situation, we will declare an inout signal that will act as an input when read transaction is on and output when write transaction is on.
/*
wire [31:0] mc_dq;
assign mc_dq = (!mvif.oe)?mvif.mc_data_o:32'hz;			
assign mvif.mc_data_i = (mvif.oe)?mc_dq:32'hz;*/

//Dq(mc_dq); 
module top();
	reg wb_clk, wb_rst;
	reg mem_clk;

	wire [31:0] mc_dq;					//Internal signal declared as wire since it is acting as an inout signal and you cannot declare logic as inout as logic 
								//cannot handle
							//bi-directional signals. SRAM memory has only one signal for data transfer whereas memory controller has two signals i.e. one for 
							//write and other for read. 

	assign mc_dq = (mif.mc_doe_doe_o)?mif.mc_data_o:32'hz;
	assign mif.mc_data_i = (!mif.mc_doe_doe_o)?mc_dq:32'hz;

	//clk instantiation
	initial begin
		wb_clk = 0;
		forever begin
			//wb_clk = #5 ~wb_clk;			//100 MHz
			#5 wb_clk = ~wb_clk;
		end
	end

	initial begin
		mem_clk = 0;
		forever begin
			//mem_clk = #10 ~mem_clk;			//50 MHz - clk to memory is 50% slower as compared to clk to memory controller
			#10 mem_clk = ~mem_clk;
		end
	end

	//rst instantiation
	initial begin
		wb_rst = 1;		//Active high reset
		repeat (2) @(posedge wb_clk);
		wb_rst = 0;
	end

	//interface instantiation
	wb_intf wif(wb_clk, wb_rst);
	mem_intf mif(mem_clk);
	
	//Connecting DUT with the WB interface and mem_controller interface
	mc_top wb_mem_dut(
			.clk_i(wif.wb_clk), 
			.rst_i(wif.wb_rst),
			.wb_data_i(wif.wb_data_i), 
			.wb_data_o(wif.wb_data_o), 
			.wb_addr_i(wif.wb_addr_i), 
			.wb_sel_i(wif.wb_sel_i), 
			.wb_we_i(wif.wb_we_i), 
			.wb_cyc_i(wif.wb_cyc_i),
			.wb_stb_i(wif.wb_stb_i), 
			.wb_ack_o(wif.wb_ack_o), 
			.wb_err_o(wif.wb_err_o), 
			.susp_req_i(wif.susp_req_i), 
			.resume_req_i(wif.resume_req_i), 
			.suspended_o(wif.suspended_o), 
			.poc_o(wif.poc_o),
			.mc_br_pad_i(wif.mc_br_i), 
			.mc_bg_pad_o(wif.mc_bg_o), 	
			.mc_clk_i(mif.mem_clk), 
			.mc_ack_pad_i(mif.mc_ack_i),
			.mc_addr_pad_o(mif.mc_addr_o), 
			.mc_data_pad_i(mif.mc_data_i), 
			.mc_data_pad_o(mif.mc_data_o), 
			.mc_dp_pad_i(mif.mc_dp_i),
			.mc_dp_pad_o(mif.mc_dp_o), 
			.mc_doe_pad_doe_o(mif.mc_doe_doe_o), 
			.mc_dqm_pad_o(mif.mc_dqm_o), 
			.mc_oe_pad_o_(mif.mc_oe_o),
			.mc_we_pad_o_(mif.mc_we_o), 
			.mc_cas_pad_o_(mif.mc_cas_o), 
			.mc_ras_pad_o_(mif.mc_ras_o), 
			.mc_cke_pad_o_(mif.mc_cke_o),
			.mc_cs_pad_o_(mif.mc_cs_o), 
			.mc_sts_pad_i(mif.mc_sts_i), 
			.mc_rp_pad_o_(mif.mc_rp_o), 
			.mc_vpen_pad_o(mif.mc_vpen_o),
			.mc_adsc_pad_o_(mif.mc_adsc_o), 
			.mc_adv_pad_o_(mif.mc_adv_o), 
			.mc_zz_pad_o(mif.mc_zz_o), 
			.mc_coe_pad_coe_o(mif.mc_coe_coe_o)
	);

	assertion asser(.clk(wif.wb_clk),
			.rst(wif.wb_rst),
			.cyc(wif.wb_cyc_i),
			.stb(wif.wb_stb_i),
			.ack(wif.wb_ack_o),
			.data_i(wif.wb_data_i),
			.data_o(wif.wb_data_o),
			.wr_rd(wif.wb_we_i)
			);


	//Two models of SRAM are connected becaause each SRAM is 16 bits wide (data bus) whereas mc is 32 bits wide. Both get the clock from the mc at the same time 
	//Slave model i.e. SRAM, SDRAM, SYnchronous chip select devices, asynchronous chip select devices instantiation
`ifdef SRAM			//These are known as compiler directives. They get compiled only when you pass them while compiling at run time through QuestaSim tool
				//When we will be testing for chip select 0 with SDRAM connected to it, we wont be compiling the SSRAM.
	mt58l1my18d sram_mem_a0 (
				.Dq(mc_dq[15:0]), 
				.Addr(mif.mc_addr_o), 
				.Mode(1'b0), 
				.Adv_n(mif.mc_adv_o), 
				.Clk(mif.mem_clk), 
				.Adsc_n(mif.mc_adsc_o), 
				.Adsp_n(1'b1), 				//basically connected to VCC so directly given a value of 1
				.Bwa_n(mif.mc_dqm_o[0]), 
				.Bwb_n(mif.mc_dqm_o[1]), 
				.Bwe_n(mif.mc_we_o), 
				.Gw_n(1'b1), 
				.Ce_n(mif.mc_cs_o[0]), 
				.Ce2(1'b1), 
				.Ce2_n(1'b0), 
				.Oe_n(1'b0), 
				.Zz(mif.mc_zz_o)
				);

	mt58l1my18d sram_mem_a1 (
				.Dq(mc_dq[31:16]), 
				.Addr(mif.mc_addr_o), 
				.Mode(1'b0), 			//1-burst, 0-single
				.Adv_n(mif.mc_adv_o), 
				.Clk(mif.mem_clk), 
				.Adsc_n(mif.mc_adsc_o), 
				.Adsp_n(1'b1), 				//basically connected to VCC so directly given a value of 1
				.Bwa_n(mif.mc_dqm_o[2]), 
				.Bwb_n(mif.mc_dqm_o[3]), 
				.Bwe_n(mif.mc_we_o), 
				.Gw_n(1'b1), 
				.Ce_n(mif.mc_cs_o[0]), 
				.Ce2(1'b1), 
				.Ce2_n(1'b0), 
				.Oe_n(1'b0), 
				.Zz(mif.mc_zz_o)
				);

	mt58l1my18d sram_mem_b0 (
				.Dq(mc_dq[15:0]), 
				.Addr(mif.mc_addr_o), 
				.Mode(1'b0), 
				.Adv_n(mif.mc_adv_o), 
				.Clk(mif.mem_clk), 
				.Adsc_n(mif.mc_adsc_o), 
				.Adsp_n(1'b1), 				//basically connected to VCC so directly given a value of 1
				.Bwa_n(mif.mc_dqm_o[0]), 
				.Bwb_n(mif.mc_dqm_o[1]), 
				.Bwe_n(mif.mc_we_o), 
				.Gw_n(1'b1), 
				.Ce_n(mif.mc_cs_o[1]), 
				.Ce2(1'b1), 
				.Ce2_n(1'b0), 
				.Oe_n(1'b0), 
				.Zz(mif.mc_zz_o)
				);

	mt58l1my18d sram_mem_b1 (
				.Dq(mc_dq[31:16]), 
				.Addr(mif.mc_addr_o), 
				.Mode(1'b0), 			//1-burst, 0-single
				.Adv_n(mif.mc_adv_o), 
				.Clk(mif.mem_clk), 
				.Adsc_n(mif.mc_adsc_o), 
				.Adsp_n(1'b1), 				//basically connected to VCC so directly given a value of 1
				.Bwa_n(mif.mc_dqm_o[2]), 
				.Bwb_n(mif.mc_dqm_o[3]), 
				.Bwe_n(mif.mc_we_o), 
				.Gw_n(1'b1), 
				.Ce_n(mif.mc_cs_o[1]), 
				.Ce2(1'b1), 
				.Ce2_n(1'b0), 
				.Oe_n(1'b0), 
				.Zz(mif.mc_zz_o)
				);
	
	mt58l1my18d sram_mem_c0 (
				.Dq(mc_dq[15:0]), 
				.Addr(mif.mc_addr_o), 
				.Mode(1'b0), 
				.Adv_n(mif.mc_adv_o), 
				.Clk(mif.mem_clk), 
				.Adsc_n(mif.mc_adsc_o), 
				.Adsp_n(1'b1), 				//basically connected to VCC so directly given a value of 1
				.Bwa_n(mif.mc_dqm_o[0]), 
				.Bwb_n(mif.mc_dqm_o[1]), 
				.Bwe_n(mif.mc_we_o), 
				.Gw_n(1'b1), 
				.Ce_n(mif.mc_cs_o[2]), 
				.Ce2(1'b1), 
				.Ce2_n(1'b0), 
				.Oe_n(1'b0), 
				.Zz(mif.mc_zz_o)
				);

	mt58l1my18d sram_mem_c1 (
				.Dq(mc_dq[31:16]), 
				.Addr(mif.mc_addr_o), 
				.Mode(1'b0), 			//1-burst, 0-single
				.Adv_n(mif.mc_adv_o), 
				.Clk(mif.mem_clk), 
				.Adsc_n(mif.mc_adsc_o), 
				.Adsp_n(1'b1), 				//basically connected to VCC so directly given a value of 1
				.Bwa_n(mif.mc_dqm_o[2]), 
				.Bwb_n(mif.mc_dqm_o[3]), 
				.Bwe_n(mif.mc_we_o), 
				.Gw_n(1'b1), 
				.Ce_n(mif.mc_cs_o[2]), 
				.Ce2(1'b1), 
				.Ce2_n(1'b0), 
				.Oe_n(1'b0), 
				.Zz(mif.mc_zz_o)
				);

	mt58l1my18d sram_mem_d0 (
				.Dq(mc_dq[15:0]), 
				.Addr(mif.mc_addr_o), 
				.Mode(1'b0), 
				.Adv_n(mif.mc_adv_o), 
				.Clk(mif.mem_clk), 
				.Adsc_n(mif.mc_adsc_o), 
				.Adsp_n(1'b1), 				//basically connected to VCC so directly given a value of 1
				.Bwa_n(mif.mc_dqm_o[0]), 
				.Bwb_n(mif.mc_dqm_o[1]), 
				.Bwe_n(mif.mc_we_o), 
				.Gw_n(1'b1), 
				.Ce_n(mif.mc_cs_o[3]), 
				.Ce2(1'b1), 
				.Ce2_n(1'b0), 
				.Oe_n(1'b0), 
				.Zz(mif.mc_zz_o)
				);

	mt58l1my18d sram_mem_d1 (
				.Dq(mc_dq[31:16]), 
				.Addr(mif.mc_addr_o), 
				.Mode(1'b0), 			//1-burst, 0-single
				.Adv_n(mif.mc_adv_o), 
				.Clk(mif.mem_clk), 
				.Adsc_n(mif.mc_adsc_o), 
				.Adsp_n(1'b1), 				//basically connected to VCC so directly given a value of 1
				.Bwa_n(mif.mc_dqm_o[2]), 
				.Bwb_n(mif.mc_dqm_o[3]), 
				.Bwe_n(mif.mc_we_o), 
				.Gw_n(1'b1), 
				.Ce_n(mif.mc_cs_o[3]), 
				.Ce2(1'b1), 
				.Ce2_n(1'b0), 
				.Oe_n(1'b0), 
				.Zz(mif.mc_zz_o)
				);

	mt58l1my18d sram_mem_e0 (
				.Dq(mc_dq[15:0]), 
				.Addr(mif.mc_addr_o), 
				.Mode(1'b0), 
				.Adv_n(mif.mc_adv_o), 
				.Clk(mif.mem_clk), 
				.Adsc_n(mif.mc_adsc_o), 
				.Adsp_n(1'b1), 				//basically connected to VCC so directly given a value of 1
				.Bwa_n(mif.mc_dqm_o[0]), 
				.Bwb_n(mif.mc_dqm_o[1]), 
				.Bwe_n(mif.mc_we_o), 
				.Gw_n(1'b1), 
				.Ce_n(mif.mc_cs_o[4]), 
				.Ce2(1'b1), 
				.Ce2_n(1'b0), 
				.Oe_n(1'b0), 
				.Zz(mif.mc_zz_o)
				);

	mt58l1my18d sram_mem_e1 (
				.Dq(mc_dq[31:16]), 
				.Addr(mif.mc_addr_o), 
				.Mode(1'b0), 			//1-burst, 0-single
				.Adv_n(mif.mc_adv_o), 
				.Clk(mif.mem_clk), 
				.Adsc_n(mif.mc_adsc_o), 
				.Adsp_n(1'b1), 				//basically connected to VCC so directly given a value of 1
				.Bwa_n(mif.mc_dqm_o[2]), 
				.Bwb_n(mif.mc_dqm_o[3]), 
				.Bwe_n(mif.mc_we_o), 
				.Gw_n(1'b1), 
				.Ce_n(mif.mc_cs_o[4]), 
				.Ce2(1'b1), 
				.Ce2_n(1'b0), 
				.Oe_n(1'b0), 
				.Zz(mif.mc_zz_o)
				);

	mt58l1my18d sram_mem_f0 (
				.Dq(mc_dq[15:0]), 
				.Addr(mif.mc_addr_o), 
				.Mode(1'b0), 
				.Adv_n(mif.mc_adv_o), 
				.Clk(mif.mem_clk), 
				.Adsc_n(mif.mc_adsc_o), 
				.Adsp_n(1'b1), 				//basically connected to VCC so directly given a value of 1
				.Bwa_n(mif.mc_dqm_o[0]), 
				.Bwb_n(mif.mc_dqm_o[1]), 
				.Bwe_n(mif.mc_we_o), 
				.Gw_n(1'b1), 
				.Ce_n(mif.mc_cs_o[5]), 
				.Ce2(1'b1), 
				.Ce2_n(1'b0), 
				.Oe_n(1'b0), 
				.Zz(mif.mc_zz_o)
				);

	mt58l1my18d sram_mem_f1 (
				.Dq(mc_dq[31:16]), 
				.Addr(mif.mc_addr_o), 
				.Mode(1'b0), 			//1-burst, 0-single
				.Adv_n(mif.mc_adv_o), 
				.Clk(mif.mem_clk), 
				.Adsc_n(mif.mc_adsc_o), 
				.Adsp_n(1'b1), 				//basically connected to VCC so directly given a value of 1
				.Bwa_n(mif.mc_dqm_o[2]), 
				.Bwb_n(mif.mc_dqm_o[3]), 
				.Bwe_n(mif.mc_we_o), 
				.Gw_n(1'b1), 
				.Ce_n(mif.mc_cs_o[5]), 
				.Ce2(1'b1), 
				.Ce2_n(1'b0), 
				.Oe_n(1'b0), 
				.Zz(mif.mc_zz_o)
				);

	mt58l1my18d sram_mem_g0 (
				.Dq(mc_dq[15:0]), 
				.Addr(mif.mc_addr_o), 
				.Mode(1'b0), 
				.Adv_n(mif.mc_adv_o), 
				.Clk(mif.mem_clk), 
				.Adsc_n(mif.mc_adsc_o), 
				.Adsp_n(1'b1), 				//basically connected to VCC so directly given a value of 1
				.Bwa_n(mif.mc_dqm_o[0]), 
				.Bwb_n(mif.mc_dqm_o[1]), 
				.Bwe_n(mif.mc_we_o), 
				.Gw_n(1'b1), 
				.Ce_n(mif.mc_cs_o[6]), 
				.Ce2(1'b1), 
				.Ce2_n(1'b0), 
				.Oe_n(1'b0), 
				.Zz(mif.mc_zz_o)
				);

	mt58l1my18d sram_mem_g1 (
				.Dq(mc_dq[31:16]), 
				.Addr(mif.mc_addr_o), 
				.Mode(1'b0), 			//1-burst, 0-single
				.Adv_n(mif.mc_adv_o), 
				.Clk(mif.mem_clk), 
				.Adsc_n(mif.mc_adsc_o), 
				.Adsp_n(1'b1), 				//basically connected to VCC so directly given a value of 1
				.Bwa_n(mif.mc_dqm_o[2]), 
				.Bwb_n(mif.mc_dqm_o[3]), 
				.Bwe_n(mif.mc_we_o), 
				.Gw_n(1'b1), 
				.Ce_n(mif.mc_cs_o[6]), 
				.Ce2(1'b1), 
				.Ce2_n(1'b0), 
				.Oe_n(1'b0), 
				.Zz(mif.mc_zz_o)
				);

	mt58l1my18d sram_mem_h0 (
				.Dq(mc_dq[15:0]), 
				.Addr(mif.mc_addr_o), 
				.Mode(1'b0), 
				.Adv_n(mif.mc_adv_o), 
				.Clk(mif.mem_clk), 
				.Adsc_n(mif.mc_adsc_o), 
				.Adsp_n(1'b1), 				//basically connected to VCC so directly given a value of 1
				.Bwa_n(mif.mc_dqm_o[0]), 
				.Bwb_n(mif.mc_dqm_o[1]), 
				.Bwe_n(mif.mc_we_o), 
				.Gw_n(1'b1), 
				.Ce_n(mif.mc_cs_o[7]), 
				.Ce2(1'b1), 
				.Ce2_n(1'b0), 
				.Oe_n(1'b0), 
				.Zz(mif.mc_zz_o)
				);

	mt58l1my18d sram_mem_h1 (
				.Dq(mc_dq[31:16]), 
				.Addr(mif.mc_addr_o), 
				.Mode(1'b0), 			//1-burst, 0-single
				.Adv_n(mif.mc_adv_o), 
				.Clk(mif.mem_clk), 
				.Adsc_n(mif.mc_adsc_o), 
				.Adsp_n(1'b1), 				//basically connected to VCC so directly given a value of 1
				.Bwa_n(mif.mc_dqm_o[2]), 
				.Bwb_n(mif.mc_dqm_o[3]), 
				.Bwe_n(mif.mc_we_o), 
				.Gw_n(1'b1), 
				.Ce_n(mif.mc_cs_o[7]), 
				.Ce2(1'b1), 
				.Ce2_n(1'b0), 
				.Oe_n(1'b0), 
				.Zz(mif.mc_zz_o)
				);		
`elsif SDRAM
		mt48lc2m32b2 sdram_cs0 (
					.Dq(mc_dq), 
					.Addr(mif.mc_addr_o[10:0]), 
					.Ba(mif.mc_addr_o[14:13]), 
					.Clk(mif.mem_clk), 
					.Cke(mif.mc_cke_o), 
					.Cs_n(mif.mc_cs_o[0]), 
					.Ras_n(mif.mc_ras_o), 
					.Cas_n(mif.mc_cas_o), 
					.We_n(mif.mc_we_o), 
					.Dqm(mif.mc_dqm_o)
					);

		mt48lc2m32b2 sdram_cs1 (
					.Dq(mc_dq), 
					.Addr(mif.mc_addr_o[10:0]), 
					.Ba(mif.mc_addr_o[14:13]), 
					.Clk(mif.mem_clk), 
					.Cke(mif.mc_cke_o), 
					.Cs_n(mif.mc_cs_o[1]), 
					.Ras_n(mif.mc_ras_o), 
					.Cas_n(mif.mc_cas_o), 
					.We_n(mif.mc_we_o), 
					.Dqm(mif.mc_dqm_o)
					);
			
			mt48lc2m32b2 sdram_cs2 (
					.Dq(mc_dq), 
					.Addr(mif.mc_addr_o[10:0]), 
					.Ba(mif.mc_addr_o[14:13]), 
					.Clk(mif.mem_clk), 
					.Cke(mif.mc_cke_o), 
					.Cs_n(mif.mc_cs_o[2]), 
					.Ras_n(mif.mc_ras_o), 
					.Cas_n(mif.mc_cas_o), 
					.We_n(mif.mc_we_o), 
					.Dqm(mif.mc_dqm_o)
					);
	
		mt48lc2m32b2 sdram_cs3 (
					.Dq(mc_dq), 
					.Addr(mif.mc_addr_o[10:0]), 
					.Ba(mif.mc_addr_o[14:13]), 
					.Clk(mif.mem_clk), 
					.Cke(mif.mc_cke_o), 
					.Cs_n(mif.mc_cs_o[3]), 
					.Ras_n(mif.mc_ras_o), 
					.Cas_n(mif.mc_cas_o), 
					.We_n(mif.mc_we_o), 
					.Dqm(mif.mc_dqm_o)
					);
	
		mt48lc2m32b2 sdram_cs4 (
					.Dq(mc_dq), 
					.Addr(mif.mc_addr_o[10:0]), 
					.Ba(mif.mc_addr_o[14:13]), 
					.Clk(mif.mem_clk), 
					.Cke(mif.mc_cke_o), 
					.Cs_n(mif.mc_cs_o[4]), 
					.Ras_n(mif.mc_ras_o), 
					.Cas_n(mif.mc_cas_o), 
					.We_n(mif.mc_we_o), 
					.Dqm(mif.mc_dqm_o)
					);
	
		mt48lc2m32b2 sdram_cs5 (
					.Dq(mc_dq), 
					.Addr(mif.mc_addr_o[10:0]), 
					.Ba(mif.mc_addr_o[14:13]), 
					.Clk(mif.mem_clk), 
					.Cke(mif.mc_cke_o), 
					.Cs_n(mif.mc_cs_o[5]), 
					.Ras_n(mif.mc_ras_o), 
					.Cas_n(mif.mc_cas_o), 
					.We_n(mif.mc_we_o), 
					.Dqm(mif.mc_dqm_o)
					);
	
		mt48lc2m32b2 sdram_cs6 (
					.Dq(mc_dq), 
					.Addr(mif.mc_addr_o[10:0]), 
					.Ba(mif.mc_addr_o[14:13]), 
					.Clk(mif.mem_clk), 
					.Cke(mif.mc_cke_o), 
					.Cs_n(mif.mc_cs_o[6]), 
					.Ras_n(mif.mc_ras_o), 
					.Cas_n(mif.mc_cas_o), 
					.We_n(mif.mc_we_o), 
					.Dqm(mif.mc_dqm_o)
					);
	

		mt48lc2m32b2 sdram_cs7 (
					.Dq(mc_dq), 
					.Addr(mif.mc_addr_o[10:0]), 
					.Ba(mif.mc_addr_o[14:13]), 
					.Clk(mif.mem_clk), 
					.Cke(mif.mc_cke_o), 
					.Cs_n(mif.mc_cs_o[7]), 
					.Ras_n(mif.mc_ras_o), 
					.Cas_n(mif.mc_cas_o), 
					.We_n(mif.mc_we_o), 
					.Dqm(mif.mc_dqm_o)
					);
	
`elsif SYNC_CS
	sync_cs_dev cs0_SYNCS(
				.clk(mif.mem_clk), 
				.addr(mif.mc_addr_o[15:0]), 
				.dq(mc_dq), 
				.cs_(mif.mc_cs_o[0]), 
				.we_(mif.mc_we_o), 
				.oe_(mif.mc_oe_o), 
				.ack_(mif.mc_ack_i) 
				);
				
`elsif FLASH_CS
	IntelAdvBoot Flash_ASYNC(
				.dq(mc_dq[15:0]), 
				.addr(mif.mc_addr_o[19:0]), 
				.ceb(mif.mc_cs_o[0]), 
				.oeb(mif.mc_oe_o), 
				.web(mif.mc_we_o), 
				.rpb(mif.mc_rp_o), 
				.wpb(1'b1), 
				.vpp(3300), 
				.vcc(3300)
				);
	
`endif
	//TB instantiation
	mem_tb mtb();

	//Passing the physical interface handle to virtual interface in the config class
	initial begin
		mem_cfg::wvif = wif;			
		mem_cfg::mvif = mif;
	end
	//Test selection mechanism
	initial begin
		$value$plusargs("testname=%s", mem_cfg::testname);	
	end

	//logic to end simulation
	initial begin
		#9000;
		$finish;
	end
endmodule
