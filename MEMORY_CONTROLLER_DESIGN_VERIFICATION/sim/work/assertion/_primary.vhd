library verilog;
use verilog.vl_types.all;
entity assertion is
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        cyc             : in     vl_logic;
        stb             : in     vl_logic;
        ack             : in     vl_logic;
        data_i          : in     vl_logic_vector(31 downto 0);
        data_o          : in     vl_logic_vector(31 downto 0);
        wr_rd           : in     vl_logic
    );
end assertion;
