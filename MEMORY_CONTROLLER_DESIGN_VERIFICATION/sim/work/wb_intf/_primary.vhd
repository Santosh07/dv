library verilog;
use verilog.vl_types.all;
entity wb_intf is
    port(
        wb_clk          : in     vl_logic;
        wb_rst          : in     vl_logic
    );
end wb_intf;
