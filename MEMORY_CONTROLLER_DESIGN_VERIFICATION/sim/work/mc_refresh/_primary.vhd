library verilog;
use verilog.vl_types.all;
entity mc_refresh is
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        cs_need_rfr     : in     vl_logic_vector(7 downto 0);
        ref_int         : in     vl_logic_vector(2 downto 0);
        rfr_req         : out    vl_logic;
        rfr_ack         : in     vl_logic;
        rfr_ps_val      : in     vl_logic_vector(7 downto 0)
    );
end mc_refresh;
