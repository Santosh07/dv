library verilog;
use verilog.vl_types.all;
entity mc_obct_dummy is
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        row_adr         : in     vl_logic_vector(12 downto 0);
        bank_adr        : in     vl_logic_vector(1 downto 0);
        bank_set        : in     vl_logic;
        bank_clr        : in     vl_logic;
        bank_clr_all    : in     vl_logic;
        bank_open       : out    vl_logic;
        any_bank_open   : out    vl_logic;
        row_same        : out    vl_logic
    );
end mc_obct_dummy;
