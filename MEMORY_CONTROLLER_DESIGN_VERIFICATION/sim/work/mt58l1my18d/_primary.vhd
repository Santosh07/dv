library verilog;
use verilog.vl_types.all;
entity mt58l1my18d is
    generic(
        addr_bits       : integer := 20;
        data_bits       : integer := 18;
        mem_sizes       : integer := 1048575;
        reg_delay       : real    := 0.100000;
        out_delay       : real    := 0.100000;
        tKQHZ           : real    := 3.500000
    );
    port(
        Dq              : inout  vl_logic_vector;
        Addr            : in     vl_logic_vector;
        Mode            : in     vl_logic;
        Adv_n           : in     vl_logic;
        Clk             : in     vl_logic;
        Adsc_n          : in     vl_logic;
        Adsp_n          : in     vl_logic;
        Bwa_n           : in     vl_logic;
        Bwb_n           : in     vl_logic;
        Bwe_n           : in     vl_logic;
        Gw_n            : in     vl_logic;
        Ce_n            : in     vl_logic;
        Ce2             : in     vl_logic;
        Ce2_n           : in     vl_logic;
        Oe_n            : in     vl_logic;
        Zz              : in     vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of addr_bits : constant is 1;
    attribute mti_svvh_generic_type of data_bits : constant is 1;
    attribute mti_svvh_generic_type of mem_sizes : constant is 1;
    attribute mti_svvh_generic_type of reg_delay : constant is 1;
    attribute mti_svvh_generic_type of out_delay : constant is 1;
    attribute mti_svvh_generic_type of tKQHZ : constant is 1;
end mt58l1my18d;
