library verilog;
use verilog.vl_types.all;
entity mc_cs_rf is
    generic(
        this_cs         : vl_logic_vector(2 downto 0) := (Hi0, Hi0, Hi0);
        reg_select      : vl_logic_vector(3 downto 0)
    );
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        wb_we_i         : in     vl_logic;
        din             : in     vl_logic_vector(31 downto 0);
        rf_we           : in     vl_logic;
        addr            : in     vl_logic_vector(31 downto 0);
        csc             : out    vl_logic_vector(31 downto 0);
        tms             : out    vl_logic_vector(31 downto 0);
        poc             : in     vl_logic_vector(31 downto 0);
        csc_mask        : in     vl_logic_vector(31 downto 0);
        cs              : out    vl_logic;
        wp_err          : out    vl_logic;
        lmr_req         : out    vl_logic;
        lmr_ack         : in     vl_logic;
        init_req        : out    vl_logic;
        init_ack        : in     vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of this_cs : constant is 2;
    attribute mti_svvh_generic_type of reg_select : constant is 4;
end mc_cs_rf;
