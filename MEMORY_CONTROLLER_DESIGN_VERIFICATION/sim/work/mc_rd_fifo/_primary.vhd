library verilog;
use verilog.vl_types.all;
entity mc_rd_fifo is
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        clr             : in     vl_logic;
        din             : in     vl_logic_vector(35 downto 0);
        we              : in     vl_logic;
        dout            : out    vl_logic_vector(35 downto 0);
        re              : in     vl_logic
    );
end mc_rd_fifo;
