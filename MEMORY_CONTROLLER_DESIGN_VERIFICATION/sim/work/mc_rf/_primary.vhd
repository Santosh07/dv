library verilog;
use verilog.vl_types.all;
entity mc_rf is
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        wb_data_i       : in     vl_logic_vector(31 downto 0);
        rf_dout         : out    vl_logic_vector(31 downto 0);
        wb_addr_i       : in     vl_logic_vector(31 downto 0);
        wb_we_i         : in     vl_logic;
        wb_cyc_i        : in     vl_logic;
        wb_stb_i        : in     vl_logic;
        wb_ack_o        : out    vl_logic;
        wp_err          : out    vl_logic;
        csc             : out    vl_logic_vector(31 downto 0);
        tms             : out    vl_logic_vector(31 downto 0);
        poc             : out    vl_logic_vector(31 downto 0);
        sp_csc          : out    vl_logic_vector(31 downto 0);
        sp_tms          : out    vl_logic_vector(31 downto 0);
        cs              : out    vl_logic_vector(7 downto 0);
        mc_data_i       : in     vl_logic_vector(31 downto 0);
        mc_sts          : in     vl_logic;
        mc_vpen         : out    vl_logic;
        fs              : out    vl_logic;
        cs_le_d         : in     vl_logic;
        cs_le           : in     vl_logic;
        cs_need_rfr     : out    vl_logic_vector(7 downto 0);
        ref_int         : out    vl_logic_vector(2 downto 0);
        rfr_ps_val      : out    vl_logic_vector(7 downto 0);
        init_req        : out    vl_logic;
        init_ack        : in     vl_logic;
        lmr_req         : out    vl_logic;
        lmr_ack         : in     vl_logic;
        spec_req_cs     : out    vl_logic_vector(7 downto 0)
    );
end mc_rf;
