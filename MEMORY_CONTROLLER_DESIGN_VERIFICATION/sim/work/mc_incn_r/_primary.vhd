library verilog;
use verilog.vl_types.all;
entity mc_incn_r is
    generic(
        incN_width      : integer := 32;
        incN_center     : vl_notype
    );
    port(
        clk             : in     vl_logic;
        inc_in          : in     vl_logic_vector;
        inc_out         : out    vl_logic_vector
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of incN_width : constant is 1;
    attribute mti_svvh_generic_type of incN_center : constant is 3;
end mc_incn_r;
