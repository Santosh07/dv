library verilog;
use verilog.vl_types.all;
entity sync_cs_dev is
    port(
        clk             : in     vl_logic;
        addr            : in     vl_logic_vector(15 downto 0);
        dq              : inout  vl_logic_vector(31 downto 0);
        \cs_\           : in     vl_logic;
        \we_\           : in     vl_logic;
        \oe_\           : in     vl_logic;
        \ack_\          : out    vl_logic
    );
end sync_cs_dev;
