library verilog;
use verilog.vl_types.all;
entity mc_dp is
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        csc             : in     vl_logic_vector(31 downto 0);
        wb_cyc_i        : in     vl_logic;
        wb_stb_i        : in     vl_logic;
        wb_ack_o        : in     vl_logic;
        mem_ack         : in     vl_logic;
        wb_data_i       : in     vl_logic_vector(31 downto 0);
        wb_data_o       : out    vl_logic_vector(31 downto 0);
        wb_read_go      : in     vl_logic;
        wb_we_i         : in     vl_logic;
        mc_clk          : in     vl_logic;
        mc_data_del     : in     vl_logic_vector(35 downto 0);
        mc_dp_i         : in     vl_logic_vector(3 downto 0);
        mc_data_o       : out    vl_logic_vector(31 downto 0);
        mc_dp_o         : out    vl_logic_vector(3 downto 0);
        dv              : in     vl_logic;
        pack_le0        : in     vl_logic;
        pack_le1        : in     vl_logic;
        pack_le2        : in     vl_logic;
        byte_en         : in     vl_logic_vector(3 downto 0);
        par_err         : out    vl_logic
    );
end mc_dp;
