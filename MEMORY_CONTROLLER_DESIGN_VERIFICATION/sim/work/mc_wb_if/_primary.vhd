library verilog;
use verilog.vl_types.all;
entity mc_wb_if is
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        wb_addr_i       : in     vl_logic_vector(31 downto 0);
        wb_cyc_i        : in     vl_logic;
        wb_stb_i        : in     vl_logic;
        wb_we_i         : in     vl_logic;
        wb_err          : out    vl_logic;
        wb_ack_o        : out    vl_logic;
        wb_read_go      : out    vl_logic;
        wb_write_go     : out    vl_logic;
        wb_first        : out    vl_logic;
        wb_wait         : out    vl_logic;
        mem_ack         : in     vl_logic;
        wr_hold         : out    vl_logic;
        err             : in     vl_logic;
        par_err         : in     vl_logic;
        wp_err          : in     vl_logic;
        wb_data_o       : out    vl_logic_vector(31 downto 0);
        mem_dout        : in     vl_logic_vector(31 downto 0);
        rf_dout         : in     vl_logic_vector(31 downto 0)
    );
end mc_wb_if;
