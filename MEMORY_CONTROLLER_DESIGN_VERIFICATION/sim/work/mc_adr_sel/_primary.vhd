library verilog;
use verilog.vl_types.all;
entity mc_adr_sel is
    port(
        clk             : in     vl_logic;
        csc             : in     vl_logic_vector(31 downto 0);
        tms             : in     vl_logic_vector(31 downto 0);
        wb_ack_o        : in     vl_logic;
        wb_stb_i        : in     vl_logic;
        wb_addr_i       : in     vl_logic_vector(31 downto 0);
        wb_we_i         : in     vl_logic;
        wb_write_go     : in     vl_logic;
        wr_hold         : in     vl_logic;
        \cas_\          : in     vl_logic;
        mc_addr         : out    vl_logic_vector(23 downto 0);
        row_adr         : out    vl_logic_vector(12 downto 0);
        bank_adr        : out    vl_logic_vector(1 downto 0);
        rfr_ack         : in     vl_logic;
        cs_le           : in     vl_logic;
        cmd_a10         : in     vl_logic;
        row_sel         : in     vl_logic;
        lmr_sel         : in     vl_logic;
        next_adr        : in     vl_logic;
        wr_cycle        : in     vl_logic;
        page_size       : out    vl_logic_vector(10 downto 0)
    );
end mc_adr_sel;
