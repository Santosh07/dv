onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /top/wif/wb_clk
add wave -noupdate /top/wif/wb_rst
add wave -noupdate /top/wif/wb_data_i
add wave -noupdate /top/wif/wb_data_o
add wave -noupdate /top/wif/wb_addr_i
add wave -noupdate /top/wif/wb_sel_i
add wave -noupdate /top/wif/wb_we_i
add wave -noupdate /top/wif/wb_cyc_i
add wave -noupdate /top/wif/wb_stb_i
add wave -noupdate /top/wif/wb_ack_o
add wave -noupdate /top/wif/wb_err_o
add wave -noupdate /top/wif/susp_req_i
add wave -noupdate /top/wif/resume_req_i
add wave -noupdate /top/wif/suspended_o
add wave -noupdate /top/wif/poc_o
add wave -noupdate /top/wif/mc_br_i
add wave -noupdate /top/wif/mc_bg_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {34950 ps} 0}
configure wave -namecolwidth 157
configure wave -valuecolwidth 40
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {241840 ps}
