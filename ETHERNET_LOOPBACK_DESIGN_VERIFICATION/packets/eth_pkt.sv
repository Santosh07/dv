class eth_pkt;
	rand bit [55:0] preamble;
	rand bit [7:0] sof;
	rand bit [47:0] da;
	rand bit [47:0] sa;
	rand bit [15:0] len;
	rand byte payload[$];
	rand bit [31:0] crc;

	byte byteQ[$];
	byte byteQ_u[$];

	virtual function void print();
		$display("Preamble = %h", preamble);
		$display("SOF = %h", sof);
		$display("DA = %h", da);
		$display("SA = %h", sa);
		$display("LEN = %h", len);
		$display("PAYLOAD = %p", payload);
		$display("CRC = %h", crc);
	endfunction

	virtual function void pack(output byte byteQ[$]);
		byteQ = {>>byte{preamble, sof, da, sa, len, payload, crc}};
	endfunction

	virtual function void unpack(byte byteQ_u[$]);
		int i = 0;
		preamble = {byteQ_u[0], byteQ_u[1], byteQ_u[2], byteQ_u[3], byteQ_u[4], byteQ_u[5], byteQ_u[6]};
		sof = byteQ_u[7];
		da = {byteQ_u[8], byteQ_u[9], byteQ_u[10], byteQ_u[11], byteQ_u[12], byteQ_u[13]};
		sa = {byteQ_u[14], byteQ_u[15], byteQ_u[16], byteQ_u[17], byteQ_u[18], byteQ_u[19]};
		len = {byteQ_u[20], byteQ_u[21]};
		for(i = 0; i<len; i++) begin
			//payload.push_back(byteQ_u[i]); 
			payload[i] = byteQ_u[(22+i)];
		end
		crc = {byteQ_u[22+len], byteQ_u[23+len], byteQ_u[24+len], byteQ_u[25+len]};
	endfunction

	virtual function bit compare(eth_pkt pkt);
		if ((pkt.preamble != preamble) || (pkt.sof != sof) || (pkt.da != da) || (pkt.sa != sa) || (pkt.len != len) || (pkt.crc != crc))return 0;
		foreach (payload[i]) begin
			if (pkt.payload[i] != payload[i]) return 0;
		end
		return 1;
	endfunction

	constraint len_c{
		len == 10;
	};

	constraint payload_c{
		payload.size() == len;	
	};
endclass
