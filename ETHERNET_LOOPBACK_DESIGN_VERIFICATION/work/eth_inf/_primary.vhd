library verilog;
use verilog.vl_types.all;
entity eth_inf is
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic
    );
end eth_inf;
