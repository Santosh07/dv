library verilog;
use verilog.vl_types.all;
entity eth_dut is
    port(
        data_out        : out    vl_logic_vector(7 downto 0);
        valid_out       : out    vl_logic;
        ready_out       : out    vl_logic;
        data_in         : in     vl_logic_vector(7 downto 0);
        ready_in        : in     vl_logic;
        valid_in        : in     vl_logic;
        clk             : in     vl_logic;
        rst             : in     vl_logic
    );
end eth_dut;
