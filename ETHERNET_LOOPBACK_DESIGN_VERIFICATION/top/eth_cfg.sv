class eth_cfg;
	static virtual eth_inf vif_i;
	static mailbox gen2bfm = new();
	static mailbox bfm2mon = new();
	static mailbox mon2cov = new();
	static mailbox mon2refTX_ref = new();
	static mailbox bfm2monRX_ref = new();
	static mailbox mon2refRX_ref = new();
	static int count = 0;
	static int driven_pkts_count = 0;
	static int rcvd_pkts_count = 0;
	static string testname;
endclass
