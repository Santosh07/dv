module top;
	//clk, rst generation
	reg clk, rst;
	
	initial begin
		clk = 0;
		rst = 0;
		forever #5 clk = ~clk;
	end

	initial begin
		//rst = 0;
		repeat(2) @(posedge clk);
		rst = 1;
	end

	//interface instance
	eth_inf vif (clk, rst);

	initial begin
		eth_cfg::vif_i = vif;
	end
	//design instance
	eth_dut dut(.clk(vif.clk),
			.rst(vif.rst),
			.data_out(vif.data_out),
			.valid_out(vif.valid_out),
			.ready_out(vif.ready_out),
			.data_in(vif.data_in),
			.ready_in(vif.ready_in),
			.valid_in(vif.valid_in));

	
	//program block
	eth_tb tb();

	initial begin
		#2500;
		$finish;
	end	

/*	final begin
		$display("No. of Pkts driven to the DUT from the BFM=%d", eth_cfg::driven_pkts_count);
		$display("No. of Pkts that were received from the DUT=%d", eth_cfg::rcvd_pkts_count);
		$display("Count = %d", eth_cfg::count);
		if (eth_cfg::count > 0) begin
			$display("Test failed");
		end
		else begin
			$display("Test Passed");
		end
	end	*/

endmodule
