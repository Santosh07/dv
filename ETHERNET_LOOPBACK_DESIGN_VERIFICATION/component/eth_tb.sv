program eth_tb;
	eth_env env;
	event e;

	initial begin
		$value$plusargs("testname=%s", eth_cfg::testname);
		-> e;
	end
	
	initial begin
		wait(e.triggered);
		env = new();
		env.run();
	end

	final begin
		$display("No. of Pkts driven to the DUT from the BFM=%d", eth_cfg::driven_pkts_count);
		$display("No. of Pkts that were received from the DUT=%d", eth_cfg::rcvd_pkts_count);
		$display("MisMatch Count = %d", eth_cfg::count);
		if (eth_cfg::count > 0) begin
			$display("------------Sorry!!! Keep working-------------------");
		end
		else begin
			$display("***********Great!!! All tests passed***************");
		end
	end
endprogram
