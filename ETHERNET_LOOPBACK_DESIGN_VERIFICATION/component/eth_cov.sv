class eth_cov;
	eth_pkt pkt;
	
	covergroup eth_cg;
		PREAMBLE_CP : coverpoint pkt.preamble{
				option.auto_bin_max = 4;
		}
		SOF_CP : coverpoint pkt.sof{
				//option.auto_bin_max = 4;
				bins SMALL = {[0:125]};
				//bins MEDIUM = {[51:100]};
				//bins MEDIUM_a = {[101:150]};
				//bins MEDIUM_b = {[151:200]};
				bins LARGE = {[126:255]};
		}
		DA_CP : coverpoint pkt.da{
				option.auto_bin_max = 4;
		}
		SA_CP : coverpoint pkt.sa{
				option.auto_bin_max = 4;
		}
		LEN_CP : coverpoint pkt.len{
				//option.auto_bin_max = 4;
				bins CORNER_10 = {10};
		}	
		PAYLOAD_CP : coverpoint pkt.payload.size(){
				option.auto_bin_max = 4;
		}
		CRC_CP : coverpoint pkt.crc{
				option.auto_bin_max = 4;
		}
	endgroup

	function new();
		eth_cg = new();
	endfunction

	task run();
		$display("eth_cov::I am in cov");	
		eth_cfg::mon2cov.get(pkt);
		eth_cg.sample();
	endtask
endclass

