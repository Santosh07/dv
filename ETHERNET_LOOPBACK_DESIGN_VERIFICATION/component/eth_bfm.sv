class eth_bfm;
	eth_pkt pkt;
	eth_pkt pkt_rx1 = new();
	virtual eth_inf vif_bfm;
	int count=0;
	int len_temp = 0; 
	byte byteQ[$];
	byte byterx[$];
	byte byteQ_tx[$];

	task run();
		vif_bfm = eth_cfg::vif_i;
		$display("eth_bfm::I am in bfm");
		fork 
		forever begin
			eth_cfg::gen2bfm.get(pkt);
			eth_cfg::bfm2mon.put(pkt);		//before packing directly sent to Monitor. Avoids an extra step of unpacking
			pkt.pack(byteQ);
			//eth_cfg::bfm2monTX_ref.put(byteQ);			
			$display("-------BYTE that was transmitted from the BFM-------");
			$display("%p", byteQ);
			$display("-------------------------------------------------------");
			drive_pkt(byteQ);
			byteQ.delete();
			//pkt.print();
		end
	
		forever begin
			@(posedge vif_bfm.clk);
			if (vif_bfm.valid_out) begin
				count += 1;
				byterx.push_back(vif_bfm.data_out);
				vif_bfm.ready_in = 1;
			end
			if (count == 22) begin
				len_temp = {byterx[20], byterx[21]};
			end
			if (count == 26 + len_temp) begin
				$display("-------------------BYTE THAT WAS RECEIVED FROM THE DUT------------------------");
				eth_cfg::rcvd_pkts_count += 1;
				$display("ETH_CFG::RCVD_PKTS_COUNT=%d", eth_cfg::rcvd_pkts_count);
			//	eth_cfg::bfm2monRX_ref(byterx);
				pkt_rx1.unpack(byterx);
				eth_cfg::bfm2monRX_ref.put(pkt_rx1);		//Unpacked and getting sent to monitor. Will be sent to the reference model from monitor later.
				$display("%p", byterx);
				$display("-------------------------------------------------------------------------------");
				byterx.delete();
				count = 0;
			end
		end
		join
		#1000;	
	endtask

	task drive_pkt(byte byteQ_tx[$]);
		bit ready_f = 1;
		eth_cfg::driven_pkts_count += 1;
		$display("ETH_CFG::DRIVEN_PKTS_COUNT=%d", eth_cfg::driven_pkts_count);
		wait(vif_bfm.rst);
		foreach (byteQ_tx[i]) begin
			@(posedge vif_bfm.clk);
			vif_bfm.valid_in = 1;
			vif_bfm.data_in = byteQ_tx[i];
		/*	if (ready_f) begin
				@(negedge vif_bfm.clk);
				if (vif_bfm.ready_out) begin
					vif_bfm.valid_in = 0;
					ready_f = 0;
					end 
				end	*/
			end	
	endtask
endclass
