interface eth_inf(input logic clk, input logic rst);
	logic [7:0] data_out;
	logic valid_out;
	logic ready_out;
	logic [7:0] data_in;
	logic ready_in;
	logic valid_in;

/*	clocking cb_bfm @(posedge clk);
		default input #2ns output #2ns;
		input clk;
		input rst;
		input data_out;
		input valid_out;
		input ready_out;
		output data_in;
		output ready_in;
		output valid_in;
	endclocking	*/
//modport bfm(input clk, rst, data_out, valid_out, ready_out, output data_in, ready_in, valid_in);

endinterface
