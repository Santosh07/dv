typedef enum bit {good, bad} pkt_typ;
class eth_gen;
	bit pkt_type;
	good_pkt g_pkt;
	bad_pkt b_pkt;
	task run();
		$display("eth_gen::I am in gen");
		case (eth_cfg::testname)
			"Good_pkt_5": begin
				repeat(5) begin
					g_pkt = new();
					assert(g_pkt.randomize());
					eth_cfg::gen2bfm.put(g_pkt);
					end
				end
			"Bad_pkt_5": begin
				repeat(5) begin
					b_pkt = new();
					assert(b_pkt.randomize());
					eth_cfg::gen2bfm.put(b_pkt);		
				end
			end
			"Mixed_pkt": begin
				repeat(5) begin
					pkt_type = $random;
				//	$display("pkt_type=%d", pkt_typ);
					case (pkt_type) 
						good: begin
							g_pkt = new();
							assert(g_pkt.randomize());
							eth_cfg::gen2bfm.put(g_pkt);
						end
						bad: begin
							b_pkt = new();
							assert(b_pkt.randomize());
							eth_cfg::gen2bfm.put(b_pkt);
						end
					endcase
				end
			end
			//test to generate packets with crc corrupted
			//test to generate packets with SOF not in order
		endcase
	endtask
endclass
