class eth_ref;
	eth_pkt pkt;
	eth_pkt pkt_rx;

	task run();
		$display("eth_ref::I am in ref");
		forever begin
			eth_cfg::mon2refTX_ref.get(pkt);
			eth_cfg::mon2refRX_ref.get(pkt_rx);
//			$display("Inside Ref::TX_pkt=%p", pkt);
//			$display("Inside Ref::RX_pkt=%p", pkt_rx);
//			$display("------------Inside Ref::Packet that was transmitted-----------");
//			pkt.print();
//			$display("--------------------------------------------------------------");
//			$display("------------Inside Ref::Packet that was Received-----------");
//			pkt_rx.print();
//			$display("--------------------------------------------------------------");
			checker_test();
		end
	endtask
	
	task checker_test();
		$display("-------------PACKETS BEING COMPARED--------------");
		$display("-------------Packets that were transmitted from the BFM----------");
		pkt.print();
		$display("------");
		$display("------------Packet that were received from the DUT---------");
		pkt_rx.print();
		$display("------");
		$display("-------------------------------------------------");
		if (pkt.compare(pkt_rx) == 0) begin
			eth_cfg::count += 1;
		end
	endtask	

endclass

