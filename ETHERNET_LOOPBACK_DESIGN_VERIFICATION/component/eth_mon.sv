class eth_mon;
	eth_pkt pkt;
	eth_pkt pkt_rx;
	byte byteTX[$];
	byte byteRX[$];
	task run();
		$display("eth_mon::I am in mon");
		forever begin
			eth_cfg::bfm2mon.get(pkt);
			eth_cfg::mon2cov.put(pkt);
			eth_cfg::mon2refTX_ref.put(pkt);		//Being sent to the reference model. This is the one that was transmitted from the BFM to DUT 
			eth_cfg::bfm2monRX_ref.get(pkt_rx);		//Getting it from the BFM model after being received from the DUT and being unpacked
			eth_cfg::mon2refRX_ref.put(pkt_rx);
		end
	endtask
endclass
