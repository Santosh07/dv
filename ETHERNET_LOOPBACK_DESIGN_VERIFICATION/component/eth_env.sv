class eth_env;
	eth_gen gen;
	eth_bfm bfm;
	eth_mon mon;
	eth_cov cov;
	eth_ref ref1;

	function new();
		gen = new();
		bfm = new();
		mon = new();
		cov = new();
		ref1 = new();
	endfunction
	task run();
		$display("eth_env::I am in run");
		fork
			gen.run();
			bfm.run();
			mon.run();
			cov.run();
			ref1.run(); 
		join
	endtask
endclass
