module eth_dut(output reg [7:0] data_out,
	output reg valid_out,
	output reg ready_out,
	input [7:0] data_in,
	input ready_in,
	input valid_in,
	input clk,
	input rst);

	byte byteQ_t[$];
	byte byteQ[$];
	byte byterx[$];
	bit[31:0] len_temp;
	bit flag_1;
	int count=0;
	semaphore smp = new(1);	

	always @(posedge clk or negedge rst) begin
		if (!rst) begin
			data_out <= 8'd0;
			ready_out <= 0;
			valid_out <= 0;
		end
		else begin
			if (valid_in) begin
	//			$display("Valid_in became 1");
				byteQ.push_back(data_in);
	//			$display("BYTEQ=%p", byteQ);
				ready_out <= 1;
				count += 1;
	//			$display("Count = %d", count);
				if (count == 22) begin
	//				$display("I got the length");
	//				$display("ByteQ[20]=%b", byteQ[20]);
	//				$display("ByteQ[21]=%b", byteQ[21]);
					len_temp = {byteQ[20], byteQ[21]};
	//				$display("Len_temp = %b", len_temp);
				end
				if (count == 26 + len_temp) begin
	//				$display("All packets received");
					byterx = byteQ;
					byteQ.delete();
					count = 0;
					flag_1 = check_pkt_f(byterx);
				//	$display("flag_1=%d", flag_1);

						if (flag_1) begin 
				//			$display("I'll be driving the packet");
							fork
								//#20;
								drive_pkt(byterx);
				//				$display("I have sent this to the drive pkt task=%p", byterx);
								byterx.delete();
				//				$display("I have driven the packet");
							join_none
						end
				end
			end
		end
	end

	function bit check_pkt_f(byte byterx[$]);
		return 1;
	endfunction

	task drive_pkt(byte byteQ_t[$]);
	//	$display("I'll be getting the key");
	//	$display("BYTEQ_T before getting the key=%p", byteQ_t);
		smp.get(1);
	//	$display("I have got the key");
	//	$display("BYTEQ_T inside the drive_pkt task=%p", byteQ_t);
		foreach(byteQ_t[i]) begin
	//			$display("I am in drive packet task");
				valid_out = 1;
	//			$display("I made valid_out 1=%b", valid_out);
				data_out = byteQ_t[i];
				@(posedge clk);
				wait(ready_in);
	//			$display("Ready_in became 1");
			end
		valid_out = 0;
		//byteQ_t.delete();
		smp.put(1);
	endtask
endmodule
